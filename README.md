# Starlims Customs and Utils

This repository contains a collection of customizations and utilities for the Starlims LIMS system.
Use them as you wish, but please be aware that these features are not supported by Starlims if not implemented by Starlims themselves.

## Usage

Some of the code you'll find needs to go in the HTML Bootstrap to edit the behavior of components in the whole application. Some other files provide functions, these ones should usually be independant client scripts that you can add to your forms when you need them.
You will also find some files that simply provide a documentation for a feature, these ones are not meant to be used in your application.

## Repository content

## Utilities

In this folder you will find:

- [Basic Bootstrap](Utilities/BasicBootstrap.js): A basic bootstrap script that can be used as a base for any environment, it contains basic UX/UI improvements and some useful functions. **Warning**: This script contains a function to apply the base context menu to every datagrids. To work properly, you also need the improved [ContextMenu](Improvements/Datagrid/ContextMenu.js) script. To disable this behavior, remove the region `ContextMenu for every datagrid`. **Fixs/Improvements included in BasicBootstrap are arked with ✔️
- [Starlims SSL Functions Manual](Utilities/Starlims%20SSL%20Functions%20Manual): A manual for some of the SSL functions available in Starlims.
- [Timezones.xlsx](Utilities/Timezones.xlsx)|[Timezones.csv](Utilities/timezones.csv): A list of .NET timezones, useful for CrystalReports when ou encounter an error with the timezone.
- [QVA-Utils](Utilities/QVA-Utils.md): A collection of useful code snippets, information and tips that can be used in Starlims.
- [CSharp.ssl](Utilities/CSharp.ssl): A script that allows you to write C# code in Starlims. Desription of usage in the script header. Examples : [HttpFetch](readme_res/examples/Utilities.HttpFetch.ssl), [GetStatuses](readme_res/examples/QVA.GetStatuses.ssl)
- [SSL Functions Doc 12.6.2](Utilities/SSLDoc12.6.2.zip) An html documentation of the SSL functions available in Starlims 12.6.2. **Warning**: This documentation has been partially generated with AI. I proofread it but there may still be some errors. If you find any, please let me know. The doc is currently live [here](https://starlims.fr/docs/en).

## Improvements

You can find a collection of improvements in the `Improvements` folder.

List of improvements:

- UX
  - ✔️ [combobox-mc-betterselect](Improvements/UX/combobox-mc-betterselect.js): Change the behavior of comboboxes and multichoice components to allow typing the first letters of the item to select it. Initial bahvior was handling only the first letter of the item. (Useful for comboboxes with a lot of items starting witht the same letter)
  - [combobox-xfd-behavior](Improvements/UX/combobox-xfd-behavior.js): Change the behavior of comboboxes to select next item when pressing the initial letter multiple times, just like in XFD.
  - [DisplayWaitMessage](Improvements/UX/DisplayWaitMessage.js): Change `form.DisplayWaitMessage` behavior to apply mask on current form and not on the whole page. (Useful with modal forms. You can still use old function by passing true as second parameter.)
  - ✔️ [ShowModalDialog](Improvements/UX/ShowModalDialog.js): Override `form.ShowModalDialog` to display the wait message when the modal is loading. Giving the user a feedback that something is happening.
  - [StarlimsDragAndDrop](Improvements/UX/StarlimsDragAndDrop.js): Easy-to-use functions to allow file drag and drop in Starlims. Can be used with SDMS to upload files in the incoming queue, or with attachments by dropping the file on the datagrid instead of having to do all the actions (Both these examples are available on pastebin, see header of the file) ***Installation: Import the [StarlimsDragAndDrop.sdp](Improvements/UX/StarlimsDragAndDrop.sdp) package in Starlims***.
  - [ContextMenu](Improvements/UX/ContextMenu.js): Improve the context menu of datagrids.
  - [Datagrid Filter Bar](Improvements/UX/StarlimsDgdFilterBar.js): Add a filter bar to datagrids to easily filter the data. The filter bar is similar to what we had in XFD. You can filter the datagrid visible data, it doesn't change the SQL query. Handles dates, comboboxes and checkboxes with type-specific fields. Other fields are filterable by text. The filter bar is displayed at the top of the datagrid if the FilterRowVisible property is set to true. If you have the latest version of the [ContextMenu](Improvements/UX/ContextMenu.js) script, you can toggle the filter bar with the context menu. *Note, the toggle option only works if the datagrid has some rows displayed, you can't toggle on the filterbar in an empty datagrid*
    <details>
      <summary>Examples:</summary>

      ![Datagrid Filter Bar](readme_res/DatagridFilterBar.gif)
    </details><br/>
  - ✔️ [Consoles Better Search](Improvements/UX/ConsolesBetterSearch.js): Improve the search in consoles, groups are included in the search. Example: If you search for 'Results', it will show the Results Entry group and every subitem e.g. 'By Batch', 'By Tests' etc...
  
- Performances
  - [consoles-newtab](Improvements/Performances/consoles-newtab.js): Allow to not load the consoles when opening in a new tab. (For NEWTAB and UNIQUETAB opening mode) (**Warning**: Only use this feature if you configured your Starlims to hide consoles when opening in a new tab.)
  - [DatagridColumns](Improvements/Performances/DatagridColumns.js): Some functions that allow making operations on datagrid columns without redrawing the grid each time. Useful for results entry where you need to add/remove columns or change their captions dynamically.
  - [Deserialize](Improvements/Performances/Deserialize.js): R&D fix that improve the performances of the `Deserialize` function.
  - ✔️ [SetData(Set)Async](Improvements/Performances/SetData(Set)Async.js): Some functions to allow loading data asynchronously easily by using `component.SetDataAsync` and `component.SetDataSetAsync` instead of handling multiple promises and toggling the loading state manually.
  - [AsyncDataLoader](Improvements/Performances/AsyncDataLoader.js) (***May cause issues***): Override the default data loading behavior for components that have Data assigned in the designer (Datagrids, ComboBoxes, GroupBoxes, etc). This will allow to load the data asynchronously and display a loading message while the data is loading. (Useful for forms with a lot of components)
    <details>
      <summary>Examples:</summary>

      ![Without Async Data Loader](readme_res/NoAsyncDataLoader.gif)

      ![With Async Data Loader](readme_res/WithAsyncDataLoader.gif)
      ![Supported Components](readme_res/AsyncLoaderComponents.gif)
    </details><br/>
  - ✔️ [DataGrid RefreshAsync](Improvements/Performances/DataGridRefreshAsync.js): Add a `RefreshAsync` function to datagrids to refresh the data asynchronously. Multiple options are available like keeping scroll values, keeping selection based on some columns, trigger or not the rowChange event, keeping groups, filters, and more. (**Installation**: Add the code of [DataGridRefreshAsync.js](Improvements/Performances/DataGridRefreshAsync.js) to the HTML Bootstrap)
  - [DirectOINT](Improvements/Performances/DirectOINT.js): Load forms without waiting for the reminder console to load when opening an app in a new tab. By default, the form doesn't load before the console has finished loading. **Installation**: Import the [DirectOINT.sdp](Improvements/Performances/DirectOINT.sdp) package in Starlims and add the code of [DirectOINT.js](Improvements/Performances/DirectOINT.js) to the HTML Bootstrap. **Warning**: This script is not compatible with the `consoles-newtab` script. If you want to use both, you need to merge them. **Warning 2**: Make sure to check line 13-14 of the js script to adapt the behavior to your environment. **Info**: This improvement will only work on environments with lockfree sessions.
  <details>
    <summary>Read more about lockfree sessions</summary>
    
    #### READ-ONLY WEB SESSIONS
    To enable Read-Only web sessions: Locate the web.config file on the STARLIMS Application Server (found in the web application folder, such as `c:\inetpub\wwwroot\<starlimsapp>\web.config`).

    Locate the section `<appSettings>` and add the setting EnableReadOnlySession with the value `true`. `<add key="EnableReadOnlySession" value="true"/>`

    If the setting already exists, change the value to `true`.

    If a server script needs read-write access to the web session, then the name of the server script must be added to the exceptions list. The exceptions list is a different setting in the web.config file.

    Add or edit the setting ReadOnlySessionExceptions and add the full name of the server script (category_name.script_name) into the setting value (the script names are separated by commas and must not contain spaces). For example: `<add key="ReadOnlySessionExceptions" value="cat1.script1,cat2.script2"/>`

    #### Read-Only Session Troubleshooting

    In order to see if a script tries to write a value into a Read-Only Web Session, add the web.config setting EnableReadOnlySessionLogging and set the value to `true`.

    `<add key="EnableReadOnlySessionLogging" value="true"/>`

    When this setting is set to `true`, the system will add an entry in the system log every time the system adds/removes a value into/from a Read-Only Web Session. For example: 
    ```
      < Error entry at 08:39:22.396 from w3wp(14900/22/38) on machine TEST. User: TEST. Ver: 12.0 >
      Session is read-only. Cannot perform <Add>. Key name: test
      Stack trace:
      at ServerScript._.t5.main_ñ() in ServerScript._.t5.ssl:line 1
      at ServerScript.Runtime_Support.RunScriptById.main_ñ(SSLValue unique_scriptId, SSLValue unique_params) in ServerScript.Runtime_Support.RunScriptById.ssl:line 9
    ```
  </details>

- [UploadFile](Improvements/UploadFile/): Improvement of the SaveFileFromHTML server script to allow multiple file upload (useful in [StarlimsDragAndDrop](Improvements/UX/StarlimsDragAndDrop.js) for example). (**Installation**: Import the [Runtime_Support.SaveFile-s-FromHtml.sdp](Improvements/UploadFile/Runtime_Support.SaveFile-s-FromHtml.sdp) package in Starlims)

## Customizations

You can find a collection of customizations in the `Customs` folder.

List of customs:

- [pinappfunction](Customs/pinappfunction.js): Change the behavior of the `APPLICATION TAB` opening mode to open new tabs with the same behavior as the `NEWTAB` opening mode (Hiding consoles) (**Warning**: Requires `Improvements/Performances/consoles-newtab`) (**Installation**: Follow [Documentation](readme_res/Menu%20item%20behavior.pdf)).
- [cachemanager](readme_res/cachemanager.md) (***Experimental***): New application allowing admins to clean their own cache or the cache of all users. (**Installation**: Follow [Documentation](readme_res/cachemanager.md)).
- ✔️ [topbar-color](Customs/topbar-color.js): Allow to easily change the color of the topbar with keywords from the URL linked to a color.
- [BugReports](Customs/BugReports.js) (***Experimental***): A simple application that allows users to report bugs in Starlims. To instlall, import the provided package [BugReports.sdp](Customs/BugReports.sdp) and add the code of [BugReports.js](Customs/BugReports.js) to the HTML Bootstrap.
- [FormDataRequest](Customs/FormDataRequest.zip): ServerScript class that allows to make FormData HTTP Requests. Append body fields or files easily just like in the normal Starlims webservice request. To install: Import the .sdp present in [FormDataRequest.zip](Customs/FormDataRequest.zip), you will also find some documentation in the zip file.
- [NeoGrabber](Customs/NeoGrabber.sdp): Application that allows grabbing files to SDMS without the grabber software. To install: Import the [NeoGrabber.sdp](Customs/NeoGrabber.sdp).
- [Modal With Logo](Customs/ModalWithLogo.js): Add a logo to the modal dialog. (**Installation**: Add the code of [ModalWithLogo.js](Customs/ModalWithLogo.js) to the HTML Bootstrap, edit the code to add your image).
- [Custom Default App](Customs/CustomDefaultApp.js): Allow users to change their default application. The application will be opened when the user logs in. (**Installation**: Add the code of [CustomDefaultApp.js](Customs/CustomDefaultApp.js) to the HTML Bootstrap and import the [CustomDefaultApp.sdp](Customs/CustomDefaultApp.sdp) package in Starlims).

## Fixes

You can find a collection of fixes in the `Fixes` folder.
These fixes are mainly for Starlims features that can have bugs used in a specific way.

List of fixes:

- Datagrid
  - [datagrid-empty-row](Fixes/Datagrid/datagrid-empty-row.js): Fix the empty datagrid row that appears at the end of the datagrid sometimes. For normal and frozen datagrid.
- Dialogs
  - ✔️ [InputBox](Fixes/Dialogs/InputBox.js): Fix the `InputBox` dialog window to correctly return the value and changed the form layout.
- Reporting
  - [TimezoneFix](Fixes/Reporting/TimezoneFix.js): Fix the timezone issue in CrystalReports when the client timezone is not found.
