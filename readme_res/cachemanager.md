# Cache Manager

## Description

This applcation allows admin users to clean the cache of all users or their own cache.

## Installation

Import [the CacheManager package](./CacheManager.sdp) in your Starlims environment.

Then add this code to the HTML_Bootstrap client script:

```javascript
// region CacheManager
function clearCacheAndReload() {
    const navvar = localStorage.getItem('Starlims.Navigator.Variables')
    localStorage.clear()
    localStorage.setItem('Starlims.Navigator.Variables', navvar)
    localStorage.setItem('last-cache-deletion', new Date().toISOString())
    window.location.reload()
}
Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
    const lastCacheDeletionDate = localStorage.getItem('last-cache-deletion')
    if(!lastCacheDeletionDate) return clearCacheAndReload()
    lims.CallServerAsync('Enterprise_Settings.GetCacheDeletionDate').then(isoDate => {
        const deletionDate = new Date(isoDate)
        const lastDeletionDate = new Date(lastCacheDeletionDate)
        if(lastDeletionDate.toString() === 'Invalide Date' || lastDeletionDate < deletionDate) clearCacheAndReload()
    })
});
// endregion
```

This code will clear the localstorage of the user. This includes cached forms. However, it won't clear the whole browser cache. If you want this change to also apply to scripts like shell-all, you can replace this code in the shell-all.js file of the htmlruntime:

Replace : `script.src=scriptName;`

By : ``` script.src=scriptName.endsWith('shell-all.js')?`${scriptName}?v=${localStorage.getItem('last-cache-deletion')}`:scriptName; ```

Also edit the code of the serverscript `Web_Support.Starthtml`:

Replace : `controlsJS := GetRegion("ScriptsRelease", {}, {});`

By

```javascript
cacheDeletionDate := ExecFunction('Enterprise_Settings.GetCacheDeletionDate');
controlsJS := GetRegion("ScriptsRelease", {'$cacheDeletionDate$'}, {cacheDeletionDate});
```

And replace : `<script type="text/javascript" src="htmlruntime/desktop/starlims-all.js"></script>`

By : `<script type="text/javascript" src="htmlruntime/desktop/starlims-all.js?v=$cacheDeletionDate$"></script>`
