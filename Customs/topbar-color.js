(async () => {
	const colorMap = [
		// Edit these to match keywords/urls contained in the environments and the colors you want to apply for each
		{ env: 'FULL_URL', color: '#0057e7' },
		{ env: 'KEYWORK', color: '#ffa700' },
		{ env: 'DEV', color: '#008744' },
	];
	const color = colorMap.find(c => document.location.href.toUpperCase().includes(c.env))?.color;
	if (!color) return;
	document.styleSheets[document.styleSheets.length - 1].addRule(
		'.shell-topbar .x-panel-body-default',
		`background-color: ${color}`,
	);
})();
