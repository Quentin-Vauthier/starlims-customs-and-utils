// region Import ExcelJS https://github.com/exceljs/exceljs#readme
(() => {
	if (window.isExcelJsLoaded) return;

	const script = document.createElement('script');
	script.src = 'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.js';

	const script2 = document.createElement('script');
	script2.src = 'https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.3.0/exceljs.min.js';

	script.addEventListener('load', ev => {
		window.isExcelJsLoaded = true;
	});
	script.addEventListener('error', ev => {
		window.isExcelJsLoaded = false;
	});
	document.head.appendChild(script);
	document.head.appendChild(script2);
})();
// endregion

// region csOpenExcelFile
/**
 * Open an excel (xlsx) file and returns the workbook and the filename (Only work in HTTPS environment)
 * @returns {Promise<[ExcelJS.Workbook, string]>}
 */
const csOpenExcelFile = () => {
	return new Promise((resolve, reject) => {
		if (!window.showOpenFilePicker) reject(new Error('This function only works in HTTPS servers'));
		window
			.showOpenFilePicker({
				excludeAcceptAllOption: true,
				types: [
					{
						description: 'Excel file',
						accept: {
							'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx'],
						},
					},
				],
			})
			.then(async res => {
				const [fileHandle] = res;
				const fileData = await fileHandle.getFile();
				const fileName = fileData.name;
				const workbook = new ExcelJS.Workbook();
				const fileReader = new FileReader();
				fileReader.onload = async function () {
					await workbook.xlsx.load(fileReader.result);
					resolve([workbook, fileName]);
				};
				fileReader.onabort = reject;
				fileReader.onerror = reject;
				fileReader.readAsArrayBuffer(fileData);
			})
			.catch(() => resolve(null));
	});
};
// endregion

// region csSaveExcelFile
/**
 * Saves an excel file to the local file system
 * @param {ExcelJS.Workbook} workbook
 * @param {string} fileName - The default filename to use when saving to the user's local file system
 * @param {boolean} directDownload - If true, the file will be downloaded directly without prompting the user to pick the location of the file
 * @returns {Promise<void>}
 */
const csSaveExcelFile = (workbook, fileName, directDownload) => {
	return new Promise((resolve, reject) => {
		if (directDownload) {
			workbook.xlsx.writeBuffer().then(buffer => {
				downloadByteArrayToXLSX(fileName, buffer);
				resolve();
			});
			return;
		}
		if (!window.showSaveFilePicker) reject(new Error('This function only works in HTTPS servers'));
		window
			.showSaveFilePicker({
				types: [
					{
						description: 'Excel file',
						accept: {
							'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx'],
						},
					},
				],
				suggestedName: fileName,
			})
			.then(async fileHandle => {
				const writable = await fileHandle.createWritable();
				const buffer = await workbook.xlsx.writeBuffer();
				await writable.write(buffer);
				await writable.close();
				resolve();
			})
			.catch(() => resolve(null));
	});
};

function downloadByteArrayToXLSX(fileName, buffer) {
	const blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
	const link = document.createElement('a');
	link.href = window.URL.createObjectURL(blob);
	link.download = fileName;
	link.click();
	link.remove();
}
// endregion

// region csExportDatagridExcelJS
/**
 * Exports the content of a datagrid to an excel file
 * @param {StarlimsDataGrid} datagrid - The datagrid to export
 * @param {object} options - An object containing the options
 * @param {string} options.fileName - The default filename to use when saving to the user's local file system
 * @param {string} options.sheetName - The name of the sheet to create
 * @param {boolean} options.exportInvisible - If true, invisible columns will be exported
 * @param {object} options.headerStyle - An object containing ExcelJS style properties to apply to the header row
 * @param {object} options.columnsStyle - An object containing names or indexes of columns and ExcelJS style properties to apply to the columns
 * @param {object} options.dataCellStyle - An object containing ExcelJS style properties to apply to the data cells
 * @param {boolean} options.directDownload - If true, the file will be downloaded directly without prompting the user to pick the location of the file
 * @param {function} options.afterProcess - A function taking the worksheet as parameter that will be called after the export is done
 * @param {number} options.colMinWidth - The minimum width of the column
 * @param {number} options.colMaxWidth - The maximum width of the column
 */
const csExportDatagridExcelJS = async (datagrid, options) => {
	const rootTable = datagrid.RootTable;
	const dataSet = datagrid.DataSet;
	try {
		datagrid.mask('Excel export in progress...');
	} catch (ign) {
		/* Ignored */
	}
	await csExportDataSetExcelJS(dataSet, {
		...options,
		rootTable,
	});
	try {
		datagrid.unmask();
	} catch (ign) {
		/* Ignored */
	}
};
// endregion

// region decodeHtml
function decodeHtml(html) {
	const txt = document.createElement('textarea');
	txt.innerHTML = html;
	return txt.innerText;
}
// endregion

// region csExportDataSetExcelJS
/**
 * Exports the content of a dataset to an excel file
 * @param {StarlimsDataGrid} datagrid - The datagrid to export
 * @param {object} options - An object containing the options
 * @param {string} options.fileName - The default filename to use when saving to the user's local file system
 * @param {string} options.sheetName - The name of the sheet to create
 * @param {boolean} options.exportInvisible - If true, invisible columns will be exported
 * @param {object} options.headerStyle - An object containing ExcelJS style properties to apply to the header row
 * @param {object} options.columnsStyle - An object containing names or indexes of columns and ExcelJS style properties to apply to the columns
 * @param {object} options.dataCellStyle - An object containing ExcelJS style properties to apply to the data cells
 * @param {boolean} options.directDownload - If true, the file will be downloaded directly without prompting the user to pick the location of the file
 * @param {function} options.afterProcess - A function taking the worksheet as parameter that will be called after the export is done
 * @param {RootTable} options.rootTable - The root table taken for the export, if not specified the columns will be taken from the DataSet
 * @param {number} options.colMinWidth - The minimum width of the column
 * @param {number} options.colMaxWidth - The maximum width of the column
 * @param {boolean} options.decodeValues - If true, the values will be decoded from HTML entities
 */
const csExportDataSetExcelJS = async (
	dataSet,
	{
		rootTable,
		fileName,
		sheetName,
		exportInvisible,
		headerStyle,
		columnsStyle,
		dataCellStyle,
		directDownload,
		afterProcess,
		colMinWidth,
		colMaxWidth,
		decodeValues = false,
	},
) => {
	try {
		const columns = rootTable
			? rootTable.Columns.filter(c => exportInvisible || c.Visible)
			: dataSet.Tables[0].Columns;
		const rows = dataSet.Tables[0].Rows;
		const header = columns.map(c => c.Caption);
		const workbook = new ExcelJS.Workbook();
		const now = new Date();
		sheetName = sheetName ?? rootTable.DataMember;
		const worksheet = workbook.addWorksheet(sheetName);
		fileName = fileName ?? `Export_${sheetName}_${now.toLocaleDateString()}.xlsx`;
		fileName = fileName.endsWith('.xlsx') ? fileName : `${fileName}.xlsx`;
		worksheet.addRow(header);
		// add rows
		for (const row of rows) {
			const values = decodeValues
				? columns.map(c => decodeHtml(row[c.DataMember]))
				: columns.map(c => row[c.DataMember]);
			worksheet.addRow(values);
		}

		// add border to all cells
		worksheet.eachRow({ includeEmpty: false }, function (row) {
			row.eachCell({ includeEmpty: true }, function (cell) {
				cell.border = {
					top: { style: 'thin' },
					left: { style: 'thin' },
					bottom: { style: 'thin' },
					right: { style: 'thin' },
				};
			});
		});

		if (typeof dataCellStyle === 'function') {
			worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
				if (rowNumber === 1) return; // skip header row (rowNumber = 1)
				row.eachCell({ includeEmpty: false }, function (cell, colNumber) {
					dataCellStyle(cell, rowNumber, colNumber);
				});
			});
		} else if (typeof dataCellStyle === 'object') {
			for (const [key, value] of Object.entries(dataCellStyle)) {
				const colIndex = worksheet.getRow(1).indexOf(key);
				if (colIndex === -1) {
					console.warn(`Column ${key} not found`);
					continue;
				}
				worksheet.getColumn(colIndex).eachCell({ includeEmpty: true }, function (cell, colNumber) {
					if (colNumber === 1) return; // skip header row (colNumber = 1)
					cell[key] = value;
				});
			}
		}

		// set columns style
		if (columnsStyle) {
			for (const [name, style] of Object.entries(columnsStyle)) {
				const colIndex = worksheet.getRow(1).values.indexOf(name);
				for (const [key, value] of Object.entries(style)) {
					worksheet.getColumn(colIndex).eachCell({ includeEmpty: true }, function (cell) {
						cell[key] = value;
					});
				}
			}
		}

		// set header style
		if (headerStyle) {
			const headerRow = worksheet.getRow(1);
			headerRow.eachCell({ includeEmpty: true }, function (cell) {
				for (const [key, value] of Object.entries(headerStyle)) {
					cell[key] = value;
				}
			});
		}

		// autofit columns
		csAutofitColumns(worksheet, undefined, undefined, { min: colMinWidth, max: colMaxWidth });

		// after process
		if (afterProcess) {
			afterProcess(worksheet);
		}

		await csSaveExcelFile(workbook, fileName, directDownload ?? false);
	} catch (err) {
		console.error(err);
		Dialogs.MessageBox(err.message, 'Error', 'OK', 'ERROR');
	}
};

// endregion

// region csAutofitColumns
/**
 * Function used to autofit columns
 * @param {ExcelJS.Worksheet} worksheet - An ExcelJS worksheet
 * @param {number} defaultFontSize - The default font size of the worksheet
 * @param {Array} excludedCells - An array of cells to be excluded from autofit (cell object, cell address, or function that returns true if the cell is excluded)
 * @param {object} options - An object containing the options
 * @param {number} options.min - The minimum width of the column
 * @param {number} options.max - The maximum width of the column
 */
const csAutofitColumns = (worksheet, defaultFontSize = 11, excludedCells = [], { min, max }) => {
	worksheet.columns.forEach(column => {
		const checkExcludedCells = Array.isArray(excludedCells);
		const excludedCellsType = checkExcludedCells ? typeof excludedCells[0] : null;
		let maxWidth = 0;
		column.eachCell(cell => {
			// Check if cell is excluded
			if (checkExcludedCells && excludedCellsType === 'object' && excludedCells?.includes(cell)) return;
			else if (checkExcludedCells && excludedCellsType === 'string' && excludedCells?.includes(cell.address))
				return;
			else if (checkExcludedCells && excludedCellsType === 'function' && excludedCells(cell) === true) return;
			const valueLength =
				cell.value instanceof Date
					? cell.value.toLocaleDateString().length
					: cell.value?.toString().length ?? 0;
			if (valueLength === 0) return;
			const cellWidth =
				((cell.font?.size ?? defaultFontSize) * valueLength * (cell.font?.bold ? 1.2 : 1)) / defaultFontSize;
			if (cellWidth > maxWidth) maxWidth = cellWidth;
		});
		let width = Math.ceil(maxWidth) + 4;
		if (min && width < min) width = min;
		if (max && width > max) width = max;
		column.width = width;
	});
};
// endregion

//	region RangeCollums
/**
 * Fonction pour générer une liste de combinaisons de colonne et de ligne dans le format Excel (ex: A1, B2, AG7, etc.)
 * @param {string} start combinaison de colonne et de ligne pour le début de la plage (ex: "A1")
 * @param {string} end combinaison de colonne et de ligne pour la fin de la plage (ex: "Z10")
 * @returns un tableau contenant toutes les combinaisons de colonne et de ligne dans la plage spécifiée
 */
// Arguments: start (chaîne de caractères) -
//            end (chaîne de caractères) -
// Retourne:
function csGenerateExcelCells(start, end) {
	// Extraire la colonne de départ, la ligne de départ, la colonne de fin et la ligne de fin à partir des arguments
	const startCol = start.match(/[A-Z]+/)[0];
	const startRow = parseInt(start.match(/[0-9]+/)[0]);
	const endCol = end.match(/[A-Z]+/)[0];
	const endRow = parseInt(end.match(/[0-9]+/)[0]);

	// Convertir les lettres de la colonne en nombre pour faciliter les calculs dans la boucle
	const startColNum = columnToNumber(startCol);
	const endColNum = columnToNumber(endCol);

	// Créer un tableau pour stocker les combinaisons de colonne et de ligne
	const cells = [];

	// Boucle à travers toutes les lignes dans la plage spécifiée
	for (let i = startRow; i <= endRow; i++) {
		// Boucle à travers toutes les colonnes dans la plage spécifiée
		for (let j = startColNum; j <= endColNum; j++) {
			// Convertir le numéro de colonne en lettre de colonne pour créer la combinaison de colonne et de ligne
			cells.push(numberToColumn(j) + i);
		}
	}

	// Retourner le tableau de combinaisons de colonne et de ligne
	return cells;
}

/**
 * Fonction pour convertir une lettre de colonne en nombre de colonne
 * @param {string} lettre de colonne (ex: "A", "Z", "AA", "AB", etc.)
 * @returns le numéro de colonne correspondant (ex: 1, 26, 27, 28, etc.)
 */
function columnToNumber(col) {
	let colNum = 0;
	// Boucle à travers chaque lettre dans la chaîne de colonne
	for (let i = 0; i < col.length; i++) {
		// Convertir la lettre en nombre en utilisant la fonction charCodeAt()
		colNum = colNum * 26 + (col.charCodeAt(i) - 64);
	}
	// Retourner le numéro de colonne résultant
	return colNum;
}

//
// Arguments: num (nombre entier) -
// Retourne: la lettre de colonne correspondante (ex: "A", "Z", "AA", "AB", etc.)
/**
 * Fonction pour convertir un nombre de colonne en lettre de colonne
 * @param {int} num numéro de colonne (ex: 1, 26, 27, 28, etc.)
 * @returns
 */
function numberToColumn(num) {
	let col = '';
	while (num > 0) {
		const remainder = (num - 1) % 26;
		col = String.fromCharCode(65 + remainder) + col;
		num = Math.floor((num - remainder) / 26);
	}
	return col;
}

// endregion
