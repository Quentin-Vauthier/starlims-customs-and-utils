// region QVA 2024-04-15 - Get fields info context menu
function getHSLColors(count) {
	const colors = [];
	for (let i = 0; i < count; i++) {
		const hue = (300 / count) * i;
		colors.push(`hsl(${hue}, 80%, 80%)`);
	}
	return colors;
}

function _getFieldInfoPanel(element) {
	const controls = element.Controls.filter(c => c.DataMember);
	const baseColors = controls.map(c => [c, c.BackColor]);
	const colors = getHSLColors(controls.length);
	const pnl = Ext.create({
		xtype: 'panel',
		height: '380px',
		autoShow: true,
		width: '500px',
		draggable: true,
		closable: true,
		floating: true,

		listeners: {
			close: function () {
				controls.forEach((c, i) => {
					c.BackColor = baseColors[i][1];
				});
			},
		},

		title: `Table: ${element.DataSet?.Tables?.[0]?.TableName ?? 'Unknown'}`,
		titleAlign: 'center',
		style: {
			border: '1px solid #d0d0d0',
		},
		layout: {
			type: 'vbox',
			pack: 'center',
			align: 'stretch',
		},
		items: [
			{
				xtype: 'label',
				html: `DataSource: ${/Provider=([A-z_]+\.[A-z_]+)&Type/.test(element.Data) ? RegExp.$1 : 'Unknown'}`,
				padding: '0 0 0 16',
			},
			{
				xtype: 'grid',
				autoSize: true,
				id: 'fieldInfoGrid',
				reference: 'fieldInfoGrid',
				store: Ext.create('Ext.data.Store', {
					fields: ['id', 'datamember'],
					data: controls.map((c, i) => ({
						id: `${c.Id}`,
						datamember: c.DataMember,
						currentValue: c.value ?? c.el.dom.innerText,
					})),
				}),
				columns: [
					{ text: 'Id', dataIndex: 'id', flex: 1 },
					{ text: 'DataMember', dataIndex: 'datamember', flex: 1 },
					{ text: 'Current value', dataIndex: 'currentValue', flex: 1 },
				],
				padding: 16,
				layout: 'fit',
				height: '100%',
			},
		],
	});
	controls.forEach((c, i) => {
		c.BackColor = colors[i];
	});
	const grid = pnl.items.items[1];
	grid.store.getData().each(record => {
		grid.view.getRow(record).style.backgroundColor = colors.shift();
	});
}

function _getFieldInfoPanelContextMenu() {
	const menu = new Menu();
	const item = new MenuItem();
	item.Text = 'Get fields info';
	item.fn = _getFieldInfoPanel;
	menu.Items.Add(item);
	return menu;
}

const _fieldInfoPanelContextMenuClick = (sender, evt) => {
	evt.menu.fn.apply(evt.menu, [sender]);
};

Ext.override(StarlimsPanel, {
	initComponent: function () {
		this.callParent(arguments);
		if (this.Controls.filter(c => c.DataMember).length === 0) return;
		this.ContextMenu = _getFieldInfoPanelContextMenu();
		this.OnContextMenuClick = _fieldInfoPanelContextMenuClick;
		this.OnContextMenuPopup = function (sender) {
			if (!this.Data) return false;
			return true;
		};
	},
});

Ext.override(StarlimsGroupBox, {
	initComponent: function () {
		this.callParent(arguments);
		if (this.Controls.filter(c => c.DataMember).length === 0) return;
		this.ContextMenu = _getFieldInfoPanelContextMenu();
		this.OnContextMenuClick = _fieldInfoPanelContextMenuClick;
		this.OnContextMenuPopup = function (sender, evt) {
			if (!this.Data) evt.menu.close();
		};
	},
});
// endregion
