// region QVA 2024-04-23 - Custom Default App
Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
	Ext.override(STARLIMS.view.main.MainController, {
		init: function () {
			window.DA1A7E405C3048DDA9B8B00019D0F21D_mainController = this;
			const oConsole = this.lookupReference('app-console');
			const oNotifications = this.lookupReference('app-notifications');
			oNotifications.hidden =
				oConsole.hidden =
				this.lookupReference('app-breadcrumbs').hidden =
				this.lookupReference('app-topbar').hidden =
					Starlims.DebugMode;
			if (!Starlims.DebugMode && !this.redirectToForm && !Starlims.Navigator.Variables.Get('sessionislocked')) {
				const defaultAppCio = localStorage.getItem(`defaultAppCio-${Starlims.Navigator.Variables.myusername}`);
				if (defaultAppCio) {
					lims.CallServerAsync('Console.GetItemDetails', [defaultAppCio], res => {
						if (res === 'Unauthorized' || res.length < 3) {
							this.onHomeClick();
							return Dialogs.MessageBox(
								'You are not authorized to access the configured default application, edit your settings or contact your administrator.',
								'Unauthorized',
								'OK',
								'ERROR',
							);
						}
						const parentCaption = res[0];
						const itemCaption = res[1];
						const args = res[2];
						const FormId = res[3];
						window.formFound = true;
						Shell.navigateToForm(FormId, args);
						Shell.lookupReference('app-breadcrumbs').getViewModel().setData({
							catname: parentCaption,
							appname: itemCaption,
						});
					});
				} else this.onHomeClick();
			}
			const bConsoleCollapse = Starlims.Navigator.Variables.Get('HTML_CONSOLE_COLLAPSE') || !!Starlims.Portal;
			if (bConsoleCollapse && !oConsole.hidden) {
				oConsole.getViewModel().set('consolePinned', false);
				this.hideConsole(oConsole);
			}
			const bNotificationsCollapse =
				Starlims.Navigator.Variables.Get('HTML_NOTIFICATIONS_COLLAPSE') || !!Starlims.Portal;
			if (bNotificationsCollapse && !oNotifications.hidden) {
				oNotifications.getViewModel().set('consolePinned', false);
				this.hideConsole(oNotifications);
			}
			window.Shell = this;
		},
	});
});
// endregion
