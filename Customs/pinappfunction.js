/**
 * Requirements: Having the consoles-newtab.js snippet installed in the environment.
 * 
 * The pinAppFunction variable should be changed to modify the behavior of the open in new tab button.
 * In this example, we are changing the behavior to match the one of the NEWTAB opening mode.
 * 
 * We check if the isPortal variable is set to true, if it is, we use the original code for safety. Otherwise, we use the new code.
 * The new code calls the Shell.getOpenAsNewTabCall function to get the required function call to make to open the new tab.
 */

// Original code:
var pinAppFunction=["var hostRoot = window.location.href.replace('#', '');","var hostRootIndex = hostRoot.toLowerCase().indexOf('/starthtml.lims');","if (hostRootIndex >= 0) ","hostRoot = hostRoot.substring(0, hostRootIndex);","window.open(hostRoot + '/starthtml.lims?FormId=",record.data.applicationNavigateTo+"&cio="+record.data.origrec+"&oint=true"+isPortal+"', '_blank');","event.stopPropagation(); return false;"].join('');

// New code:
var pinAppFunction=isPortal?["var hostRoot = window.location.href.replace('#', '');","var hostRootIndex = hostRoot.toLowerCase().indexOf('/starthtml.lims');","if (hostRootIndex >= 0) ","hostRoot = hostRoot.substring(0, hostRootIndex);","window.open(hostRoot + '/starthtml.lims?FormId=",record.data.applicationNavigateTo+"&cio="+record.data.origrec+"&oint=true"+isPortal+"', '_blank');","event.stopPropagation(); return false;"].join(''):Shell.getOpenAsNewTabCall(record);
