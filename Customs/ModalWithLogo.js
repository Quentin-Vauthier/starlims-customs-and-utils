// region Add an image logo to ShowDialog
Ext.override(StarlimsInnerForm, {
	_ApplyText: function (value, justText) {
		const parentWnd = this._GetParentWindow();
		const frame = this.up('StarlimsFrame');
		if (document.title === 'Remote Form' && value) {
			document.title = value;
		}
		value = Ext.String.htmlEncode(value);
		if (parentWnd) {
			value = {
				// Change the image URL and height as needed
				html: `<div style="display:flex; gap: 1rem; margin-left: 8px; margin-top: 3px">
                <img height="30px" src="RUNTIME_SUPPORT.GetImageById.lims?ImageId=00000000-AAAA-1111-BBBB-222222222222" />
                <div style="display:flex; align-items: end; font-size: 0.8rem; line-height: 50px">${value}</div>
              </div>`,
			};
			parentWnd.setTitle(value, justText);
		} else if (frame) {
			frame._OnRetitleRequest(this, value);
		} else {
			if (this.getBubbleParent()) {
				this.fireEvent('formtextupdate', this, value);
			} else if (window.Shell) {
				window.Shell.onFormTextUpdated(this, value);
			}
		}
	},
	_SetText: function (value) {
		if (this.m_dto) {
			const textEl = document.createElement('p');
			textEl.innerHTML = value;
			const justText = textEl.textContent;
			this.m_dto.Text = justText;
			if (!this.rendered) {
				this.on('render', _ => this._ApplyText(value, justText), this, {
					single: true,
				});
				return;
			} else {
				this._ApplyText(value, justText);
			}
		}
	},
});
Ext.override(Ext.window.Window, {
	setTitle: function (title, justText) {
		var me = this,
			oldTitle = me.title;
		if (title !== oldTitle && me.headingEl) {
			me.headingEl.setHtml(title);
		}
		this.setHeaderConfig(title, 'title', 'setTitle', false);
		this.title = justText ?? title;
	},
});
// endregion
