// region bug reports
Ext.override(StarlimsAppFunctions, {
	ShowError: function (exc) {
		if (Ext.MessageBox && Ext.MessageBox.show) {
			Shell.LaunchApp('BugReports.ErrorViewer', { error: exc }, 'MODAL');
		} else {
			this.callParent(arguments);
		}
	},
});
// endregion
