# QVA Utils

## Author : Quentin Vauthier

This file contains a bunch of useful code snippets, information and tips that can be used in Starlims

### Table of Contents

- [How to get the client timezone in JS](#how-to-get-the-client-timezone)
- [Timezones used in Reports](#timezones-used-in-reports)
- [Use parameterized SQL queries](#use-parameterized-sql-queries)
- [Execute SQL Query in the Dictionary](#execute-sql-query-in-the-dictionary)
- [Refresh left console or notifications](#refresh-left-console-or-notifications)
- [Add a button to show the mobile qr code in the topbar](#add-a-button-to-show-the-mobile-qr-code-in-the-topbar)
- [Toggle narrow grid rows](#toggle-narrow-grid-rows)
- [Snippet to find element automation id by clicking on it](#snippet-to-find-element-automation-id-by-clicking-on-it)
- [Reduce console caption font size](#reduce-console-caption-font-size)
- [Fix DatePicker not showing years](#fix-datepicker-not-showing-years)
- [Unlock a system protected category](#unlock-a-system-protected-category)
- [Add a user to all service groups of a site (datasource)](#add-a-user-to-all-service-groups-of-a-site-datasource)
- [Add external libraries to your page](#add-external-library-to-your-page)
- [Enable json compression in IIS](#command-to-enable-json-compression-in-iis)
- [Use regexs in SSL](#use-regexs-in-ssl)
- [Prevent datagrid column from rounding values](#prevent-dgd-column-from-rounding-values)
- [Trigger an action when user pastes a value in a field](#trigger-an-action-when-user-pastes-a-value-in-a-field)
- [Remove splashscreen blank window delay (old)](#remove-splashscreen-blank-window-delay)
- [Start Process in SSL](#start-process-in-ssl)
- [Parse dates in SSL](#parse-dates-in-ssl)
- [Global variables in SSL](#global-variables-in-ssl)
- [Starlims logos in base64](#starlims-logos-in-base64)

### Content

#### How to get the client timezone

```javascript
const timezone = new Date().toLocaleTimeString('en', {timeZoneName: 'long'}).substr(12);
```

### Timezones used in Reports

Reports.HTML_ShowReport fix:

```js
// region Prepare Parameters get right timezone (Client Script Reports.HTML_ShowReport)
function PrepareParameters( reportParams ) {
    if(reportParams == null)
        reportParams = [];

    var sep = Utils.GetLocalNumberFormatInfo();
    reportParams.push(["LocalNumberDecimalSeparator", sep.DecimalSeparator]);
    reportParams.push(["LocalNumberGroupSeparator", sep.GroupSeparator]);
    
    let TimeZoneGMT=(new Date()).toLocaleDateString('en-US', {timeZoneName:'short'}).split(', ')[1];
    let TimeZone = GetTimeZonebyGMT(TimeZoneGMT) ?? TimeZoneGMT;
    reportParams.push(["PrintClientTimeZone", "#" + TimeZone]);

    return reportParams;
}
```

```js
// For more timezeones : https://gitlab.com/Quentin-Vauthier/starlims-customs-and-utils/-/blob/main/Utilities/Timezones.xlsx
function GetTimeZonebyGMT(gmt) {
    const zones = new Map([
        ['GMT','Greenwich Standard Time'],
        ['GMT+1','Central Europe Standard Time'],
        ['GMT+2','Kaliningrad Standard Time'],
        ['GMT+3','Russian Standard Time'],
        ['GMT+4','Saratov Standard Time'],
        ['GMT+5','West Asia Standard Time'],
        ['GMT+5:30', 'India Standard Time'],
        ['GMT+6','Central Asia Standard Time'],
        ['GMT+7','North Asia Standard Time'],
        ['GMT+8','North Asia East Standard Time'],
        ['GMT+9','Yakutsk Standard Time'],
        ['GMT+10','West Pacific Standard Time'],
        ['GMT+11','Central Pacific Standard Time'],
        ['GMT+12','Kamchatka Standard Time'],
        ['GMT+13','UTC+13'],
        ['GMT+14','Line Islands Standard Time'],
        ['GMT-1','Cape Verde Standard Time'],
        ['GMT-2','Mid-Atlantic Standard Time'],
        ['GMT-3','E. South America Standard Time'],
        ['GMT-4','Pacific SA Standard Time'],
        ['GMT-5','Eastern Standard Time'],
        ['GMT-6','Central Standard Time'],
        ['GMT-7','Mountain Standard Time'],
        ['GMT-8','Pacific Standard Time'],
        ['GMT-9','Alaskan Standard Time'],
        ['GMT-11','UTC-11']
    ]);
    return zones.get(gmt);
}
// endregion
```

### Uploadfile autoclose after upload

```javascript
lims.UploadFile("Enterprise_Utilities.EchoFile", onUploadComplete, {
    closeOnUploadComplete: true
})
```

### Use parameterized SQL queries

- In a Datasource

```sql
-- Default values are optional in prod but required if you want to launch the ds from designer
:PARAMETERS p1 := 'value1', p2 := 'value2';
SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE WHERE WHRFLD1 = @p1 AND WHRFLD2 = @p2;
```

- In a script

```c
:PARAMETERS p1, p2;
:DEFAULT p1, "value1";
:DEFAULT p2, "value2";
/* Multiple ways to execute the query */;
/* 1 - SqlExecute : Returns rows in a 2D Array, e.g. [[r1v1, r1v2], [r2v1, r2v2]] or [[r1v1]] for one row with one field */;
:RETURN SqlExecute("SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE WHERE WHRFLD1 = ?p1? AND WHRFLD2 = ?p2?");
/* 2 - LSearch : Returns only 1 value, make sure that your query returns only 1 value */;
:RETURN LSearch("SELECT FLD1 FROM LIMSXXXXTABLE WHERE WHRFLD1 = ? AND WHRFLD2 = ?", defaultValue, "DATABASE", {p1, p2});
/* 3 - LSelect or LSelect1 : Returns same structure as SqlExecute but parameters are passed as an array */;
:RETURN LSelect("SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE WHERE WHRFLD1 = ? AND WHRFLD2 = ?", NIL, "DATABASE", {p1, p2});
:RETURN LSelect1("SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE WHERE WHRFLD1 = ? AND WHRFLD2 = ?", "DATABASE", {p1, p2});
/* 4 GetDataSet : Returns a dataset object */;
:RETURN GetDataSet("SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE WHERE WHRFLD1 = ? AND WHRFLD2 = ?", {p1, p2}, .T., "DATABASE");
/* 5 RunSQL : Returns true if the query was successful, false otherwise */;
:RETURN RunSQL("DELETE FROM LIMSXXXXTABLE WHERE WHRFLD1 = ? AND WHRFLD2 = ?", "DATABASE", {p1, p2});
```

### Execute SQL Query in the Dictionary

- In a SQL Datasource

```sql
:DSN := DICTIONARY;
SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE;
```

- In a script

```sql
:RETURN SqlExecute("SELECT FLD1, FLD2, ... FROM LIMSXXXXTABLE", "DICTIONARY");
```

### Refresh left console or notifications

```javascript
Shell.refreshConsole() // Left console
Shell.refreshNotifications() // Right console
```

#### Add a button to show the mobile qr code in the topbar

```javascript
//region mobile qrcode button in topbar
function displayMobileQRCode() {  
 const me = this;
 Ext.create('Ext.Window', {
  width: 330,
  myParent: me,
  closable: true,
  resizable: false,
  border: false,
  modal: true,
  cls: 'apps__window',
  title: GetResource(this, 'MOBILE CONNECT'),
  layout: {
   type:   'vbox',
   pack:  'top',
   align:  'stretch'
  },
  items: [
  {
   xtype:  'container',
   cls:  'apps__window__contentContainer',
   layout: {
    type:  'vbox',
    pack:  'top',
    align:  'stretch'
   },
   items: [
    {    
     xtype:  'label',
     padding: '0 0 15 0',
     cls:  'login__mobileConnect__label',
     text:  'Scan this QR code with the starlims app'
    },
    {
     xtype: 'container',
     height: 200,
     layout: {
      type: 'hbox',  
      pack: 'center',
      align: 'stretch'
     },
     items: [
      { 
       xtype: 'component',
       listeners: {
        afterRender: function (myComponent) {
         myComponent.setHtml('<iframe scrolling="no" style="overflow:hidden; margin-left: -49px; width: 270px; height: 222px;" frameBorder="0" src="WEB_SUPPORT.MOBILECONNECT.lims"/>');          
        }
       }
      }
     ]
    }
   ]
  },
  {
   xtype:  'container',
   cls:  'login__container--padding-10',
   layout: {
    type: 'hbox',  
    pack: 'center',
    align: 'center'
   },
   items: [
    {
     xtype: 'button',      
     cls: 'login__actionButton login__actionButton--primary',
           
     text: GetResource(this, 'Close'),
     
     handler: function () {      
      this.up('window').destroy();
     }
    }
   ]
  }
 ]
 }).show();
}

Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", () => {
 tb = Shell.getView().getReferences()['app-topbar'];
 navBar = tb.items.items[0].items.items[1];
 btn = new Button();
 btn.iconCls = 'x-fa fa-mobile fa-2x';
 btn.Width= 30;
 btn.Height = 30;
 navBar.add(btn);
 btn.addListener('onclick', displayMobileQRCode);
});
//endregion
```

### Toggle narrow grid rows

- Enable the following enterprise setting : NARROW_HTML_DATAGRID_ROWS - *Note that it may not be visible in the enterprise settings list when opening from the app, but it is when launching the form from the designer*
- Add the below code to Dashboard.GetNavigatorVars before :RETURN serverVariables;
- `AAdd( serverVariables, {"NarrowHtmlDatagridRows", IIf(Empty(GetSetting("NARROW_HTML_DATAGRID_ROWS")), .F., .T.)} );`

#### Snippet to find element automation id by clicking on it

```javascript
function findAutomationId(item) {
    let automationId
    do {
        try {automationId = item.getAttribute('automation-id')} catch(e) {return null}
        item = item.parentElement
    }while(automationId === null || automationId === undefined || automationId.toLowerCase() === 'not-found')
    return automationId
}

function evtToAutomationId(evt) {
 document.removeEventListener('click', evtToAutomationId)
 alert(findAutomationId(evt.srcElement))
 evt.stopPropagation()
}

document.addEventListener('click', evtToAutomationId)
```

#### Reduce console caption font size

```javascript
document.styleSheets[0].addRule('.shell-console-leaf', 'font-size: 10.5px !important;')
```

#### Fix DatePicker not showing years

```javascript
// region QVA 2022-07-18 FNBLIMS-722 - Corrige les années qui n'apparaissent pas sur les datepicker
document.styleSheets[0].addRule('.x-monthpicker-body', 'display: flex !important;')
document.styleSheets[0].addRule('.x-monthpicker-years', 'float: right !important;')
document.styleSheets[0].addRule('.x-monthpicker-yearnav', 'display: flex !important;')
// endregion
```

#### Unlock a system protected category

- In a datasource

```sql
:DSN:=DICTIONARY;
UPDATE LIMSXXXXCATEGORY SET ISSYSTEM = 'N' WHERE CATNAME = 'CATEGORYNAME'
```

- In a script

```sql
:RETURN Sqlexecute("UPDATE LIMSXXXXCATEGORY SET ISSYSTEM = 'N' WHERE CATNAME = 'CATEGORYNAME'", 'DICTIONARY');
```

#### Add a user to all service groups of a site (datasource)

```sql
:PARAMETERS site := 'DEPT', usrnam := 'USRNAM';
insert into SERVGANALYST (DEPT, SERVGRP, USRNAM) (
    select distinct @site as DEPT, SERVGRP, @usrnam as USRNAM
    FROM SERVGANALYST 
    WHERE 1=1
        AND DEPT = @site
        AND USRNAM <> @usrnam 
        AND SERVGRP NOT IN (
            SELECT SERVGRP 
            FROM SERVGANALYST 
            WHERE 1=1 
                AND USRNAM = @usrnam 
                AND DEPT = @site
        )
)
```

#### Add external library to your page

```javascript
// Create new script element
const script = document.createElement('script');
script.src = '/path/to/js/file.js'; // URL of external file
// Append to the `head` element
document.head.appendChild(script);

script.addEventListener('load', function() {
    // The script is loaded completely
    // Do something or comment remove this listner
});
```

#### Function to wait for a report preview to be closed

```javascript
// region Function to wait for a report to be closed, used in case multiple previews are opened simultaneously (default behavior is to replace the previous preview)
async function waitForReportClose() {
 return new Promise((resolve, reject) => {
  let inter = setInterval(() => {
   if(!document.getElementById('idReport')) {
    clearInterval(inter)
    resolve();
   }
  }, 500);
 });
}
// Usage: await waitForReportClose()
// endregion
```

### Command to enable json compression in IIS

```powershell
%systemroot%\system32\inetsrv\APPCMD.exe set config -section:system.webServer/httpCompression /+"dynamicTypes.[mimeType='application/json',enabled='True']" /commit:apphost
```

### Use regexs in SSL

```javascript
// dotnet documentation
// https://learn.microsoft.com/fr-fr/dotnet/api/system.text.regularexpressions.regex?view=net-7.0
rgx := LimsNetConnect("", "System.Text.RegularExpressions.Regex", {string});
```

### Prevent dgd column from rounding values

```javascript
dgd_OnChangingCell() {
    if(dgd.rendered && dgd.RowCount > 0) {
        const col = dgd.columnManager.columns.find(c => c.Id === 'AMOUNT');
        const editor = col.getEditor();
        editor.initField();
        if(col.field) col.field.decimalPrecision = 4
    }
}
```

### Trigger an action when user pastes a value in a field

```javascript
function myPasteHandler(pasteEvent) {
    const pastedText = pasteEvent.clipboardData.getData('text');
    // Do something
}

// control is your element, e.g. a textbox
control.el.dom.addEventListener('paste', myPasteHandler);
```

### Remove splashscreen blank window delay

Serverscript -> Web_Support.StartHtml

Delete these lines:

- ~698 : `window.splashScreenStart = Date.now();`
- ~885 :
  - `window.splashScreenEnd = Date.now()`;
  - `var delay = window.splashScreenEnd - window.splashScreenStart;`
  - `delay = delay < 2000 ? 2000 - delay : 0;`
  - `delete window.splashScreenStart;`
  - `delete window.splashScreenEnd;`

- ~893: `setTimeout(function() {`
  - Remove one indent level between these lines
- ~944: `}, delay)`;

### Start Process in SSL

```c
:PROCEDURE StartProcess;
:PARAMETERS command, arguments, workpath, stdin;
:DEFAULT workpath, GlbDefaultWorkPath;
:DEFAULT stdin, {'exit'};
    :DECLARE processInfo, Process;
    processInfo := LimsNetConnect("", "System.Diagnostics.ProcessStartInfo", {command, arguments});
    processInfo:CreateNoWindow := .T.;
    processInfo:UseShellExecute := .F.;
    processInfo:RedirectStandardInput := .T.;
    processInfo:RedirectStandardError := .T.;
    processInfo:RedirectStandardOutput := .T.;
    processInfo:StandardOutputEncoding := LimsNetConnect("", "System.Text.Encoding",,.T.):GetEncoding(850);
    processInfo:StandardErrorEncoding := LimsNetConnect("", "System.Text.Encoding",,.T.):GetEncoding(850);
    processInfo:WorkingDirectory := workpath;

    Process := LimsNetConnect("", "System.Diagnostics.Process");
    Process := Process:Start(processInfo);
    :FOR stdinIndex := 1 :TO stdin:Length;
        Process:StandardInput:WriteLine(stdin[stdinIndex]);
    :NEXT;
    stdOut := Process:StandardOutput:ReadToEnd();
    errOut := Process:StandardError:ReadToEnd();
    Process:StandardError:Close();
    Process:StandardOutput:Close();
    Process:StandardInput:Close();
    Process:WaitForExit();
    exitCode := Process:ExitCode;
    result := exitCode==0;
    Process:Close();
    :RETURN {result, exitCode, stdOut, errOut};
:ENDPROC;
```

### Parse dates in SSL

Either use the `DateFromString(date, format)` function or see the following script.

```c
:PARAMETERS sDate;
:DEFAULT sDate, "2024-01-24"; /* To test your script and date format*/;
:DECLARE DateTime, culture, date;

DateTime := LimsNetConnect("", "System.DateTime", {}, .T.);
/* Culture is optional, but very useful for non standard date formats */;
culture := LimsNetConnect("", "System.Globalization.CultureInfo", {"fr-FR"});

/* Make sure to handle errors as the Parse function throws if not able to read the date */;
:TRY;
    date := DateTime:Parse(sDate, culture);
:CATCH;
    date := today();
:ENDTRY;

:RETURN date;
```

### Global variables in SSL

| Description     | Variable                |
|-----------------|-------------------------|
| WorkPath        | GlbDefaultWorkPath      |
| WorkPath Temp   | GlbDefaultTempDirectory |
| App base folder | GetAppBaseFolder()      |
| Server name     | StationName()           |
| User language   | UserLang (SSL)          |
| User name       | MyUserName              |

### Starlims logos in base64

```text
// Text + logo RGB
data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj4AAADxCAYAAADLE73kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFoxJREFUeNrs3c9rHNmBB/DKMsnBBFZLmEAmh2kbcguMDDkusXwayEXyaY5qgVkmh2DpsqeALPIHWL0QGCYGtY5hYdy6LEwIuE32uMGaU+akaR8CgTWMwmwc58dktp7qCbe7q6Wq7upWV/fnAzXytFotdfWret96vypJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMv/23/+7Zi8M7ZPVdGvYE7Ac3rALYDm8e+391f/7r/9ZSSv5g/R/n6Vb+8N/fbO3pGEnBJ2NdFt//vLL1ke9F+HhnlICi+9rdgEsVfh5kH7543cPtk/f+O637sXK/jDdOmkIOl2CwBPCzma6raXb3oeffrGSfv3n8O/k7o1TJQQEH2Dxwk8z/bKbBp9WGoBCxX8vfqsTQlAagLoLFnZWY9gJ7zu83/2Pei+ePH/5ZQiBrTTw7CsVIPgAix1+Qhh4nG7H1374/cM3f/rerRgMgl6StQLVtissDTsr8f2EwLMaH+7+9vlf9377/C+78bGtNPR0lAYQfIDlCD8rMfyEENBOw8+TNATd6wsKZ2EhyVqB2jUJPOddWRt9D/c+/8s/9v7zsz+9k/57O4S9GHqOlQIQfIDlC0BhsHMz3cIYl1Yc/xNaRVb6nha+F1pHWmkImqvAkNOV1f83tz789Ivw9fz9hCB3x3geEHyA5Q4/oSXkQfzf3tff/vbeW7/4SSMGhkG9ECiSbEB074rCTggxoVVnsIXqXPuj3ouj5y+/3O37/n4aeHZ82iD4AITws5Z+eZS8ajXpfvNHP2h9a3t9sOuoX2gFOppVV1jsylpPXo1HGtQ9+eLvh7/+/Z/XB/7m0LXV9ikDgg/QH35C60jo+upvRWl/5+c/PvrG994KLUKNET86ta6wuObOvRhkRv3+3h//+o/WL0/+FELb7sDfddt4HkDwAUaFn5UYfjYGAkTr7V/9rH+8zCi9JOsKa4+7NlCBrqzX/q408JymwWfw7zqOocd4HkDwAS4NQKGFZ3sw1Hzje2/tfOfnP76V870852sDFZo2XqAr67XX/vXv/3x08sXfzxck7NdOtx2hBxB8gDLhJwSQg5xvdb+1vd765o9+cC8ndOQ5jWHkcLArLHZlhd8TAkyjwGsdp2GnlYaeWyMC0o5FCQHBBxg3/JwvdpjXvbX/1i9+8snX3/72bsHQchZckmyBxNPk1e0jigjP3/nw0y/C77mX8/eE71uUEBB8gInDT/9ih3mBZO/tX/1sZUQgqUJYgDAsRDgqYPWSbH0eg5gBwQeoLPyEcT/NEU85/sb33tr7zs9/XHSMThHd3/zhZet3p3+7qEutm1iUEBB8gCkFoP7FDvN03vzpe0fXfvj9Mt1Yg3rPX36581HvxWUhqp0Gni2fCiD4ANMMP2H2VRj0fFG31l68/UVorWkUfOmzbrMPP/0i/PuyafMWJQQEH2Bm4SdvscNBvTT4hPt/FRn/s/9R78WT5y+/fHBJULIoISD4AFcSfkKQCbe5WLvkqd1rP/z+4Zs/fW/wVhJn3/vd6d9av/nDyyJT40PYCeN5evY+IPgAVxWA8hY7zNNOw8+TNASdtf58/pd/hNla7xT92cSihIDgA8xJ+Gkm+YsdDjobx/Pxf/x7aL3pvynqRfbSwHPfXgYm9U92AVCFj1980E6/3IzB5iIh6IQur7UCoSe81h2hBxB8gHkMP6EV53qSjcWZVC/JBjFbiRkQfIC5DT/ZrKtsTM64ukloPTJzC6jYG3YBMKXws/XutffD/zZL/vhxGnhu24vANGjxAabpkzF+5shuAwQfoI7G6arq2W2A4AMIPgCCDzCv4lifcgsO3r3RtecAwQeoqzKtPj27CxB8AMEHQPABauBZiec+sbsAwQeoszItPm5ACgg+wNIEHys1A4IPUF8lZ3YJPoDgA9RekUBzmty9oasLEHyApQg+WnsAwQdYCH8UfADBB1gW3QLPeWY3AYIPsAh6BZ6jxQcQfID6+/jFB4IPIPgAS6V7wffM6AIEH2Ch9C74ntYeQPABFsozwQcQfIBl0R0zFAEIPkDt9C74nhYfQPABFsclM7sEH0DwARZON+cxM7oAwQdYSL2cx7T2ADPzhl1QkYcnq+l/N9LtnXRrpNvq0FVtdoIP2yfp1nGVyxJ6VjAMwdJ499r7K331x2qsQxo5T+3Gr09iXdL9+MUHp2P+zvvpl90R3+7G33H+/f309+zEnwt/39Pz56WP3xZ8livshMK6nW6bIwppv/DctbgFB+nPt9Ovh2kA6s4omD2Yu31498btOfgcm/Ez7Pck/dvu1yh0T/rZ9mIoycL53RvTCiPdnJPt2DO60pPwg5yLjFx1OEGn7yevLI6yk76n4xn8TYX38Qh76d/ZrfhvapydQycwD+Uhft73SuzftYGv4TU66Zej9P20xzjmuwOvd9x3kd4vhLKd+O/NpOYEn/Erm4144K1M8CrNsy0LQDtTbgFa6T9YeM1uTnBdTT+X/Zq0ylX/2T48CSfFcEJtVRyCehdcxY5jdcHKdaPE+1mZ0d806T7uTfgZ57lX5889DSvb8bxTxWcY6qKNGFBDyNwvGPxCvdOOf89XfWG6Gx/rv/BrhJaeGLRrf7wZ41O+QlhJtxB4HlV44gkB6Gm8cme2n2czyW+tW4mfy7IK+yScnD9L99HjdGtU8aIjZnb1FMSFthG7cip9zZoGnhAgQjfRgykE1/B6D8Lrx+6oqm3Glrba11OCT3mPL6kQQ1reS7c76XY7bnfiY51LKprHws/MbV5yVUl2hfc0hsQqvN6MPr1uNebDSpVBJVbqjbrthL6xMdM+x5/9ntiNVoVOX9jcGHislnR1lWsdOBhRaEN3SCvdLuoa6cTXOB8XtDviBPEofc7NKXSx9GL4msRuzmse1vjzvKwJv5E+Z20mY7CqF/7mJyWeP2pQfn/ZDOPSQlBpV1AWV/r+ThbfehK7VaZ8sTLPoedxMrvuyfNjugqf9J0bzuuAo6SmrW6CT7lKMnzIzREVzJ3CQSV73v309TpJ1l3WyKkUqpddVd+fcB8MB5+6DADOdy9n3zdynlPHynm8wdlZl1Yzvu+8k/SD9Dlh8PMkg2rDifTWVMs78+asu2vcGUgDmnV647F7aNahp30+C6sihzH4hPdwXPfjVldXcXmzZjpns5LGaZ3JKo6byevN/ntjvx7jVvCDB/dgyNmoanxLLYSAnAWm60lowRy2kkw+g6y/zLtH1xKFnwpCxMaMA0QVDmb8N4cp5lsVv2Zn4DxZa4JPsUpyI8lvmZmscGUBZytWBLdr3npSN3lXjfsjDurlG+sTyubdGzsjyvjaWRfg+PqvFruK4tKo4jhar9MbjuNs1mb4K0NdcmecsBS304HjNDzWi5MS2vH/O/F53aSmi4/q6hr/YNurpGXmVcsPV3sSbsfPsx279BoDIWlnKfdSGM/z8GQ952p97C7AMCU2rRDyQhCLbTV0+1xyz7aLQkSlg6RnZHfGoef2ON2JeWsa9U93j/8/eBF0u64FUYtPwQM2t0KgnrLZSYNNz62+fw+2+qxUOKOpjvJC36RXscfxOBJ8lsvGhD9bm26uMWefhdCyHwPM1/q3eIG8M+JiIfzcnYrGUC08wWe84NO1S2ptd+jzfH2wbl6oXd6p7Vk46eaEwcYEr/rMcbSUJpmRtV6z91o25IUupOthUHLeStehpTQsTphuYfzdVvKqW+o0BiUXEYLPVEnVdZWNTRmssA9zKvrOUPidbFxL3eVNjZ8k+IRw6eakS3gROc7iejXt5rpV4rkh1BRusYndUKGrqRdDj2OpBGN8BJ9lMzyFPb/bspVzog1Xq127cHJnJ/i7H9gRi6WbvJryfJHNMUJv0dBznMzPysJlLgz2xjiGwnu9rtiVp8Vn+gWaeZF1zQyeQPOnZmaLFvYGHm3GBSiBfEVW9B2n5aZIV3N7zi5Ky9QTPUVH8Jk3p4LPQsg7eV50Q79WzmPbS7rv3lZ8KOCoSCAo091V4v5QRzXeby6oBJ+5czwUfNxTq16ylprm0BXixUsStHMe21zSPbhW4LhgyX384oNOUqzVpcxxVKSF6DT+7rraVHoEn3mTN7Dzgd1SK82cq6qLVyA9X9dnOPQ2lyw0NpO8BTytME6+TsHjsah7Ff3OWeuVOT9VeFNRBJ9KtHOvgB+ebNs1tTF48jwuePPR1lJfnWUtZQ9qUtEwH4p0Oa3E209cqMRaOPPYzdUr+fyD9P0+jl17CD5XLH96c5JkN2w8sIPmvvLOu+VIq+Bnf5wMd+msLcX9u7L3OOrmii0FizwluruKrMtT5CJjXru5xglja+n2WQxAzTiNH8HnyuyMOJjDTJ+nS77Gy7wbbO05Lbnydl4lv7vQeyxrzXya5A8q3bPiMpeoanbXRkW/a173wUUBKFxUf56Gn0cxBDUUK8FntrIT/aj7Na2eXRk/PHm85Lc2mMcKfDUZHphbtrUi7wp2Y6GmtofWndAy9vAktGJ+nmTdW3nv79jNdCmgaHfXyPNl7AprVPS7Zq7vxp6T2oghKLQEPU23B+MsAskrFjAsF37CDRuTWAhHpfS1s8ojqyyP0p8xFuJq5Q2MbJf83E/TzzT8TP+YrvNZYvtz+r53481Wq3R2E0RFigKVfietnE+Ty6dpr19wPBbpCpv32Vw7SbX3GFuN23a6f8Px2IqrOFOCFp9xws+rpcJHXsnESvFRWvl8lW6PzroOlmFcyHy1Yoyawt4b49VaBUPVotpP99tNM7kooVB31wXjWIp0c811pR9vQRHqi2kcNyEAHcRWoDXFTfCZdvjpJtmdcvcKFuhwAIdWoM/ieCArAM9G3qy7wzE/8xCWugOPNpZgbFf7rKzfvbGjOFFS0S6ooYATu7mKnCMP530nxFtLTCv8nAegMBjaEiuCz9TDz2kc63B+p9zjEoX0IIag+wLQVI07hb3MSXbRWn2OY9gJZfpf0v21NXDneiha4Rftgtos+NigXl1uztl3X61pdsuF7i+zjAWfmQWg9lk3QNYKFMZ89Ar8ZAg8YQyGGWHTkA0yHwyVrQk/63bOZ7sxp12Y7XiVOWrLG5t03p21Fcu0bi0mVaSiX+ufsVTiTuy1Gj8Zur3CHdjj8ded0q9pavkRfGYdgo7PugTu3rievOoKu+yKJBzwZoNVb9Ip7KPUpdXn2Vnr1qgtv5t2261YqNg43V0bExyLdQhA3XQ7vwBpT+FXbBvzI/hcZQi6H1uCrscr7IuuoA+En4pkLWiDFXhVJ5i812nWsHyGspg3bsfVIlUap7trobq5LglAWZdyktxJqr27vC4vwefKK5lebAkKBXzrgsJ94Iq7EnktMK3KPsvhk/lKLUNr1gLWHXh0TQCnwsr9tGD4WQ3dXbHLq0hrRWeR9lEYDxVCULpVFYIa1voRfOatsrl+QQuEpD6JbLzNxtBJstqVhhdpant+q49B91SnaHfXvWTBu7kKBqH+ELSVjD8eaEPRE3zmKfycng0gzQ8/qwY7T2R6rT2vPr9wIuotxOeWzdgaHOh8PvAeqlC0dSZU1EvRzVUiBLXjeKDQClS2BeiWopfPys1XW+lsxRaKwQpzPZneqP/Flb9gYfA4rrg9bZs1/dz24n7rb+UJA52PJpz+D2ddOe9ee79ToAWiUfAlW0u4D8NK2N0ku2lw0S4srbYjaPGZj0onGWo9YBwbV3yw13NhSgOdmb4q76e1lLcBiuOltkr8iHpE8JnbSiek+N7Ao2t2zFjmoXtmu6blsJ0ML72wGu/SDvMSVo7jzT+XUuzis6Co4LMQenbBhMKdxYs3lU/TZo334k5umDTQmckr7KKzuy5T60HNYXHGC+5NVpSFRSdkjM98UJAnlzeo+c4M9u3BQOBqnE0Hr2axxNkKrY/ZXeibfY+Gk3To8tpSxJhQ6O6adKZRbbu5YuB5HP99O4bBcRTtwuoqcoLPPHNFPYn8AeJhCntnBr+7lQyPhQmtPu2a7s2dZHisVBi7dGigMxWElkmW66h7N9ejvtASbip6p+z7SX9mu0R94YJ6BF1dgs8iyBvbM6uZH+2cE8zanN6/63LZQOe8AfcGOjORCrq7atvNFW8e2n9xFgLQ0/TxZsnQU+Y4/ESpy6fFp9zV/Upf5VDVazaS4aZLV9blPpPBk0dvZq0ToSw8POnk/A0hjNWze+jujf30PW0OlMtsoHP4Xn0rn68qfLnQVeE4LW+S7q5OTcvdQZK/zEY4dx2k39+Noa4zuD5R30rW4TmNZdhfs6DFp3gFGwrf06T6mUN5B4RR+8XlzTram/HfkNe6tFHzQcGjBjo3FDkmMG5lXMturthK07zkaY1Yr4QWoK/S7bN0exqD+mfJ8DjCIpZmkUfBZ1otCg9PQvPi41j4tuMMoipee3VEkDq04wsbnEVV1eyR4rLVj49zruaatd2rWYtZJ+c96fJibBN0d9Vu0cLYjTXO8RLqmUnX4NlT2gSfcYNJKIBPc1oVJr+ZaNYa8CjnO91YkXL5PmzmXAm1K+2KnOzEfK/me3gnGR6/tOGWKkxonMUMa9VtE0PPVd13MbSOtRUzwWdcp0n+yPiVs0A07uJuWWh6muQ3X0rqxU3/vlxFZdPXT4eu3OocErIbu+btTzfSZRJlQ0xngqnfVxF6wvn9qlpGy67uLPgwdOIPheh2MnpaYLiL9ePCXV+hBenhycEFoWfflOHC4TEEitWhE2q1d2Evq10wnNXpGLifDC+wGcrxfYWQcYzR3XVUw7d5VUFtx9iey5nVVST8PDwJ4efRiLCylmTTl88P5jCFsL/grcQKej25uN82dNHs2OGF5a2QfNXjAMLvH2wF3DgLvFcbyCYVriAfDwW6sNhhvd8XV6fM7K5adXOF4PHutfdvxjpjbZbHqS6uYrT4FAs/IciEgty94Fnng1nPB0Kfb6Hw7xYIPZoni8rGXjUHHu1deWtZFgLy/oa6t/p0c96Xgc5MomiYqVU3V1/4OU23cMGcN06uauG8c1PoEXymcfI/TbeqC3J2V2yhp6y8IDEvY6PyZuQ1F2Cf55VRA50ZOxgUDD9HNX+f+/GieRqh5Hyx0Zu6t8rR1VU+AO3H+xltxAp4dcwCG7pF9q9oBtK4Bq/6r+pga+T8LZ05KR/ttHyEbs3X1/AJAWF6LVKnOfujV/H76qXvIYT+9YHvbCZXu+DmVZXBaR23vRL783TO9nHZz+IouXzV+k5NysNF4Sd8plvvXnt/L14EbSaT3VC5F+uPdh1bw+bB1+yCCb26T9St5NX6Cys5J6jztV6ezOQeUgDMpb4Vmd+JdUZjRBjqxS3UHWH8aLfm9ysDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABq7f8FGAAgcw0CCzOV4AAAAABJRU5ErkJggg==
// Text + logo white
data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj4AAADxCAYAAADLE73kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAE1JJREFUeNrs3Y912si+AGD5nlsAt4LHVnBJBUsqCKkgpII4FcSpgKQCnArsVABbgb0VmFQQvwr8NJvhLYtHWP8ACb7vHB0n2AhpGGl+mr9ZBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8MvT09NYKjxLk1G+DaUEnId/SQI4nwI+/zHIf87z7eqcC/tw7vl2mW+L/L8hHUZyCAh8gBNycXFxn//4Pd9+5Ntjvi1CwZ9v03wbnEnAM8m3m/yfd/GlP2KaLOUQADjNwj8EOg+xxiPU/PyM2/wUm8JiU9YsnuNT/PdknQZyBACcfvAzioHAusZn/vS3h743hYUarBjY3W2cVzjXcfwZzn0iJwDA+QQ/g43AYB4DoM1AYR0sTHt0Tn81ZW2dw0M8t1n8/13s7wQAnGEAtK7t+Rlrei43moWeNn4372LAkGjKetpxPotz6c8EABQHD5eJGpKrp7R1/6DhEY93UFBD9bRRgzXZ+v3MNw0ArIOJ8VatyaKg6WjTzSGbwuLxzHccz7rP0vYxT33DAMB2YDFK1KLMN0ZBFdlbU1icc2f2wudvjlLbPi79eQCAwkBjkKgx2dX/pygIGTQ8hl1NWWWO605/HgCgbPAxKwhqJgW/K2oKm1T4zJeasp41s8XmrVQfH0EPAFAp+Jnu6EszKQg6impmZqlmp9iUdfVCU9Z2Tc50R4BkUkIAoHbwM9rRvDXbmAn6qULgcrmjtmZX8DTdmG069XuTEgIAjYOfwY7+Nj+3lr/Yh6sXAqwHnZgBgLaDn/kLNTlV+uiUUaZJzaSEAMDeAqDLBp2Oy3ooGUTNfSMAwL6Dn0mJZq31MPOHiv14LksOm5/6JgCAQwU/oxLz7DxU6P8zKzFRokkJAYCjBT+Dkk1aRUtJVB0af3fMNcIAALIKExrON2ZkXi+GWuW9OjEDAJ0IfqYV+/GMKwx/v5LCAEDXgp9RyWBmkVhQ9MmkhMC+/UsSAG25uLi4z3/8lm/3LexulW+v833eSllA4AN0Nfh5DAFLvl032M0y317FQAoAoPt2TEC4q6nrTsoB+6LGB9inP2u857tkAwQ+QB/VaapaSTZA4AMIfAAEPkBXxY7OjxXfs5RygMAH6KsqtT4ryQUIfACBD4DAB+iBHxX+9g/JBQh8gD6rUuPzKLkAgQ9wLoGPmZoBgQ/QXxVHdgl8AIEP0HtlAprHGCQBCHyAkw981PYAAh/gJPyvwAcQ+ADnYlnib35IJkDgA5yCVYm/UeMDCHyA/ru4uBD4AAIf4Kwsd/zOiC5A4AOclNWO36ntAQQ+wEn5IfABBD7AuVjWDIoABD5A76x2/E6NDyDwAU7HCyO7BD6AwAc4OcvEa0Z0AQIf4CStEq+p7QEEPn3z9PQ0yrerfLvJt7un537m2yLfZvk2zbeBVOMM/SgZDME5lR+DWC7MYjnx8JS2iFsoayZNypG4j6cdn7P5+9lWWff/f9fH9P63LNcss+Y/LvPtXb4NX/jz8LfjuAXz/P3X+c9vFxcXy0MEZvmPWdfSMD/31x34HqfxO9z0R35sV30Julv4blcxKAm1L/clZ1quI+T1TyWCobLnHs571Je8VjMvFvmYn9P9AY6pdBoX+Nz2PS4/pnC/nZ/IvedDhfQdb/0M+7jNf3zPz+e6xjW/3NpfyE+P2fNa2EnIb/Hf7zLONuiZxFqcNsz3XQOU73/81EEd+S4fCmroBj3Ji/v4bh/i0+ew5WMdJp4oxw32t+hTXmv4FL5tfKBjWjS9v+0jGOvzvSf/+MsWy4/Ne9ZlzeN5lqcSeXEUX7/re42Ppq4atTzxQr6JtThtCFH/3TpjcfAnrmFBDd30jJMmpEm4iT7Egq+VAKigJmklJ560yR4eIiY9vd+EwP8u+1VD23aahP3NYmCyj7LkXbwP9L6cEvhUt3ihQFzm2+d8e5tvr+P2Nr52+0JBsxD8HNyuatsPkucv4xiYtxUI3pcIhjgdgzYDlXiPHPYtEeJx3x0gcBi1fL3ebgSbk63Xekkfn2oZd16QaUOb6Nd8+7JjWO5t3Me6X9CnghtE6Bz9ag/De1cx+GriU2Kf33r8fY6yjbbyVDAaqn4P0QdrD8Ix/1Hh7/8bC5PRjsIrNMlmNfoSpPLiYOM4OX1v8u26pX31ro9JvNcssvZreV66ptvw58a9YV0GfM96WutGtYw72dH7fVDnQijoW7Loat+S1LH2PZBN9GvZdtOD80j18bmqua9hbNv/uaMfwajh8V5t9PGZN9yXPj77P6ZFS31QBi0dz88+9S+M19TPA3efnNe8t48TefEq9klau9u45+jjc+JSo2Zuw8iAOrUzcTTGq+yf1f6f6+6P6jej7HmT5bdEDcSk7Q6+XRaaneJott/y7UtBzU/TEWSbed4aXedj0sJ1OzlwrUkb5gc+5mV+Db9veZ+3W/fJXhP4lL/Ytgu/Vb41ylwxwHkfC4LXfRk+fSKmide+FFzUZ9fXJ+TNfPtYkMfHDWsbVps3aVnxbLRxHb3pWdkR7jPjA35kKEve1gmW4va4dZ2G11axH951/P9t/LtlZvLRkw585omqxOkZpsPJNHUlqp7nG7/bbvL62fFzaa2pq2D/N203AW40dQ1b2I+mrv0e06LFJphhg+MYtNlkdKC0ezhg89adiXHV+LRplHgivpYsvQ16QtC6fYP4uvHv7VqfwTkGuhs+pmp9WngyNaLr/Ewavrc3BXvN0WehJiXUPIcWgH/IfnWN+Jilp38I73urm4TAZ5+Bz1KS9Nr26LTl1gy4qaD2bIe2x+BkmQgGm9TW/HAdnaUmI7Le9OxcqwZ5oQnpt9DEnBpJGu5R+RZGDof+d++zv5ulHmOg5CFC4LNXouqeis0D2wX2t0RBvz1PxehQTQsdlRoa3yTwCcGl/gFn+BBZZ1RgbMLp2/Dp3yv8bQhqStfYxBaHMEfcKgY9riWBj8CHQts1N6uCZsuvLT+t8s8b96Nq+ZOzLHlvrHMdlQ16uhQAVHkw+FzjGgrB0m+CHoFPFzM0HRGbZrZvoN8KbirL7Hlb+lTnQdipzIy+dWpuyjQ1X3fsobRKObGSdQQ+XfMo8DkJqZvnlx1/n6r1uTzTtPsf2YcSvpcJCKo0d1VYH+p7j9PNA5XAp3Pum1y4HF+sqZluPyG+0NxynXjtXJu7xiWuC85cfj3dZu03d5WpIXqMn91XmtEFPp2T6tg5kyy9Mk08VX174Sb+mAh+huc2tL1gBfuVPjoUuC15PZb1oaXPPLRVlfvTmU+ZIfDpoNSTf5g07lLS9Mb2zfO+5OKjZ93JOdaUzXpS0NANZZqcBnFG/JfyX9m5cLrYzLWq+PfzOFnkUBYS+BxdwfDmYNZ0kUUOUninlhz5WvK7D80594mgd3gG6RbOsWhF6a9yFgXXTNnmrjLz8pR5yOhqM1edYGycbw8xADKYQuBzdB8LLubperVaSdRZHxI3yusK708V8p9OPOgJtZl3WbpT6WeTpfGCtkZ3TVr6rK6mwa4AKDxU/4xLxkzVBAl8jvEUs8rSU/dnsXBYrKN0qdWpAnyUPe+YW7W2IvUEOzmlp7FwUw01Y/k2i2uTheat1PndW0yXEso2d0135MlUTW3dzzpWmXHdwq4mMQh6iA/ZM4NrOHQBMS2xWNzPuLDp5MTOvXeLlBYsMDussZ9ZYj+XHTnH8bkugmiR0k6l8WLrfWUWFL2peO0+7VpAuGv5oe2FVRPXowdtNT4HieJDBL+eKrzwSSb7NWphvap1+HmpqvLghUjREPZVjd2laonOaf2usEbQKyO5qKBUc9eOYLrMg+N1lxMgXi+hvNjHdRNqfea6Wgh8DpWZl9mvlXI/l8zQ4QIOTQfrqkqd1g4jVSPzreZ3HoKl5dbLwzO44YSCJQQ8H2UnKirbBDVJPLSUXYn9W9cTIQ6Q2Ffwsw6AQk2XKVYEPvuP5GNfh/VKufcVMum6vfZKALRXdYewV7nJnlqtz30MdkKe/k+eXu+tBUTNe2TZzr3vSr62bdWXvBmPM5QV++yIfWmUMQcXOpzFviAPFdppH/pSa9CnPj4FfbGmLez3oY0+Qy2fa6qPzzy+XrSl+izNenjN6ePTnTReJN57U/K9w433DEq+Z9bH/BCvv8Ue++Kp+eGoQdBVbNoqY9qDc+pT4HO3qxNkywXT7Mjnmgp8rl54T1Gny1HPrjOBT7cDn2nJ917WeM+oz/khXrfzPQU/44xCmrr2JFRthqaw0CE0+1XFGRbD3NXGO9dDv70bSvZ8/pnrlnaf2s+0h/kz5MVUvx1Pi7SpTnPXSTVz7bgGl6EpOf/nf/Ltbdbu6vKavAQ+R8/g4SL9mG8hg7/fkbnn5mdoRarfzde2vsvEzXzQx6A1jlBcbr08FoDTcoBdJvgZxbmkhll6Qdy6AVUv0ij0h4r96doKgiykLfDpXGHz244aCJF6A/HGuT1K5LblmYZPaWh7stZHp3taVHZ014es3BD24NupJtZWEPQ+8XBS1kTWE/h0LcJ/XxD8jLTPNrK32p6N7y/ciFan8L3F5oIvWy+HoOeTrERLytbOhIL6LJq5qjwo51sYCh9qgarWAP0u6wl8upipi6L5N1KnuoIJC4PWR1Bk6an0+7pqe2o+qksBOG096JUMfsI1VaZ55usZpmFIv9BSUCXgU2sr8Ol0obNN22w9kyNf7L2cmFJHZw6gzfW0bs8xAeN1+r7CW5QjAp/OZuZl9rzZxJN2PV1onrnsaT68TjxNjrqyHhm911awct9yf72+Xaf3WbVaHwQ+nbWSBM1UWMl53971OBlTtT6fdHSmhQK7bHPXS3rdqTnOn9X0erJeXkP/lgSdICM3l+rU/PYAaTvfCrjCMNJprEHpW+G0zI89HPd04+Vwkw5NXu9lMRoKzV1NRxr1tpkrBjyL+O/XDRb8LduEtZTlBD5d5om62Q0lBB7j7RtkhbWCmnz21+x5X5hQ63Pd0+T8mD3vKxX6Ln1ruM4Z3GbNpuvoezPXzUbQEgZcvK16PrHpuWx54YG6gKYugc8pSPXtOdTIj+vEDWZ87PW76opPoakO9zo600beavIw0ttmrrh46ObDWQiA7qpMFhqDnirX4Z9yncCnjcw7aLu/Qywgt6suPVlX+E6y50PYV4eqndhxM+/tPDj5OYV5fU6uo7O1kDqhyeiu257mu3mWnmYj3LvmceHjq4K1x4Zx7bKHGg8ft7KbwKdp5g03urs9FGipC0Kv/fJShfHnAx9DqnZp0vNOwUUdnYeyHEcIXnrZzBUfFqYv/NkwlivrhZUf1oss56+FgGe7H2EZZzPJo8BnTzUKcfXtRcx8l3EEURv7HhUEUt+kfGnbo6jaGj1SpYYkNcR0kPVw8dKNc1om0nGQafKiWb6qe332btLC2IxV53oJ5UzTOXg+y20Cn7oZN2TAu0StQuPFRGNtwE3iV0uReqUby/aT0HWD0RJNnNL6XWsfs+f9lyaaeWioTnNXr5pt4r3pWOsu3vdxVKnApzses3TP+BC03NXt8xCDprssXX0pUi9v7+tyVXiSvU7klWGfg4TYtJBKTwvp0kTVIOb2SA8zdYOecH8/Vs1o1dmdBT48u/GHTPQ6Kx4WGFaxXpRt+ood1eY7gp4vhgyXvrmEgGKUuEGujnhY1yWDsz5dA1fZ8wk2Qz6+kgtpcF+tEvx87+FpHitQ+6jFQODTZvBTVKCGAvgmLwh+hqAm1AKFQnljm8Qe+yHYCR3VpkWFZv5ZH6V4aakZko/dD6Cok/Ow52mdeoL8oKMzDVQJZnrVzBUDj1fZ4UfnvtfERds1DINYu7MP856kwbbFkY5jmDiWh46kUSqPzPb8mePEZ14d4LxuOprehzDe0/lcdW1IfYU0XlS8n5Zxs6/8cKC0u4wPxfv00LTPqRofCmt+8i3U/KQ6fNb116rY+X61yVaTaj7qSt+o1Ii86Qmk+fuC2qyx7Eid+2nJmpzvPT/PL7H2Zx81MevJRl9p3qrGkhU1MnJcz2gSC+BRzQwbmkW+9KnTXva86vZYF9swcSy3Hckf13n+eJNtzcYdAoQ99t96TKTHquXzWuXnEIL+N1u/epcdd8LNY+XBfV23qwrp+dixNK76XXzPXp61/rYn+WHntRMeHPLr53N8CHqXNVtQeRXLj+uelR/d+U4kQTMb60T9nv09/8IgcYNaz/XyxyHWkAKg8+XGf2OZMSwIhlZxC2VHWIJi2fP1ygAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgF77PwEGAImDz8+8vg3FAAAAAElFTkSuQmCC
```
