/**
 * Allow to fetch a client script code by its name
 * @param {string} name Example : "Enterprise_Utilities.HTML_ContextMenu"
 * @param {boolean} doResolveIncludes If the script contains #include statements, resolve them recursively
 * @returns The client script code (with includes resolved if doResolveIncludes is true)
 */
async function getClientScriptByName(name, doResolveIncludes = true) {
	const source = `Runtime_support.GetClientScriptByName.lims?ScriptName=${name}`;
	const script = await Utils.GetTextAsync(source);
	if (!doResolveIncludes) return script;
	const includePattern = /^#include\s+[\"<](?<cs>\w+\.\w+)[\">]\s*\r?$/gm;
	const resolved = await resolveIncludes(script);
	return resolved;

	function resolveIncludes(script) {
		return new Promise(async resolve => {
			const includes = script.match(includePattern);
			if (!includes) {
				resolve(script);
				return;
			}
			const includeProms = [];
			for (const include of includes) {
				const includeSource = includePattern.exec(include).groups.cs;
				includeProms.push(
					Utils.GetTextAsync(`Runtime_support.GetClientScriptByName.lims?ScriptName=${includeSource}`).then(
						code => {
							script = script.replace(include, code);
						},
					),
				);
			}
			await Promise.all(includeProms);
			const remainingIncludes = script.match(includePattern);
			if (!remainingIncludes) resolve(script);
			resolve(resolveIncludes(script));
		});
	}
}
// Adds a function to get a client script code by name to the lims object (also available globally as `getClientScriptByName`)
lims.GetCS = getClientScriptByName;

(async () => {
	async function addCssRule(rule) {
		if (!document.bootstrapStyleSheet) {
			const styleEl = document.createElement('style');
			document.head.appendChild(styleEl);
			document.bootstrapStyleSheet = styleEl.sheet;
		}
		document.bootstrapStyleSheet.insertRule(rule);
	}

	// region VLE - 20200730 - Fix dropdowns width issue - Ticket #Z00002399B0008
	// Make the combobox list of items fit the width of the longest item (not the combobox itself)
	addCssRule('.x-boundlist { min-width: max-content !important; }');
	// endregion

	// region Place images in datagrid cells vertically in the middle
	addCssRule('.x-grid-cell-inner > img { vertical-align: middle !important; }');
	// endregion

	// region Align checkboxes in the center of narrow rows (uncomment only for datagrid that have narrow rows active)
	// addCssRule('.x-grid-checkcolumn { top: 2px !important; }');
	// endregion
	// region fix narrow rows on grids with a checkbox column (uncomment only for datagrid that have narrow rows active)
	// addCssRule(`
	// 	.starlims-narrow-grid-row .x-grid-checkcolumn-cell-inner {
	// 		padding: 0 !important;
	// 	}
	// `);
	// endregion

	// region QVA Display loading message each time ShowModalDialog is called to prevent any other action
	Ext.override(StarlimsForm, {
		ShowModalDialog: async function ShowModalDialog(url, args, onCloseCallback, windowConfig) {
			const me = this;
			try {
				this.mask('Loading...');
			} catch (ign) {
				/* empty */
			}

			if (typeof onCloseCallback !== 'function' && !windowConfig) {
				windowConfig = onCloseCallback;
				onCloseCallback = null;
			}

			if (onCloseCallback) {
				return this.callParent([
					url,
					args,
					() => {
						onCloseCallback();
						try {
							me.unmask();
						} catch (ign) {
							/* empty */
						}
					},
					windowConfig,
				]);
			}

			const ret = await this.callParent([url, args, null, windowConfig]);
			try {
				me.unmask();
			} catch (ign) {
				/* empty */
			}
			return ret;
		},
	});
	// endregion

	// region QVA 2022-05-03 - Adds a title on empty caption MessageBox for them to be draggable
	Ext.override(Dialogs, {
		statics: {
			MessageBox: function (text, caption = ' ', theButtons, icon, defaultButton, theCallBack) {
				return this.callParent(arguments);
			},
		},
	});
	//endregion

	// region Dims mask background opacity
	addCssRule(`
		body > .x-mask {
			background-image: none; background-color: rgba(0, 0, 0, 0.4) !important;
		}
	`);
	// endregion

	// region QVA fix for when a cell is clicked then hidden, caused an error
	Ext.override(Ext.view.Table, {
		privates: {
			activateCell: function (b) {
				var a = this,
					g = b.view !== a ? a.lockingPartner : null,
					h = a.grid.actionables,
					n = h.length,
					l = a.getNavigationModel(),
					k = b.target,
					m,
					i,
					f,
					e,
					c,
					j,
					d;
				b = b.clone();
				m = b.record;
				b.view.grid.ensureVisible(m, {
					column: b.column,
				});
				f = a.all.item(b.rowIdx, !0);
				if (a.actionPosition) {
					i = a.all.item(a.actionPosition.rowIdx, !0);
					if (i && f !== i) {
						Ext.fly(i).saveTabbableState({
							skipSelf: !0,
							includeSaved: !1,
						});
					}
				}
				a.activating = !0;
				if (!g) {
					e = Ext.fly(b.getCell(!0));
					a.actionPosition = b;
					for (c = 0; c < n; c++) {
						j = j || h[c].activateCell(b, null, !0);
					}
					e = Ext.fly(b.getCell(!0));
				}
				if (
					g ||
					(e &&
						e.restoreTabbableState({
							skipSelf: !0,
						}).length | (d = e.findTabbableElements()).length) ||
					j
				) {
					for (c = 0; c < n; c++) {
						if (h[c].activateRow) {
							h[c].activateRow(f);
						}
					}
					if (!b.column.Visible) return;
					if (g || d.length) {
						Ext.fly(f).restoreTabbableState({
							skipSelf: !0,
						});
						if (g) {
							a.actionableMode = !0;
							a.actionPosition = null;
							a.activating = !1;
							return !0;
						}
						if (d) {
							a.actionRow = a.actionRowFly.attach(f);
							a.actionableMode = a.ownerGrid.actionableMode = !0;
							l.setPosition();
							l.actionPosition = a.actionPosition = b;
							if (k && Ext.Array.contains(d, k)) {
								Ext.fly(k).focus();
							} else {
								Ext.fly(d[0]).focus();
							}
							a.activating = !1;
							return !0;
						}
					}
				}
				a.activating = !1;
			},
		},
	});
	// endregion

	// region fix url matcher validator
	Ext.override(Ext.data.validator.Url, {
		config: {
			matcher:
				/^(https?|ftp):\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
		},
	});
	// endregion

	// region Element.SetDataSetAsync functions
	// region glboal datasetasync functions
	const globalSetDataSetAsync = function (map) {
		return async function (dsName, parameters) {
			// Store the parameters to prevent multiple calls ambiguity
			map.set(this, parameters);
			// Mask the control, if it throws an error, log it and continue as it should not stop the process
			try {
				this.mask('Loading...');
			} catch (err) {
				console.warn('Error masking element', err);
			}
			const thisObj = this;
			// Convert the parameters to string to mimic the behavior of lims.GetDataSource
			await lims
				.GetDataSetAsync(
					dsName,
					parameters?.map(p => p?.toString()),
				)
				.then(ds => {
					if (thisObj.isDestroyed) return; // if the grid is destroyed before the async call is finished
					if (map.get(thisObj) !== parameters) return; // Prevent multiple calls ambiguity
					// Assign Data
					thisObj.DataSet = ds;
					thisObj.m_dto.Data = lims.GetDataSource(dsName, parameters);
					// Same as mask above
					try {
						thisObj.Enabled === false ? thisObj.mask() : thisObj.unmask();
					} catch (err) {
						console.warn('Error unmasking element', err);
					}
				});
		};
	};
	const globalSetDataAsync = function (map) {
		return async function (dsName, parameters) {
			map.set(this, parameters);
			try {
				this.mask('Loading...');
			} catch (err) {
				console.warn('Error masking element', err);
			}
			const thisObj = this;
			await lims.GetDataAsync(dsName, parameters).then(data => {
				if (thisObj.isDestroyed) return; // if the grid is destroyed before the async call is finished
				if (map.get(thisObj) !== parameters) return; // Prevent multiple calls ambiguity
				thisObj.Data = data;
				try {
					thisObj.Enabled === false ? thisObj.mask() : thisObj.unmask();
				} catch (err) {
					console.warn('Error unmasking element', err);
				}
			});
		};
	};
	//endregion

	// region allows changing datagrid data asynchrounously easily
	const dsLastSetDataSetAsyncArgs = new Map();
	StarlimsDataGrid.prototype.SetDataSetAsync = globalSetDataSetAsync(dsLastSetDataSetAsyncArgs);
	StarlimsHDataGrid.prototype.SetDataSetAsync = StarlimsDataGrid.prototype.SetDataSetAsync;

	const dsLastSetDataAsyncArgs = new Map();
	StarlimsDataGrid.prototype.SetDataAsync = globalSetDataAsync(dsLastSetDataAsyncArgs);
	StarlimsHDataGrid.prototype.SetDataAsync = StarlimsDataGrid.prototype.SetDataSetAsync;
	// endregion

	// region allows changing datagrid column data asynchrounously easily
	const dsColLastSetDataSetAsyncArgs = new Map();
	StarlimsDataGridColumn.prototype.SetDataSetAsync = async function SetDataSetAsync(
		dsName,
		parameters,
		maskGrid = false,
	) {
		dsColLastSetDataSetAsyncArgs.set(this, parameters);
		// Mask the entire grid if needed
		if (maskGrid) {
			try {
				this.Parent.mask('Loading...');
			} catch (err) {
				console.warn('Error masking element', err);
			}
		}
		// Disable the column while loading
		this.disable();
		// Create stylesheet if not already created
		if (!window.dgColStyleSheet) {
			window.dgColStyleSheet = document.createElement('style');
			document.head.appendChild(window.dgColStyleSheet);
		}
		// Add loading style to the column
		window.dgColStyleSheet.sheet.insertRule(
			`.x-grid-cell-${this.DataMember} { background-color: rgba(209, 209, 209, 0.5)}`,
			0,
		);
		const rule = window.dgColStyleSheet.sheet.cssRules[0];

		const thisCol = this;
		await lims
			.GetDataSetAsync(
				dsName,
				parameters?.map(p => p?.toString()),
			)
			.then(ds => {
				// if the column is destroyed before the async call is finished
				// Prevent multiple calls ambiguity
				if (thisCol.isDestroyed || dsColLastSetDataSetAsyncArgs.get(thisCol) !== parameters) {
					// Remove loading style from stylesheet
					const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
					window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
					return;
				}
				thisCol.DataSet = ds;
				thisCol.m_dto.Data = lims.GetDataSource(dsName, parameters);
				// Remove loading style
				const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
				window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
				if (maskGrid)
					try {
						this.Parent.Enabled === false ? this.Parent.mask() : this.Parent.unmask();
					} catch (err) {
						console.warn('Error unmasking element', err);
					}
				thisCol.enable();
			});
	};
	StarlimsDataGridCheckBoxColumn.prototype.SetDataSetAsync = StarlimsDataGridColumn.prototype.SetDataSetAsync;
	const dsColLastSetDataAsyncArgs = new Map();
	StarlimsDataGridColumn.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters, maskGrid = false) {
		dsColLastSetDataAsyncArgs.set(this, parameters);
		if (maskGrid)
			try {
				this.Parent.mask('Loading...');
			} catch (err) {
				console.warn('Error masking element', err);
			}
		this.disable();

		if (!window.dgColStyleSheet) {
			window.dgColStyleSheet = document.createElement('style');
			document.head.appendChild(window.dgColStyleSheet);
		}
		window.dgColStyleSheet.sheet.insertRule(
			`.x-grid-cell-${this.DataMember} { background-color: rgba(209, 209, 209, 0.5)}`,
			0,
		);
		const rule = window.dgColStyleSheet.sheet.cssRules[0];

		const thisCol = this;
		await lims.GetDataAsync(dsName, parameters).then(data => {
			if (thisCol.isDestroyed || dsColLastSetDataSetAsyncArgs.get(thisCol) !== parameters) {
				const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
				window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
				return;
			}
			thisCol.Data = data;
			const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
			window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
			if (maskGrid)
				try {
					this.Parent.Enabled === false ? this.Parent.mask() : this.Parent.unmask();
				} catch (err) {
					console.warn('Error unmasking element', err);
				}
			thisCol.enable();
		});
	};
	StarlimsDataGridCheckBoxColumn.prototype.SetDataAsync = StarlimsDataGridColumn.prototype.SetDataAsync;
	// endregion

	// region allow changing combobox data asynchrounously easily
	const cbLastSetDataSetAsyncArgs = new Map();
	StarlimsComboBox.prototype.SetDataSetAsync = async function SetDataSetAsync(
		dsName,
		parameters,
		selectFirstItem = false,
	) {
		cbLastSetDataSetAsyncArgs.set(this, parameters);
		this.disable();
		this.suspendEvents();
		this.getStore().insert(0, [['Loading data...', null]]);
		this.select('Loading data...');
		const thisCombo = this;
		await lims
			.GetDataSetAsync(
				dsName,
				parameters?.map(p => p?.toString()),
			)
			.then(ds => {
				if (thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
				if (cbLastSetDataSetAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
				thisCombo.getStore().removeAt(0);
				thisCombo.select(null);
				thisCombo.resumeEvents();
				thisCombo.DataSet = ds;
				thisCombo.m_dto.Data = lims.GetDataSource(dsName, parameters);
				if (thisCombo.Items.length) thisCombo.Items[0].Selected = selectFirstItem;
				thisCombo.Enabled && thisCombo.enable();
			});
	};

	const cbLastSetDataAsyncArgs = new Map();
	StarlimsComboBox.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters, selectFirstItem = false) {
		cbLastSetDataAsyncArgs.set(this, parameters);
		this.disable();
		this.suspendEvents();
		// Insert Loading item to show loading state
		this.getStore().insert(0, [['Loading data...', null]]);
		this.select('Loading data...');
		const thisCombo = this;
		await lims.GetDataAsync(dsName, parameters).then(data => {
			if (thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
			if (cbLastSetDataAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
			// remove loading data item
			thisCombo.getStore().removeAt(0);
			thisCombo.select(null);
			thisCombo.resumeEvents();
			thisCombo.Data = data;
			if (thisCombo.Items.length) thisCombo.Items[0].Selected = selectFirstItem;
			thisCombo.Enabled && thisCombo.enable();
		});
	};
	// endregion

	// region allows changing panel data asynchrounously easily
	const pnlLastSetDataSetAsyncArgs = new Map();
	StarlimsPanel.prototype.SetDataSetAsync = globalSetDataSetAsync(pnlLastSetDataSetAsyncArgs);

	const pnlLastSetDataAsyncArgs = new Map();
	StarlimsPanel.prototype.SetDataAsync = globalSetDataAsync(pnlLastSetDataAsyncArgs);
	// endregion

	// region allows changing groupbox data asynchrounously easily
	const gbLastSetDataSetAsyncArgs = new Map();
	StarlimsGroupBox.prototype.SetDataSetAsync = globalSetDataSetAsync(gbLastSetDataSetAsyncArgs);

	const gbLastSetDataAsyncArgs = new Map();
	StarlimsGroupBox.prototype.SetDataAsync = globalSetDataAsync(gbLastSetDataAsyncArgs);
	// endregion

	// region TabPage setDataAsync
	const tpLastSetDataSetAsyncArgs = new Map();
	StarlimsTabPage.prototype.SetDataSetAsync = globalSetDataSetAsync(tpLastSetDataSetAsyncArgs);

	const tpLastSetDataAsyncArgs = new Map();
	StarlimsTabPage.prototype.SetDataAsync = globalSetDataAsync(tpLastSetDataAsyncArgs);
	// endregion

	// region Multichoice SetDataSetAsync
	const mcLastSetDataSetAsyncArgs = new Map();
	StarlimsMultiChoice.prototype.SetDataSetAsync = globalSetDataAsync(mcLastSetDataSetAsyncArgs);

	const mcLastSetDataAsyncArgs = new Map();
	StarlimsMultiChoice.prototype.SetDataAsync = globalSetDataAsync(mcLastSetDataAsyncArgs);
	// endregion

	// region ListBox SetDataSetAsync
	const lbLastSetDataSetAsyncArgs = new Map();
	StarlimsListBox.prototype.SetDataSetAsync = globalSetDataAsync(lbLastSetDataSetAsyncArgs);

	const lbLastSetDataAsyncArgs = new Map();
	StarlimsListBox.prototype.SetDataAsync = globalSetDataAsync(lbLastSetDataAsyncArgs);
	// endregion

	// region CheckedListBox SetDataSetAsync
	const clbLastSetDataSetAsyncArgs = new Map();
	StarlimsCheckedListBox.prototype.SetDataSetAsync = globalSetDataAsync(clbLastSetDataSetAsyncArgs);

	const clbLastSetDataAsyncArgs = new Map();
	StarlimsCheckedListBox.prototype.SetDataAsync = globalSetDataAsync(clbLastSetDataAsyncArgs);
	// endregion
	// endregion

	// region QVA 2023-04-19 - override combobox and multichoice behavior to allow selecting items by typing the first letters
	Ext.override(StarlimsComboBox, {
		SAME_SEARCH_DELAY: 500, // Delay in ms to consider the same search
		_InitEvents: function () {
			this.callParent(arguments);
			this.on('keypress', function () {
				if (this.AllowTextEdit) return;
				if (event.key.length > 1) return;
				const pressedKey = event.key.toUpperCase();
				const now = new Date().getTime();
				if (now - this.currentSelectLastPress < this.SAME_SEARCH_DELAY) {
					this.currentSelectLastPress = now;
					this.currentSelection += pressedKey;
				} else {
					this.currentSelectLastPress = now;
					this.currentSelection = pressedKey;
				}
				const filteredItems = this.Items.filter(i => i.m_text.toUpperCase().startsWith(this.currentSelection));
				if (!filteredItems.length) return;
				const index = this.Items.indexOf(filteredItems[0]);
				this.setSelection(index);
				this.expand();
			});
		},
	});

	Ext.override(Ext.view.BoundList, {
		privates: {
			finishRenderChildren: function () {
				this.callParent(arguments);
				// Clear the keypress listener added by default
				this.events.itemkeypress?.clearListeners();
			},
		},
		ITEM_HEIGHT: 32, // Default item height, change if needed
		SAME_SEARCH_DELAY: 500, // Delay in ms to consider the same search
		onItemKeyDown: function () {
			if (event.key.length > 1) return;
			const pressedKey = event.key.toUpperCase();
			const now = new Date().getTime();
			if (now - this.currentSelectLastPress < this.SAME_SEARCH_DELAY) {
				this.currentSelectLastPress = now;
				this.currentSelection += pressedKey;
			} else {
				this.currentSelectLastPress = now;
				this.currentSelection = pressedKey;
			}
			const filteredItems = this.getStore().data.items.filter(t =>
				t.data.m_text.toUpperCase().startsWith(this.currentSelection),
			);
			if (!filteredItems.length) return;
			const index = this.getStore().data.items.indexOf(filteredItems[0]);
			this.setScrollY(this.ITEM_HEIGHT * index);
			this.select(filteredItems[0]);
			event.stopPropagation();
		},
	});
	// endregion

	// region ContextMenu for every datagrid - !!!REQUIRES THE IMPROVED CONTEXT MENU CLIENT SCRIPT!!! Applies the basic context menu to every datagrid
	Ext.override(StarlimsDataGrid, {
		initComponent: function () {
			this.callParent(arguments);
			const me = this;
			// If there is no context menu client script, load it
			if (!window.contextMenuClientScript && !ApplyBaseContextMenu)
				return (window.contextMenuClientScript = getClientScriptByName(
					'Enterprise_Utilities.HTML_ContextMenu',
				).then(contextMenuClientScript => {
					document.head.appendChild(document.createElement('script')).innerHTML = contextMenuClientScript;
					ApplyBaseContextMenu(this);
					window.contextMenuClientScript = true; // So it's not a promise anymore
				}));
			// If the context menu client script is not loaded yet, wait for it to load
			if (window.contextMenuClientScript instanceof Promise)
				return window.contextMenuClientScript.then(contextMenuClientScript => ApplyBaseContextMenu(me));
			// If the context menu client script is loaded, apply it
			ApplyBaseContextMenu(this);
		},
	});
	// endregion

	const colorMap = [
		// Edit these to match keywords/urls contained in the environments and the colors you want to apply for each
		// { env: 'starlims.dev.com', color: '#0057e7' },
		// { env: 'QUAL', color: '#ffa700' },
		// { env: 'REP', color: '#008744' },
	];
	const color = colorMap.find(c => document.location.href.toUpperCase().includes(c.env))?.color;
	if (color)
		addCssRule(`
			.shell-topbar .x-panel-body-default {
				background-color: ${color}
			}
		`);

	// region VLE - 20210308 - Keep groups, filters and sorters on refresh + QVA Keep expanded groups
	Ext.define('StarlimsDataGridMethods', {
		override: 'StarlimsDataGrid',
		_SetData(value) {
			let me = this;
			if (me.rendered) {
				me.mask('loading...');
				me.updateLayout(true);
				me.suspendLayouts();
			}
			const grouper = me.store?.getGrouper();
			const sorters = me.store?.getSorters()?.items;
			let filters = me.store.getFilters()?.items;
			if (filters) filters = [...filters]; //copy
			const expandedGroups = me.store
				.getGroups()
				?.items?.map(i => i.config.groupKey)
				?.map(gk => [gk, me._getGroupByFeature()?.getMetaGroup(gk)?.isCollapsed])
				.filter(([_, collapsed]) => !collapsed)
				.map(([gk, _]) => gk);
			me.AssignData(value, value);
			if (me.store) {
				if (grouper) {
					me.suspendEvents();
					me.store.group(grouper);
					me.resumeEvents();
					const gbf = me._getGroupByFeature();
					const gbfCache = gbf?.getCache();
					for (const groupName in gbfCache) {
						if (gbfCache.hasOwnProperty(groupName)) {
							gbfCache[groupName].isCollapsed = true;
						}
					}
					for (const groupName of expandedGroups) {
						if (groupName in gbfCache) gbfCache[groupName].isCollapsed = false;
					}
					Ext.suspendLayouts();
					gbf.dataSource.onDataChanged();
					Ext.resumeLayouts(true);
					for (const groupName in gbfCache) {
						if (gbfCache.hasOwnProperty(groupName)) {
							const isCollapsed = !expandedGroups.includes(groupName);
							gbf.afterCollapseExpand(isCollapsed, groupName);
							if (gbf.lockingPartner) {
								gbf.lockingPartner.afterCollapseExpand(isCollapsed, groupName);
							}
						}
					}
				}
				if (sorters?.Length) me.store.setSorters(sorters);
				if (filters?.Length) me.store.setFilters(filters);
			}
			if (me.rendered) {
				me.resumeLayouts();
				me.unmask();
			}
		},
	});
	// endregion

	// region Change Dialogs.InputBox layout to be vertical instead of 'form' + 'ok' to 'OK' or value isn't returned
	Ext.override(Dialogs, {
		statics: {
			_InputBox: function (defaultValue, prompt = 'Input data', caption = 'Input', data, callBack) {
				const inputOkHandler = function msgBox_Handler(buttonText, selectedText) {
					if (Ext.isFunction(callBack)) {
						const result = buttonText.toUpperCase() === 'OK' ? selectedText : null;
						callBack.call(null, buttonText, result);
					}
					Dialogs.__FocusCurrentControl();
				};
				if (!data) Ext.Msg.prompt(caption, prompt, inputOkHandler, null, false, defaultValue);
				else {
					const inputWindow = Ext.create('Ext.window.Window', {
						autoShow: true,
						width: 300,
						layout: {
							type: 'vbox',
							align: 'stretch',
						},
						title: caption,
						referenceHolder: true,
						defaultFocus: 'cmbfield',
						defaultButton: 'okButton',
						modal: true,
						items: [
							{
								xtype: 'label',
								text: prompt,
								height: 30,
								anchor: '100%',
							},
							{
								xtype: 'StarlimsComboBox',
								reference: 'cmbfield',
								fieldLabel: prompt,
								height: 30,
							},
						],
						buttons: [
							{
								reference: 'okButton',
								text: 'Ok',
							},
						],
					});
					const btn = inputWindow.lookupReference('okButton');
					btn.on(
						'click',
						function (theButton) {
							const buttonId = theButton.getText().toUpperCase();
							let selectedText = this.items.items[1] !== null ? this.items.items[1].SelectedText : '';
							if (selectedText === undefined) selectedText = '';
							inputOkHandler(buttonId, selectedText);
							this.close();
						},
						inputWindow,
					);
					const combo = inputWindow.items.items[1];
					const dataset = this.GetDataSet(data);
					if (dataset && dataset.Tables[0] && dataset.Tables[0].Columns[0]) {
						combo.DisplayMember = dataset.Tables[0].Columns[0].ColumnName;
					}
					combo.DataSet = dataset;
					if (defaultValue !== null && defaultValue !== undefined) {
						combo.setValue(defaultValue);
					}
					inputWindow.show();
				}
			},
		},
	});
	// endregion

	// region QVA fix Utils.ConvertString to number that prevents UpdateProvider to run sometimes (numbers with ',' instead of '.')
	Ext.override(Utils, {
		statics: {
			ConvertString: function (stringValue, newType) {
				const { NumberDecimalSeparator, NumberGroupSeparator } = Starlims.UserProfile;
				const numberTypes = ['int', 'byte', 'int16', 'int32', 'float', 'number', 'decimal'];
				stringValue = numberTypes.includes(newType.toLowerCase())
					? stringValue.replace(NumberGroupSeparator, '').replace(NumberDecimalSeparator, '.')
					: stringValue;
				return this.callParent([stringValue, newType]);
			},
		},
	});
	// endregion

	//region allow ',' and '.' in every numberfield (kw: comma, dot, virgule, point)
	Ext.override(Ext.form.field.Number, {
		initComponent: function () {
			this.callParent(arguments);
			this.maskRe = /[0123456789\.\,e\+\-]/;
		},
		parseValue: function (val) {
			if (!val) return val;
			const parsed = parseFloat(val.toString().replace(',', '.'));
			return isNaN(parsed) ? null : parsed;
		},
		getErrors: function (value) {
			value = value || this.getRawValue();
			value = value.replace(',', '.');
			return this.callParent([value]);
		},
	});
	// endregion

	// region QVA - DataGrid RefreshAsync
	Ext.override(StarlimsDataGrid, {
		/**
		 * Refreshes the data of the datagrid asynchronously
		 * @param {*} options
		 * @param {boolean} options.keepScrollX
		 * @param {boolean} options.keepScrollY
		 * @param {string[]} options.keepSelectionFields
		 * @param {string} options.maskText
		 * @param {boolean} options.keepGroups
		 * @param {boolean} options.keepGroupsState
		 * @param {boolean} options.keepFilters
		 * @param {boolean} options.keepSorters
		 * @param {boolean} options.triggerRowChange
		 * @returns
		 */
		RefreshAsync: async function ({
			keepSelectionFields = [],
			maskText = 'Refreshing data...',
			keepGroups = true,
			keepGroupsState = true,
			keepFilters = true,
			keepSorters = true,
			keepScrollX = false,
			keepScrollY = false,
			triggerRowChange = true,
		} = {}) {
			if (!this.Data) return;
			try {
				if (this.rendered) this.mask(maskText);
				// Save current config
				let grouper, sorters, filters, expandedGroups, scrollX, scrollY, currentSelectionData, currentRowData;
				if (keepGroups) grouper = this.store?.getGrouper();
				if (keepSorters) sorters = this.store?.getSorters()?.items;
				if (keepFilters) filters = this.store?.getFilters()?.items;
				if (keepFilters && filters) filters = [...filters]; //copy
				if (keepGroupsState)
					expandedGroups = this.store
						.getGroups()
						?.items?.map(i => i.config.groupKey)
						?.map(gk => [gk, this._getGroupByFeature()?.getMetaGroup(gk)?.isCollapsed])
						.filter(([_, collapsed]) => !collapsed)
						.map(([gk, _]) => gk);
				if (keepScrollX) scrollX = this.getScrollX();
				if (keepScrollY) scrollY = this.getScrollY();
				if (keepSelectionFields?.length) {
					const currentRecord =
						this.m_currentRecord ?? this.getSelection()?.[0] ?? this.store?.data?.items?.[0] ?? {};
					currentRowData = keepSelectionFields.map(f => currentRecord.data[f]);
					const currentSelection = this.getSelection();
					if (currentSelection?.length)
						currentSelectionData = currentSelection.map(r => keepSelectionFields.map(f => r.data[f]));
					this.suspendEvent('selectionchange');
				} else if (!triggerRowChange) this.suspendEvent('selectionchange');

				const data = await Utils.GetTextAsync(this.Data);
				// Replace current config
				this.DataSet = new DataSet(JSON.parse(data));

				// Restore config
				if (!this.store)
					if (this.rendered) return this.unmask();
					else return;

				if (keepGroups && grouper) {
					this.suspendEvents();
					this.store.group(grouper);
					this.resumeEvents();
					if (keepGroupsState) {
						this.CollapseRows();
						const gbf = this._getGroupByFeature();
						expandedGroups?.forEach(g => {
							try {
								gbf.doCollapseExpand(false, g);
							} catch (ign) {
								/*ignore, group doesn't exist anymore*/
							}
						});
					}
				}
				if (keepSorters && sorters?.Length) this.store.setSorters(sorters);
				if (keepFilters && filters?.Length) this.store.setFilters(filters);
				if (keepSelectionFields?.length) {
					const selection = currentSelectionData?.length
						? currentSelectionData.map(data =>
								this.store.data.items.find(r => keepSelectionFields.every((f, i) => r[f] === data[i])),
							)
						: [];
					if (triggerRowChange) this.resumeEvent('selectionchange');
					if (selection.length) {
						this.setSelection(selection);
						this.m_currentRecord =
							this.store.data.items.find(r =>
								keepSelectionFields.every((f, i) => r[f] === currentRowData[i]),
							) ?? selection[0];
						this.ensureVisible(selection[0], { focus: false });
					}
					if (!triggerRowChange) this.resumeEvent('selectionchange');
				} else if (!triggerRowChange) this.resumeEvent('selectionchange');
				if (keepScrollX || keepScrollY) {
					if (keepScrollX === false) scrollX = this.getScrollX();
					if (keepScrollY === false) scrollY = this.getScrollY();
					this.scrollTo(scrollX, scrollY);
				}
				if (this.rendered) this.unmask();
			} catch (e) {
				if (this.rendered) this.unmask();
				console.error(e);
				// Display an error popup, uncomment the following line to enable it
				// Dialogs.MessageBox('Unable to refresh datagrid data', 'Error', 'OK', 'ERROR');
			}
		},
	});
	// endregion

	// region QVA - Displays user info in navBar and fix profile menu position + open designer button + logo (optional)
	Ext.Function.interceptAfter(window, '__StarlimsOnShellScriptFilesLoaded', () => {
		const tb = Shell.getView().getReferences()['app-topbar'];
		const navBar = tb.items.items[0].items.items[1];
		// Retrieve user infos
		const { starlimsdept, myusername, fullname, myrolename } = Starlims.Navigator.Variables;

		// Designer button
		if (Starlims.Navigator.Variables.isdesigner) {
			navBar.insert(0, {
				xtype: 'button',
				id: 'btnDesigner',
				reference: 'btnDesigner',
				iconCls: 'x-fa fa-code',
				tooltip: 'Open designer',
				handler: () => {
					if (Starlims.Navigator.Variables && !Starlims.Navigator.Variables['SHOWDESIGNERINTASKBAR'])
						Starlims.Navigator.Variables['SHOWDESIGNERINTASKBAR'] = 'true';
					bridge.openForm('DesignerHTML5Bridge.DesignerHost');
					Ext.toast('Opening designer...');
				},
			});
		}

		// User infos text
		navBar.insert(0, {
			xtype: 'panel',
			reference: 'topbaruserinfos',
			cls: 'topbar-user-infos',
			baseCls: 'topbar-user-infos',
			items: [
				{
					xtype: 'label',
					html: `${fullname} (${myusername}) - ${starlimsdept} - ${myrolename}`,
					style: 'color: white;', // Change color of user info here
					left: 0,
				},
			],
		});

		// Override main controller to update user infos when switching sites and fix profile menu position
		Ext.override(STARLIMS.view.main.MainController, {
			// switch user infos when switching sites
			onUserInfoSwitch: function (view, userInfo) {
				const currentInfos = navBar.items.items.find(e => e.reference === 'topbaruserinfos');
				const { starlimsdept, myusername, fullname, myrolename } = {
					...Starlims.Navigator.Variables,
					starlimsdept: userInfo.site,
				};
				if (currentInfos.el.dom)
					currentInfos.el.dom.innerText = `${fullname} (${myusername}) - ${starlimsdept} - ${myrolename}`;
				this.callParent(arguments);
			},
			// fix profile menu position
			showUserProfile: function (userProfileButton) {
				this._userProfileAnimationInProgress = true;

				this.userProfileForm.show();
				const [x, y] = userProfileButton.getPosition();
				const { width, height } = this.userProfileForm.getSize();
				this.userProfileForm.setPosition(
					x - width + userProfileButton.getWidth() + 40,
					userProfileButton.y + userProfileButton.getHeight() + 10,
				);
				this.userProfileForm.focus();
				this.userProfileTip.setPosition(
					this.userProfileForm.x + STARLIMS.view.userProfile.UserProfileView.prototype.width - 65,
					this.userProfileForm.y - Ext.isIE ? 24 : 17,
				);
				this.userProfileTip.show();
				this.userProfileTip.getEl().fadeIn({
					opacity: 1,
					easing: 'easeOut',
					duration: 300,
				});
				var me = this;
				this.userProfileForm.getEl().fadeIn({
					opacity: 1,
					easing: 'easeOut',
					duration: 300,
					callback: function () {
						me._userProfileAnimationInProgress = false;
					},
				});
			},
		});

		//! The following code is used to add the customer logo to the topbar, uncomment and replace the base64 string with the customer logo
		//! You can also change the width and height of the logo, if you do so, make sure to edit the margins also
		/*
		const logo = document.createElement('img');
		logo.src = 'BASE64_STRING_HERE (data:image/png;base64,...) or if you uploaded the image to the server, the url here (Runtime_Support.GetImageById?...)';
		logo.style.width = '160px';
		logo.style.margin = '0 auto';

		const logoContainer = document.createElement('div');
		logoContainer.appendChild(logo);
		logoContainer.style.display = 'flex';
		logoContainer.style.alignItems = 'center';
		logoContainer.style.justifyContent = 'center';
		logoContainer.style.position = 'fixed';
		logoContainer.style.top = '0';
		logoContainer.style.left = '50%';
		logoContainer.style.width = '160px';
		logoContainer.style.height = tb.height + 'px';
		logoContainer.style.marginLeft = '-80px';
		// add the logo to the topbar
		document.body.appendChild(logoContainer);
		*/

		//! Code below to change the icons/buttons colors and hover effects
		//! This is especially useful when you changed the color of the topbar to make sure the icons are still visible
		/*
		addCssRule(`
			.x-btn-icon-el-default-small {
				color: #475569 !important;
			}
		`);

		addCssRule(`
			.shell-topbar-button:hover {
				opacity: 0.4;
			}
		`);

		addCssRule(`
			.shell-starlims-console-button:hover {
				color: #475569 !important;
				opacity: 0.4;
			}
		`);

		addCssRule(`
			a.shell-starlims-console-button.x-btn-focus ::before {
				color: #475569 !important;
			}
		`);

		tb.getReferences().showConsoleButton.overCls = '';
		tb.getReferences().showConsoleButton.focusCls = '';

		// Change logo color
		addCssRule(`
			.shell-starlims-logo {
				filter: invert(0.8);
			}
		`);
		*/
	});
	// endregion

	// region QVA 2024-04-23 - Improve console/notification searchbar
	Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
		Ext.override(STARLIMS.view.console.ConsoleController, {
			filterStore: function (value) {
				const me = this;
				const searchString = value.toLowerCase();
				const store = this.view.items.items[0].getStore();
				if (!searchString) return store.clearFilter();

				const filterFn = function (node) {
					if (node.childNodes?.some(filterFn)) {
						node.expand();
						return true;
					}
					if (!node.childNodes?.length)
						return (
							node.data.text?.toLowerCase().includes(searchString) ||
							node.parentNode?.data.text?.toLowerCase().includes(searchString)
						);
					if (node.data.text.toLowerCase().includes(searchString)) return true;
					return false;
				};

				store.getFilters().replaceAll({
					filterFn,
				});
			},
		});
	});
	// endregion

	// region QVA 2024-04-26 - Prevent error when unmasking destroyed components
	Ext.override(Ext.Component, {
		unmask: function () {
			if (this.destroyed) return;
			this.callParent(arguments);
		},
	});
	// endregion
})();
