//! THIS SCRIPT IS A MESS, I wrote everything I saw, developed or thought of in it, it's not meant to be a ready to deploy snippet list. However, you can find some useful code snippets in it.


// Timezone in js
const timezone = new Date().toLocaleTimeString('en', {timeZoneName: 'long'}).substr(12);

/*
Port du bridge : 5468

Control qui reste disables alors quil devrait être enabled
essayer control.enable() ou control.disable()
plutot que control.Enabled = true/false
*/

//Fonction pour attendre la fermeture d'un report:
async function waitForReportClose() {
	return new Promise((resolve, reject) => {
		let inter = setInterval(() => {
			if(!document.getElementById('idReport')) {
				clearInterval(inter)
				resolve();
			}
		}, 500);
	});
}
// Usage: await waitForReportClose()

Requête dictionnaire depuis Datasource
:DSN := DICTIONARY;

Page de connexion bypass SSO
WEB_SUPPORT.starthtml.lims

JSON dans requete SQL
https://docs.microsoft.com/fr-fr/sql/relational-databases/json/use-openjson-with-the-default-schema-sql-server?view=sql-server-ver15

For avec curseur dans SQL Server
{
ReceiveInLab.setDefaultResults
/********************************************************************************
Description.. :	

DS Type...... : SQL
Author....... : QUENTIN
Date......... :	2021-11-18
Parameters... :	- 

********************************************************************************/
:PARAMETERS FOLDERNO
BEGIN TRAN

Select Distinct  RESULTS.ORIGREC as RO, ANALYTES.ORIGREC as AO INTO #TempRes
From ORDERS Inner Join
  BATCHSAMPLINGREQUIREMENT On ORDERS.ORDNO = BATCHSAMPLINGREQUIREMENT.ORDNO Inner Join 
  BATCHSAMPLINGREQ_TESTS   On BATCHSAMPLINGREQUIREMENT.ORDNO = BATCHSAMPLINGREQ_TESTS.ORDNO And BATCHSAMPLINGREQUIREMENT.INVENTORYID =
    BATCHSAMPLINGREQ_TESTS.INVENTORYID Inner Join
  ORDTASK                  On ORDERS.ORDNO = ORDTASK.ORDNO Inner Join
  METHODSRELATEDTOTESTS    On ORDTASK.TESTCODE = METHODSRELATEDTOTESTS.TESTCODE And
                              ORDTASK.METHOD = METHODSRELATEDTOTESTS.METHOD Inner Join
  RESULTS                  On ORDTASK.ORDNO = RESULTS.ORDNO And
                              ORDTASK.TESTCODE = RESULTS.TESTCODE Inner Join
  ANALYTES                 On ANALYTES.TESTCODE = RESULTS.TESTCODE And 
                              ANALYTES.ANALYTE = RESULTS.ANALYTE
Where ORDERS.FOLDERNO = @FOLDERNO And 
      METHODSRELATEDTOTESTS.AUTOFILL = 'Y'

DECLARE load_cursor CURSOR FOR 
    SELECT RO, AO 
    FROM #TempRes
	
DECLARE @ro INTEGER
DECLARE @ao INTEGER
OPEN load_cursor 
FETCH NEXT FROM load_cursor INTO @ro, @ao 

WHILE @@FETCH_STATUS = 0 
BEGIN 
	DECLARE @res VARCHAR(255)
	SELECT @res = DEFAULT_RESULT FROM ANALYTES WHERE ORIGREC = @ao
	
	UPDATE RESULTS SET
		NUMRES = @res,
		FINAL = @res,
		DATEENTER = (SELECT GETDATE()),
		FIRSTUSER = 'BATCH',
		S = 'DONE',
		SA = 'Released'
	WHERE ORIGREC = @ro
	FETCH NEXT FROM load_cursor INTO @ro, @ao 
END
CLOSE load_cursor 
DEALLOCATE load_cursor
SELECT * FROM RESULTS WHERE ORIGREC IN (SELECT RO FROM #TempRes)
	
ROLLBACK
}


Différencier appel HTML et XFD en serverscript
:IF Request:IsJSON;

Transformer XmlDataSet en JSON
ToJson(ExecFunction("Enterprise_Server.DataSetSupport.DSFromString", {XmlDataSet}));

//Sélection d'imprimante par le bridge
var sPrinter = await bridge.openForm("BatchManager.PrinterSettingsHost", []);

Détecter copié collé:
cboTank.el.dom.addEventListener('paste', (pasteEvent) => console.log(pasteEvent.clipboardData.getData('text')))

Refresh console et reminders / notifications
Shell.refreshConsole() // Console de gauche
Shell.refreshNotifications() // Console de droite

Suspend et resume layout sur à priori n''importe quel objet sencha
obj.SuspendLayout();
obj.ResumeLayout();

Connection strings XFD (non affiché en html)
:RETURN GetConnectionStrings();

Sélectionner des couleurs
function getDifferentColors(numberOfColors) {
    step = parseInt((300/numberOfColors))
    let colors = []
    for(let h = 0; h <= 300; h += step) {
        colors.push(hslToHex(h, 80, 60))
    }
    return colors;
}

function hslToHex(h, s, l) {
    l /= 100;
    const a = s * Math.min(l, 1 - l) / 100;
    const f = n => {
      const k = (n + h / 30) % 12;
      const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
    };
    return `#${f(0)}${f(8)}${f(4)}`;
}

Noms de colonne datagrid avec des retour à la ligne
dataGrid.onAfter('render', () => {
	setTimeout(() => { // Important!!! Car dans afterrender, la datagrid n'est pas encore rendered sur l'écran donc pas encore de html
		let dataGrid = form.All('dgCrossTab');
		dataGrid.columnManager.columns.forEach(c => {
			if(!c.Caption.includes('\\n')) return
			let parts = c.Caption.split('\\n').map(s => s.trim())
			let captionEl = c.el.dom.firstChild.firstChild.firstChild.firstChild
			let clone = captionEl.firstChild.cloneNode()
			parts.forEach((part, index) => {
				if(index === 0) {
					c.Caption = part
					return
				}
				let newClone = clone.cloneNode()
				newClone.innerHTML = part;
				captionEl.appendChild(document.createElement('br'))
				captionEl.appendChild(newClone)
			});
		});
		if(dataGrid.columnManager.columns[0]) dataGrid.columnManager.columns[0].Width = dataGrid.columnManager.columns[0].Width + 1
	}, 500);
});

Export treeview en excel dans le HTML_Excel
function csExportTreeListViewToExcel(tlv, aSearchCriteria, fileName) {
	if(tlv.DataArray.length === 0) return;
	let copyDS = makeDateTimeColumnsInvariant(tlv.DataSet.Copy());
	let jsonDS = copyDS.Serialize();
	let columns = tlv.columnManager.columns.filter(col => col.isVisible()).map(col => ({
		Caption: col.Text,
		DataMember: col.dataIndex,
		FormatString: ""
	}));
	let sDefaultDateFormatString = convertColumnFormatString();
	lims.CallServerAsync("OfficeAutomation.ExportToExcel", [jsonDS, columns, aSearchCriteria, sDefaultDateFormatString], 
		function onSuccess(serverFile){
			lims.DownloadFile("Enterprise_Utilities.EchoFile", [serverFile], (fileName ? fileName : 'Excel') + ".xlsx" );		
		}
	);	
}

Exemple send to batch + connexion a autre base, avec la connectionstring ajoutée sur le serveur dans web.config
{
:IF !InBatchProcess();
    SUBMITTOBATCH("QVA.qva");
    :RETURN "Script sent to batch";
:ENDIF;
:DECLARE arr;
arr := {};
:TRY;
	LimsSqlConnect("OLDDatabase");
	arr := SqlExecute("SELECT DISTINCT BATCHNO, RETRAIT FROM BATCHES WHERE RETRAIT <> 0", "OLDDatabase");
:CATCH;
	ErrorMes(FormatErrorMessage(GetLastSQLError()));
:FINALLY;
	LimsSqlDisconnect("OLDDatabase");
	count := Len(arr);
	:FOR i := 1 :TO count;
		row := arr[i];
		SQLExecute("UPDATE BATCHES SET RETRAIT = "+LimsString(row[2])+" WHERE BATCHNO = '"+row[1]+"'");
	:NEXT;
	
:ENDTRY;
}

Upload file:
QuickIntro.25_UploadDownload
lims.UploadFile("ImportData.uploadFileProcessingScript", result => {
	if(!result) return
	const [fileName, serverFilePath] = result;
	// Code
});

Fixs bootstrap

// region VLE - 20210308 - SLIM-161 - Keep groups, filters and sorters on refresh + QVA Keep expanded groups
Ext.define('StarlimsDataGridMethods', {
	override: 'StarlimsDataGrid',
	_SetData(value) {
		let me = this;
		if(me.rendered) {
			me.mask("loading...");
			me.updateLayout(true);
			me.suspendLayouts();
		}
		let group = me.store?.getGroupField();
		let defaultGroup = me.groupColumns?.[0]?.DataMember;
		//const grouper = me.store?.getGrouper();
		let sorters = me.store?.getSorters()?.items;
		let filters = me.store.getFilters()?.items;
		const filterFields = me._filterBarPluginData?.fields?.map;
		const filterVisible = me._filterBarPluginData?.visible;
		if(filters) filters = [...filters]; //copy
		const expandedGroups = me.store.getGroups()?.items?.map(i => i.config.groupKey)?.map(gk => [gk, me._getGroupByFeature()?.getMetaGroup(gk)?.isCollapsed]).filter(([_, collapsed]) => !collapsed).map(([gk, _]) => gk);
		me.AssignData(value, value);
		if(me.store) {
			if(group || defaultGroup) {
				if(group) me.store.setGroupField(group);
				const gbf = me._getGroupByFeature()
				expandedGroups?.forEach(g => {
					try{gbf.doCollapseExpand(false, g)}catch(ign){/*ignore, group doesn't exist anymore*/}
				})
			}
			//if(grouper) me.store.setGrouper(grouper);
			if(sorters?.Length) me.store.setSorters(sorters);
			if(filters?.Length) me.store.setFilters(filters);
			if(me.grouper) me.store.group(me.grouper)
			if(filterFields) {
				me._filterBarPluginData.fields.keys.forEach(k => {
					if(!filterFields.hasOwnProperty(k)) return;
					const value = filterFields[k].value;
					if(value) {
						me._filterBarPluginData.fields.map[k].setValue(value)
						me.filterBar.applyFilters.call(me, me._filterBarPluginData.fields.map[k])
					}
				})
				me.fireEvent('filterupdated', me._filterBarPluginData.filterArray);
				if(filterVisible) me.showFilterRow();
			}
		}
		if(me.rendered) {
			me.resumeLayouts();
			me.unmask();
		}
	}
});
// endregion

// region QVA 2022-07-18 - Override reconfigure pour garder les filtres car ils disparaissent à certains moments (changement de ligne etc...)
Ext.override(Ext.panel.Table, {
	reconfigure: function(b, c, k) {
		const me = this;
		if(!me.FilterRowVisible || me.RowCount === 0) return me.callParent([b, c, k])
		let filters = me.store.getFilters()?.items;
		if(filters) filters = [...filters]; //copy
		const filterFields = me._filterBarPluginData?.fields?.map;
		const filterVisible = me._filterBarPluginData?.visible;
		const filterArray = me._filterBarPluginData?.filterArray;
		me.callParent([b, c, k])
		if(filters?.Length) me.store.setFilters(filters);
		if(filterFields) {
			me._filterBarPluginData.fields.keys.forEach(key => {
				if(!filterFields.hasOwnProperty(key)) return;
				const value = filterFields[key].value;
				if(value) {
					me._filterBarPluginData.fields.map[key].setRawValue(value)
					me._filterBarPluginData.fields.map[key].value = value
				}
			})
			me._filterBarPluginData.filterArray = filterArray
			if(filterArray.length > 0) me.filterBar.applyFilters.call(me, filterFields[filterArray[0].config.property])
		}
	}
})
// endregion


//VLE - 20200730 - Fix dropdowns width issue - Ticket #Z00002399B0008
// Combobox fix list
insertCustomCss(`
	.x-boundlist {
		min-width: max-content !important;
	}
`);
// Same but oneline
document.styleSheets[0].insertRule('.x-boundlist { min-width: max-content !important; }', 1);


// to toggle on/off small lines / small rows in datagrid (narrow rows), change this enterprise setting
// NARROW_HTML_DATAGRID_ROWS
// may not be visible in the enterprise settings list, but it is when launching the form from designer
// And this code to Dashboard.GetNavigatorVars before :RETURN serverVariables;

// HTML DataGrid controls, support for narrow rows and header;
// (greater density of rows per screen real estate, useful for some implementation);
AAdd( serverVariables, {"NarrowHtmlDatagridRows", IIf(Empty(GetSetting("NARROW_HTML_DATAGRID_ROWS")), .F., .T.)} );

// only for datagrid that have narrow rows active
// QVA Align checkboxes in the center of narrow rows
insertCustomCss(`
	.x-grid-checkcolumn {
		top: 2px !important;
	}
`);

// QVA Place images in datagrid cells vertically in the middle
insertCustomCss(`
	.x-grid-cell-inner > img {
		vertical-align: middle !important;
	}
`);

// region QVA Display loading message each time ShowModalDialog is called to prevent any other action
Ext.override(StarlimsForm, {
    ShowModalDialog: async function ShowModalDialog(url, args, onCloseCallback, windowConfig) {
        try {this.mask('Chargement...')} catch(ign) {} // this.Resources['Loading...']
        let me = this;
        Utils.checkArguments(arguments, [Utils.Comparers.StringNotNullOrEmpty, ['Array', 'Object', 'Number', 'String', 'Boolean', null], ['Function', 'String', null], ['Object', null]], "starlims.forms.js:ShowModalDialog");
        url = lims.GetFormSource(url);
        [onCloseCallback,windowConfig] = this._ProcessDialogStyle(onCloseCallback, windowConfig);
        if (onCloseCallback) {
            return this._ShowDialog(url, args, async () => {onCloseCallback(); try {me.unmask()} catch(ign) {}}, windowConfig);
        }else {
            return new Promise(function(resolve, reject) {
                me._ShowDialog(url, args, async function(returnValue) {
                    try {me.unmask()} catch(ign) {}
                    resolve(returnValue);
                }, windowConfig).then(function() {}).catch( async function(err) {
                    try {me.unmask()} catch(ign) {}
                    reject(err);    
                });
            })
        }
    }
})
// endregion

// QVA FNBLIMS-607 fix for when a cell is clicked then hidden, caused an error
Ext.override(Ext.view.Table, {
    privates: {
        activateCell: function(b) {
            var a = this, g = b.view !== a ? a.lockingPartner : null, h = a.grid.actionables, n = h.length, l = a.getNavigationModel(), k = b.target, m, i, f, e, c, j, d;
            b = b.clone();
            m = b.record;
            b.view.grid.ensureVisible(m, {
                column: b.column
            });
            f = a.all.item(b.rowIdx, !0);
            if (a.actionPosition) {
                i = a.all.item(a.actionPosition.rowIdx, !0);
                if (i && f !== i) {
                    Ext.fly(i).saveTabbableState({
                        skipSelf: !0,
                        includeSaved: !1
                    })
                }
            }
            a.activating = !0;
            if (!g) {
                e = Ext.fly(b.getCell(!0));
                a.actionPosition = b;
                for (c = 0; c < n; c++) {
                    j = j || h[c].activateCell(b, null, !0)
                }
                e = Ext.fly(b.getCell(!0))
            }
            if (g || (e && (e.restoreTabbableState({
                skipSelf: !0
            }).length | (d = e.findTabbableElements()).length)) || j) {
                for (c = 0; c < n; c++) {
                    if (h[c].activateRow) {
                        h[c].activateRow(f)
                    }
                }
                if(!b.column.Visible) return;
                if (g || d.length) {
                    Ext.fly(f).restoreTabbableState({
                        skipSelf: !0
                    });
                    if (g) {
                        a.actionableMode = !0;
                        a.actionPosition = null;
                        a.activating = !1;
                        return !0
                    }
                    if (d) {
                        a.actionRow = a.actionRowFly.attach(f);
                        a.actionableMode = a.ownerGrid.actionableMode = !0;
                        l.setPosition();
                        l.actionPosition = a.actionPosition = b;
                        if (k && Ext.Array.contains(d, k)) {
                            Ext.fly(k).focus()
                        } else {
                            Ext.fly(d[0]).focus()
                        }
                        a.activating = !1;
                        return !0
                    }
                }
            }
            a.activating = !1
        }
    }
})


// QVA 2022-04-21 - region override combobox to match V10/V11 behavior
// When pressing a key multiple times, it will select rows that starts wth this key
(async () => {
    Ext.override(StarlimsComboBox, {
        _InitEvents: function() {
            this.callParent(arguments);
            this.on('keypress', function() {
                if(this.AllowTextEdit) return
                const pressedKey = event.key.toUpperCase()
                if(!this.lastPressedKey || this.lastPressedKey !== pressedKey) {
                    this.lastPressedKey = pressedKey;
                    this.pressedKeyCount = 0;
                    return;
                }
                const filteredItems = this.Items.filter(i => i.m_text.toUpperCase().startsWith(pressedKey))
                if(++this.pressedKeyCount > filteredItems.length) this.pressedKeyCount = 0;
                const index = this.Items.indexOf(filteredItems[this.pressedKeyCount])
                this.setSelection(index)
                this.expand()
            })
            this.on('focusleave', function() {this.lastPressedKey = null})
        }
    })
})();
//endregion
// QVA 2022-04-21 - make StarlimsDataGrid and StarlimsColumn ._SetDataSet asynchronous
(async () => {
    Ext.override(StarlimsDataGrid, {
        async _SetDataSet(oDataSet) {
            const me = this;
            if (oDataSet && !(oDataSet instanceof DataSet)) {
                oDataSet = new DataSet(oDataSet);
            }
            me.m_dataSet = oDataSet;
            me.m_currentRecord = null;
            me.m_oldRec = null;
            var hadRows = me.store ? me.store.getData().getCount() > 0 : false;
            if (!oDataSet) {
                if (me.store)
                    me.store.clearFilter(true);
                if (me.store && me.store.getData().getCount() > 0) {
                    me.store.removeAll(false);
                    me.reconfigure();
                }
                if (hadRows) {
                    me._OnRowChange(null);
                }
                return;
            }
            me.groupColumns = me._prepareColumns(me.getColumns(), oDataSet);
            me.reconfigure(oDataSet.Tables[0]);
            if (me.groupColumns.length > 0) {
                me.Regroup();
            }
            if (me.rendered) {
                let sel = me.getSelectionModel();
                if (sel.getCount() > 0)
                    sel.deselectAll(true);
                sel.select(0);
                var hasRows = oDataSet.Tables && oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0;
                if (hadRows && !hasRows) {
                    me._OnRowChange(null);
                }
            }
            if (me.store)
                me.store.on('datachanged', function() {
                    if (me.store.getCount() === 1 && me.getSelection().length === 0)
                        me.getSelectionModel().select(me.store.getAt(0));
                }, me);
        }
    })
    Ext.override(StarlimsDataGridColumn, {
        _SetDataSet: async function _SetDataSet(dataSet) {
            if (dataSet && Ext.getClassName(dataSet) !== 'DataSet')
                this.m_dataSet = new DataSet(dataSet);
            else
                this.m_dataSet = dataSet;
            if (this._hasComboBoxEditor()) {
                if (this.m_dataSet) {
                    let dt = this.m_dataSet.Tables[0];
                    this._ensureMembersForComboType(dt);
                    this.m_comboBoxEditor.DisplayMember = this.DisplayMember;
                    this.m_comboBoxEditor.ValueMember = this.ValueMember;
                }
                this.m_comboBoxEditor.DataSet = this.m_dataSet;
                this._RedrawGrid();
            }
        }
    })
})();
//endregion



//region allow ',' and '.' in every numberfield (virgule, point)
Ext.override(Ext.form.field.Number, {
	initComponent: function() {
		this.callParent(arguments);
		this.maskRe = /[0123456789\.\,e\+\-]/
	},
	parseValue: function(val) {
		if(!val) return val;
		const parsed = parseFloat(val.toString().replace(',', '.'))
		return isNaN(parsed) ? null : parsed
	},
	getErrors: function (value) {
		value = value || this.getRawValue();
		value = value.replace(',', '.');
		return this.callParent([value])
	}
})
// endregion

// region QVA 2022-05-03 - SLIM-249 adds a title on empty caption MessageBox for them to be draggable
Ext.override(Dialogs, {
	statics: {
		MessageBox: function(text, caption = ' ', theButtons, icon, defaultButton, theCallBack) {
			return this.callParent([text, caption, theButtons, icon, defaultButton, theCallBack])
		}
	}
})
//endregion

// region alléger le css des mask
insertCustomCss(`
	body > .x-mask {
		background-image: none;
		background-color: rgba(0, 0, 0, 0.5) !important;
	}
`);
// endregion

// region amélioration du displayWaitMessage
Ext.override(StarlimsForm, {
	DisplayWaitMessage: async function(mask) {
		// Do not change conditions, we need to check the strict equality
		if(mask === false) try { this.unmask() } catch(e) {return} // ignored, throw an error only when the mask is already gone
		else this.mask(mask === true ? 'Loading...' : mask)
		await Application.DoEvents()
	}
})
// endregion


function findAutomationId(item) {
    let automationId
    do {
        try {automationId = item.getAttribute('automation-id')} catch(e) {return null}
        item = item.parentElement
    }while(automationId === null || automationId === undefined || automationId.toLowerCase() === 'not-found')
    return automationId
}

function evtToAutomationId(evt) {
	document.removeEventListener('click', evtToAutomationId)
	alert(findAutomationId(evt.srcElement))
	evt.stopPropagation()
}

document.addEventListener('click', evtToAutomationId)


// device.Scan on desktop
Ext.override(device, {
	statics: {
		Scan: function(succ, err) {
			if(typeof plugins === 'undefined') Dialogs.InputBox('', 'Scan barcode', 'Scan', (ok, barcode) => {ok === 'OK' ? succ(barcode, null, false) : err(ok)})
			else this.callParent([succ, err])
		}
	}
})



// QVA 2022-04-21 - make an asynchronous version of SetDataSet
Ext.override(StarlimsDataGrid, {
	async _SetDataSetAsync(oDataSet) {
		const me = this;
		if (oDataSet && !(oDataSet instanceof DataSet)) {
			oDataSet = new DataSet(oDataSet);
		}
		me.m_dataSet = oDataSet;
		me.m_currentRecord = null;
		me.m_oldRec = null;
		var hadRows = me.store ? me.store.getData().getCount() > 0 : false;
		if (!oDataSet) {
			if (me.store)
				me.store.clearFilter(true);
			if (me.store && me.store.getData().getCount() > 0) {
				me.store.removeAll(false);
				me.reconfigure();
			}
			if (hadRows) {
				me._OnRowChange(null);
			}
			return;
		}
		me.groupColumns = me._prepareColumns(me.getColumns(), oDataSet);
		me.reconfigure(oDataSet.Tables[0]);
		if (me.groupColumns.length > 0) {
			me.Regroup();
		}
		if (me.rendered) {
			let sel = me.getSelectionModel();
			if (sel.getCount() > 0)
				sel.deselectAll(true);
			sel.select(0);
			var hasRows = oDataSet.Tables && oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0;
			if (hadRows && !hasRows) {
				me._OnRowChange(null);
			}
		}
		if (me.store)
			me.store.on('datachanged', function() {
				if (me.store.getCount() === 1 && me.getSelection().length === 0)
					me.getSelectionModel().select(me.store.getAt(0));
			}, me);
	}
})

Ext.override(StarlimsDataGridColumn, {
	_SetDataSetAsync: async function _SetDataSetAsync(dataSet, refresh = true) {
		if (dataSet && Ext.getClassName(dataSet) !== 'DataSet')
			this.m_dataSet = new DataSet(dataSet);
		else
			this.m_dataSet = dataSet;
		if (this._hasComboBoxEditor()) {
			if (this.m_dataSet) {
				let dt = this.m_dataSet.Tables[0];
				this._ensureMembersForComboType(dt);
				this.m_comboBoxEditor.DisplayMember = this.DisplayMember;
				this.m_comboBoxEditor.ValueMember = this.ValueMember;
			}
			this.m_comboBoxEditor.DataSet = this.m_dataSet;
			if(refresh) this._RedrawGrid();
		}
	}
})

// region Methods used to improve performances to not redraw grid each time you call one of these methods, in a loop for example
	// region normal column
	Ext.override(StarlimsDataGridColumn, {	
		_SetCaptionNoRefresh(value) {
			Utils.checkArguments(arguments, ['String']);
			if (this.m_dto.Caption != value) {
				this.m_dto.Caption = value;
				value = Utils.htmlEncode(value);
				if (this.setText) {
					this.setText(value);
				} else
					this.text = value;
			}
		},
		
		_SetWidthNoRefresh(value) {
			this.m_dto.Width = value;
			if (this.setWidth) {
				this.setWidth(value);
			} else
				this.width = value;
		},
		
		_SetVisibleNoRefresh(value) {
			Utils.checkArguments(arguments, ['Boolean']);
			if(this.Visible === value) return; // Already in the right state
			this.m_dto.Visible = value;
			if (this.rendered && this.setVisibleNoRefresh)
				this.setVisibleNoRefresh(value);
			else
				this.hidden = !value;
		},
		
		setVisibleNoRefresh: function(visible) {
			return this[visible ? 'show' : 'hide'](false)
		},
		
		hide: function(refresh = true) {
			var me = this,
				rootHeaderCt = me.getRootHeaderCt(),
				owner = me.getRefOwner();
			// During object construction, so just set the hidden flag and jump out
			if (owner.constructing) {
				me.callParent();
				return me;
			}
			if (me.rendered && !me.isVisible()) {
				// Already hidden
				return me;
			}
			// Save our last shown width so we can gain space when shown back into fully flexed HeaderContainer.
			// If we are, say, flex: 1 and all others are fixed width, then removing will do a layout which will
			// convert all widths to flexes which will mean this flex value is too small.
			if (rootHeaderCt.forceFit) {
				me.visibleSiblingCount = rootHeaderCt.getVisibleGridColumns().length - 1;
				if (me.flex) {
					me.savedWidth = me.getWidth();
					me.flex = null;
				}
			}
			rootHeaderCt.beginChildHide();
			Ext.suspendLayouts();
			// owner is a group, hide call didn't come from the owner
			if (owner.isGroupHeader) {
				// The owner only has one item that isn't hidden and it's me; hide the owner.
				if (me.isNestedGroupHeader()) {
					owner.hide();
				}
				if (me.isSubHeader && !me.isGroupHeader && owner.query('>gridcolumn:not([hidden])').length === 1) {
					owner.lastHiddenHeader = me;
				}
			}
			me.callParent();
			
			if(refresh) {
				// Notify owning HeaderContainer. Will trigger a layout and a view refresh.
				rootHeaderCt.endChildHide();
				rootHeaderCt.onHeaderHide(me);
			}
			
			Ext.resumeLayouts(true);
			return me;
		},
		
		MakeVisibleIfModeAllows: function(refresh = true) {
			if (this.Visible) {
				return;
			}
			var frm = this.ParentForm;
			if (frm == null) {
				return;
			}
			var modeAllow = frm.GetModePropertyValue(this, 'Visible');
			if (modeAllow == null || modeAllow === true) {
				if(refresh) this.Visible = true;
				else this.setVisibleNoRefresh(true);
			}
		}
	})
	//endregion
	// region checkboxcolumn
	Ext.override(StarlimsDataGridCheckBoxColumn, {
		_SetCaptionNoRefresh(value) {
			Utils.checkArguments(arguments, ['String']);
			if (this.m_dto.Caption != value) {
				this.m_dto.Caption = value;
				value = Utils.htmlEncode(value);
				if (this.setText) {
					this.setText(value);
				} else
					this.text = value;
			}
		},
		
		_SetWidthNoRefresh(value) {
			this.m_dto.Width = value;
			if (this.setWidth) {
				this.setWidth(value);
			} else
				this.width = value;
		},
		
		_SetVisibleNoRefresh(value) {
			Utils.checkArguments(arguments, ['Boolean']);
			if(this.Visible === value) return; // Already in the right state
			this.m_dto.Visible = value;
			if (this.rendered && this.setVisibleNoRefresh)
				this.setVisibleNoRefresh(value);
			else
				this.hidden = !value;
		},
		
		setVisibleNoRefresh: function(visible) {
			return this[visible ? 'show' : 'hide'](false)
		},
		
		hide: function(refresh = true) {
			var me = this,
				rootHeaderCt = me.getRootHeaderCt(),
				owner = me.getRefOwner();
			// During object construction, so just set the hidden flag and jump out
			if (owner.constructing) {
				me.callParent();
				return me;
			}
			if (me.rendered && !me.isVisible()) {
				// Already hidden
				return me;
			}
			// Save our last shown width so we can gain space when shown back into fully flexed HeaderContainer.
			// If we are, say, flex: 1 and all others are fixed width, then removing will do a layout which will
			// convert all widths to flexes which will mean this flex value is too small.
			if (rootHeaderCt.forceFit) {
				me.visibleSiblingCount = rootHeaderCt.getVisibleGridColumns().length - 1;
				if (me.flex) {
					me.savedWidth = me.getWidth();
					me.flex = null;
				}
			}
			rootHeaderCt.beginChildHide();
			Ext.suspendLayouts();
			// owner is a group, hide call didn't come from the owner
			if (owner.isGroupHeader) {
				// The owner only has one item that isn't hidden and it's me; hide the owner.
				if (me.isNestedGroupHeader()) {
					owner.hide();
				}
				if (me.isSubHeader && !me.isGroupHeader && owner.query('>gridcolumn:not([hidden])').length === 1) {
					owner.lastHiddenHeader = me;
				}
			}
			me.callParent();
			
			if(refresh) {
				// Notify owning HeaderContainer. Will trigger a layout and a view refresh.
				rootHeaderCt.endChildHide();
				rootHeaderCt.onHeaderHide(me);
			}
			
			Ext.resumeLayouts(true);
			return me;
		},
		
		MakeVisibleIfModeAllows: function(refresh = true) {
			if (this.Visible) {
				return;
			}
			var frm = this.ParentForm;
			if (frm == null) {
				return;
			}
			var modeAllow = frm.GetModePropertyValue(this, 'Visible');
			if (modeAllow == null || modeAllow === true) {
				if(refresh) this.Visible = true;
				else this.setVisibleNoRefresh(true);
			}
		}
	})
	//endregion
// endregion


// region override uploadfile to simply select a file
Ext.override(StarlimsAppFunctions, {
	UploadPanel: function(scriptName) {
		var uploadPanel = Ext.create('Ext.form.Panel', {
			width: 400,
			bodyPadding: 10,
			frame: true,
			items: [{
				xtype: 'filefield',
				name: 'file',
				fieldLabel: GetGlobalResource('File'),
				labelWidth: 50,
				msgTarget: 'side',
				allowBlank: false,
				anchor: '100%',
				buttonText: GetGlobalResource('Select File...'),
				listeners: {
					change: function() {
						const form = this.up('form').getForm();
						if (form.isValid()) {
							form.submit({
								clientValidation: true,
								url: 'Runtime_Support.SaveFileFromHTML.lims?ScriptName=' + scriptName,
								waitMsg: GetGlobalResource('Uploading your file...'),
								success: function(fp, o) {
									this.form.owner.ownerCt.scriptresult = o.result.result;
									this.form.owner.ownerCt.close();
								},
								failure: async function(form, action) {
									switch (action.failureType) {
										case Ext.form.action.Action.CLIENT_INVALID:
											await Dialogs.MessageBox(GetGlobalResource('Form fields may not be submitted with invalid values.'), 'Error', 'OK', 'ERROR');
											break;
										case Ext.form.action.Action.CONNECT_FAILURE:
											await Dialogs.MessageBox(GetGlobalResource('Communication failed.'), 'Error', 'OK', 'ERROR');
											break;
										case Ext.form.action.Action.SERVER_INVALID:
											await Dialogs.MessageBox(action.result.msg, 'Error', 'OK', 'ERROR');
									}
									this.form.owner.ownerCt.close();
								}
							});
						}
					}
				}
			}]
		});
		return uploadPanel;
	},
	UploadFile: function(scriptName, onUploadComplete, eOpts) {
		var uploadPanel = this.UploadPanel(scriptName);
		var uploadWindow = Ext.create('Ext.window.Window', {
			title: 'Upload a File',
			height: 100,
			width: 100,
			layout: 'fit',
			modal: true,
			constrain: true,
			closeAction: 'destroy',
			items: [uploadPanel],
			listeners: {
				destroy: function() {
					onUploadComplete.apply(eOpts != null && eOpts.scope != null ? eOpts.scope : this, [this.scriptresult]);
				}
			}
		});
		uploadWindow.setSize(500, 140);
		uploadWindow.setBodyStyle("backgroundColor: 'WHITE'");
		uploadWindow.show();
		var task = new Ext.util.DelayedTask(function() {
			try {
				Ext.WindowManager.bringToFront(uploadWindow);
			} catch (ex) {}
		}
		);
		task.delay(50);
	},
})
// endregion


// region ouvrir un fichier avec ExcelJS
// à intégrer dans la région HTML de StartHTML
<!-- ExcelJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.3.0/exceljs.min.js" integrity="sha512-UnrKxsCMN9hFk7M56t4I4ckB4N/2HHi0w/7+B/1JsXIX3DmyBcsGpT3/BsuZMZf+6mAr0vP81syWtfynHJ69JA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
// A mettre où besoin, ou bootstrap pour une utilisation globale
// Ouvre un fichier excel avec ExcelJS
function openExcelFile() {
	return new Promise((resolve, reject) => {
		if(!window.showOpenFilePicker) reject(new Error('This function only works in HTTPS servers'));
		window.showOpenFilePicker({
			excludeAcceptAllOption: true,
			types: [
				{
					description: 'Excel file',
					accept: {
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx']
					}
				}
			]
		}).then(async res => {
			const [fileHandle] = res;
			const fileData = await fileHandle.getFile();
			const workbook = new ExcelJS.Workbook();
			const fileReader = new FileReader();
			fileReader.onload = async function () {
				await workbook.xlsx.load(fileReader.result)
				resolve(workbook);
			}
			fileReader.onabort = reject;
			fileReader.onerror = reject;
			fileReader.readAsArrayBuffer(fileData);
		}).catch(() => resolve(null));
	})
}
// endregion

inputElement = document.createElement('input');
inputElement.type = 'file';
inputElement.accept = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
inputElement.click();
inputElement.onchange = async function() {
	const file = this.files[0];
	const reader = new FileReader();
	reader.onload = async function() {
		const workbook = XLSX.read(reader.result);
		console.log(workbook);
	}
	reader.readAsArrayBuffer(file);
}

// region .Data to async
XX.m_dto.Data = lims.GetDataSource(...)
lims.GetDataSetAsync(...).then(ds => {
	XX.DataSet = ds;
})
// endregion

// region override isOnline to not make useless request
Ext.override(Utils, {
	statics: {
		IsOnline: function() {
			if(navigator) return navigator.onLine
			return callParent();
		}
	}
});
// endregion


// region QVA Change la couleur de la topbar en fonction de la configration dans les lookupvalues (Listes de données)
Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
	lims.GetDataSetAsync('GlobalDataSources.dsGetLookupValues', ['COLOR_ENVIRONNEMENT']).then(ds => {
		let color;
		for(let i = 0; i < ds.Tables[0].Rows.Count; ++i) {
			if(document.location.href.includes(ds.Tables[0].getAt(i).data.VALUE)) {
				color = ds.Tables[0].getAt(i).data.TEXT
				break;
			}
		}
		
		insertCustomCss(`
			.shell-topbar .x-panel-body-default {
				transition: 1s;
				background-color: ${color};
			}
		`)
	});
});
// endregion

'LIMS' password
B74CB58D47196568F82DEF64F4F30EE03ED28B28
	UPDATE USERS SET PASSWRD = 'B74CB58D47196568F82DEF64F4F30EE03ED28B28' WHERE USRNAM = 'QUENTIN_EN'

// QVA console captions plus petit font
insertCustomCss(`
	.shell-console-leaf {
		font-size: 10.5px !important;
	}
`)

// region QVA 2022-07-18 FNBLIMS-722 - Corrige les années qui n'apparaissent pas sur les datepicker
insertCustomCss(`
	.x-monthpicker-body {
		display: flex !important
	}
	.x-monthpicker-years {
		float: right !important
	}
	.x-monthpicker-yearnav {
		display: flex !important
	}
`)
// endregion

// Package CacheManager then add this code to HTML_Bootstrap
// region QVA cache deletion
Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
	function clearCacheAndReload() {
		console.log('Client cache deleted since there was no previous record of cache clear');
		const navvar = localStorage.getItem('Starlims.Navigator.Variables');
		localStorage.clear();
		localStorage.setItem('Starlims.Navigator.Variables', navvar);
		localStorage.setItem('last-cache-deletion', new Date().toISOString());
		window.location.reload();
	}
	const lastCacheDeletionDate = localStorage.getItem('last-cache-deletion');
	if(!lastCacheDeletionDate) return clearCacheAndReload();
	lims.CallServerAsync('Enterprise_Settings.GetCacheDeletionDate').then(isoDate => {
		const deletionDate = new Date(isoDate)
		const lastDeletionDate = new Date(lastCacheDeletionDate);
		if(lastDeletionDate.toString() === 'Invalide Date' || lastDeletionDate < deletionDate) clearCacheAndReload();
	})
});
// endregion


// Remove splashscreen delay | white page on opening in new tab | blank page oint
Serverscript -> Web_Support.StartHtml

Delete these lines:

~698 : window.splashScreenStart = Date.now();

~885 : window.splashScreenEnd = Date.now();
	   var delay = window.splashScreenEnd - window.splashScreenStart;
	   delay = delay < 2000 ? 2000 - delay : 0;
	   delete window.splashScreenStart;
	   delete window.splashScreenEnd;

~893: setTimeout(function() {
		// Remove one indent level between these lines
~944: }, delay);




// region QVA do not load console and reminders on NEWTAB mode opening as they are not visible and buttons to show them either
Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
	const originalLaunchApp = Shell.LaunchApp;
	Shell.LaunchApp = async function(appFormId, args, mode, appName) {
		if(mode === 'NEWTAB') {
			var newTab = window.open(getTabURL(appFormId, args) + '&webOpeningMode=' + mode.toUpperCase(), '_blank');
			newTab.name = String.IsNullOrEmpty(appName) ? appFormId : appName;
			newTab.focus();
			return newTab;
		}
		return originalLaunchApp(appFormId, args, mode, appName);
		function getTabURL(appFormId, args) {
			var hostRoot = window.location.href.replace('#', '');
			var hostRootIndex = hostRoot.toLowerCase().indexOf('/starthtml.lims');
			var formArgs = "";
			if (hostRootIndex >= 0)
				hostRoot = hostRoot.substring(0, hostRootIndex);
			if (args != null && args)
				formArgs = '&formargs=' + encodeURIComponent(lims.ToJson(args));
			var isPortal = Starlims.Portal ? "&isPortal=true" : "";
			return hostRoot + '/starthtml.lims?FormId=' + appFormId + formArgs + isPortal;
		}
	}
});

Ext.Function.interceptBefore(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
	Ext.override(STARLIMS.view.console.ConsoleController, {
		onConsoleTreeAfterRender: function(treePanel) {
			const urlParams = new URLSearchParams(window.location.search)
			if(urlParams.get('webOpeningMode') === 'NEWTAB') {
				return this.fireViewEvent('onConsoleRecordsLoaded', treePanel.getStore().getData());
			};
			this.callParent([treePanel]);
		}
	})
});
// endregion

// Merge rows in a dataset
Orders_dg.m_dto.Data = lims.GetDataSource("ApproveResentByOrder.ORDERS_DG",  [ "Resent", Whr, form.Variables["showAll"], form.Variables["sQueryType"] ] );
Orders_dg.mask('Loading data...');
lims.GetDataSetAsync("ApproveResentByOrder.ORDERS_DG", ["Resent", Whr, form.Variables["showAll"].toString(), form.Variables["sQueryType"]]).then(ds => {
	const rows = ds.Tables[0].Rows
	for(let i = 0; i < rows.Count; ++i) {
		const row = rows[i];
		const duplicate = rows.ToArray().find((r, ind) => ind > i && r.data.ORIGREC === row.data.ORIGREC)
		if(!duplicate) return
		for(prop in row.data) {
			row.data[prop] = duplicate.data[prop] || row.data[prop]
		}
		ds.Tables[0].remove(duplicate)
	}
	Orders_dg.DataSet = ds;
	Orders_dg.unmask();
	Results_gd.Refresh();
}).catch(e => {
	Orders_dg.unmask();
	throw e;
})

Unlock Application Serverscript etc. category (blue table )
:RETURN Sqlexecute("UPDATE LIMSXXXXCATEGORY SET ISSYSTEM = 'N' WHERE CATNAME = 'CATEGORYNAME'", 'DICTIONARY');
//or in ds
:DSN:=DICTIONARY;
UPDATE LIMSXXXXCATEGORY SET ISSYSTEM = 'N' WHERE CATNAME = 'CATEGORYNAME'
UPDATE LIMSCLIENTSCRIPTCATEGORIES SET ISSYSTEM = 'N' WHERE CATNAME = 'Utilities'
UPDATE LIMSCLIENTSCRIPTCATEGORIES SET ISSYSTEM = 'N' WHERE CATNAME = 'Euriware'
UPDATE LIMSSERVERSCRIPTCATEGORIES SET ISSYSTEM = 'N' WHERE CATNAME = 'STARDOC'
UPDATE LIMSSERVERSCRIPTCATEGORIES SET ISSYSTEM = 'N' WHERE CATNAME = 'Web_Support'

// Se donner tous les servgrp (sites) pour un dept departement
:PARAMETERS site := 'MCA-China', usrnam := 'QVA';
insert into SERVGANALYST (DEPT, SERVGRP, USRNAM) (
	select distinct @site as DEPT, SERVGRP, @usrnam as USRNAM
	FROM SERVGANALYST 
	WHERE 1=1
		AND DEPT = @site
		AND USRNAM <> @usrnam 
		AND SERVGRP NOT IN (
			SELECT SERVGRP 
			FROM SERVGANALYST 
			WHERE 1=1 
				AND USRNAM = @usrnam 
				AND DEPT = @site
		)
)

:PROCEDURE printWB;
:PARAMETERS wb, maxCellLength;
sheet := wb:GetSheetAt(0);
:FOR rowIndex := 0 :TO sheet:PhysicalNumberOfRows - 1;
	rowToPrint := sheet:GetRow(rowIndex);
	rowString := "";
	:FOR cellIndex := 0 :TO rowToPrint:PhysicalNumberOfCells - 1;
		cellToPrint := rowToPrint:GetCell(cellIndex);
		:IF cell == NIL .OR. Empty(cell); 
			:LOOP; 
		:ENDIF;
		cellType := cellToPrint:CellType;
		:DECLARE cellValue;
		:BEGINCASE;
			:CASE cellType == "String";
				cellValue := cellToPrint:StringCellValue;
			:EXITCASE;
			:CASE cellType == "Numeric";
				cellValue := LimsString(cellToPrint:NumericCellValue);
			:EXITCASE;
			:OTHERWISE;
				cellValue := LimsString(cellToPrint);
			:EXITCASE;
		:ENDCASE;
		rowString += limsstring(cellValue) + "|";
	:NEXT;
	usrmes(rowString);
:NEXT;
:ENDPROC;


// Reflexion ssl
SDMSInterface.RecognitionQueueSupport.ReprocessDocument;


	//reflection;
	type := oWorkbook:GetProperties():GetType();
	field := type:GetField( "cust", LimsNetCast( 4 + 32, "enum:System.Reflection.BindingFlags" ) );
	newCust := LimsNetConnect("NPOI.dll", "NPOI.CustomProperties");
	field:SetValue(oWorkbook:GetProperties(), newCust);
	field:SetValue(oWorkbook:GetProperties(), NIL);
	
	cust := oWorkbook:GetProperties():CustomProperties;
	props := cust:GetUnderlyingProperties();
	properties := props:GetPropertyList();
	:FOR p:=1 :TO properties:Count;
		properties[p]:Delete();
	:NEXT;
	oWorkbook:GetProperties():Commit();


// Activation compression requête requete http
%systemroot%\system32\inetsrv\APPCMD.exe set config -section:system.webServer/httpCompression /+"dynamicTypes.[mimeType='application/json',enabled='True']" /commit:apphost


// Sort global declarations alphabetically
const declarations = `...`;
sorted = declarations.split('\n').sort((a,b) => {
    let avar, bvar;
    avar = a.split(' ')[1]
    bvar = b.split(' ')[1]
    return a.localeCompare(b)
});
sorted.join('\n');

// Erreur is trusted istrusted (timeout requête ?)
// parseInt à la place de convert.XXX
// Après pas sûr que ça marche bien


// region allows changing datagrid data asynchrounously easily
let dsLastSetDataSetAsyncArgs = new Map()
StarlimsDataGrid.prototype.SetDataSetAsync = async function SetDataSetAsync(dsName, parameters) {
	dsLastSetDataSetAsyncArgs.set(this, parameters);
	this.mask('Loading...');
	this.m_dto.Data = lims.GetDataSource(dsName, parameters);
	const thisGrid = this;
	await lims.GetDataSetAsync(dsName, parameters?.map(p => p?.toString())).then(ds => {
		if(thisGrid.isDestroyed) return; // if the grid is destroyed before the async call is finished
		if(dsLastSetDataSetAsyncArgs.get(thisGrid) !== parameters) return; // Prevent multiple calls ambiguity
		thisGrid.DataSet = ds;
		thisGrid.unmask();
	})
}
StarlimsHDataGrid.prototype.SetDataSetAsync = StarlimsDataGrid.prototype.SetDataSetAsync;

let dsLastSetDataAsyncArgs = new Map()
StarlimsDataGrid.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters) {
	dsLastSetDataAsyncArgs.set(this, parameters);
	this.mask('Loading...');
	const thisGrid = this;
	await lims.GetDataAsync(dsName, parameters).then(data => {
		if(thisGrid.isDestroyed) return; // if the grid is destroyed before the async call is finished (e.g. when the user navigates away from the page
		if(dsLastSetDataAsyncArgs.get(thisGrid) !== parameters) return; // Prevent multiple calls ambiguity
		thisGrid.Data = data;
		thisGrid.unmask();
	})
}
StarlimsHDataGrid.prototype.SetDataAsync = StarlimsDataGrid.prototype.SetDataSetAsync;
// endregion

// region allows changing datagrid column data asynchrounously easily
let dsColLastSetDataSetAsyncArgs = new Map()
StarlimsDataGridColumn.prototype.SetDataSetAsync = async function SetDataAsync(dsName, parameters) {
	dsColLastSetDataSetAsyncArgs.set(this, parameters);
	this.disable();
	this.m_dto.Data = lims.GetDataSource(dsName, parameters);
	const thisCol = this;
	await lims.GetDataSetAsync(dsName, parameters?.map(p => p?.toString())).then(ds => {
		if(thisCol.isDestroyed) return; // if the column is destroyed before the async call is finished
		if(dsColLastSetDataSetAsyncArgs.get(thisCol) !== parameters) return; // Prevent multiple calls ambiguity
		thisCol.DataSet = ds;
		thisCol.enable();
	})
}
StarlimsDataGridCheckBoxColumn.prototype.SetDataSetAsync = StarlimsDataGridColumn.prototype.SetDataSetAsync;
// endregion

// region allow changing combobox data asynchrounously easily
let cbLastSetDataSetAsyncArgs = new Map()
StarlimsComboBox.prototype.SetDataSetAsync = async function SetDataSetAsync(dsName, parameters, selectFirstItem = false) {
	cbLastSetDataSetAsyncArgs.set(this, parameters);
	this.disable();
	this.select('Loading data...');
	this.m_dto.Data = lims.GetDataSource(dsName, parameters);
	const thisCombo = this;
	await lims.GetDataSetAsync(dsName, parameters?.map(p => p?.toString())).then(ds => {
		if(thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
		if(cbLastSetDataSetAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
		thisCombo.DataSet = ds;
		this.select(selectFirstItem ? 0 : null);
		thisCombo.enable();
	})
}

let cbLastSetDataAsyncArgs = new Map()
StarlimsComboBox.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters, selectFirstItem = false) {
	cbLastSetDataAsyncArgs.set(this, parameters);
	this.disable();
	this.select('Loading data...');
	this.m_dto.Data = lims.GetDataSource(dsName, parameters);
	const thisCombo = this;
	await lims.GetDataAsync(dsName, parameters).then(data => {
		if(thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
		if(cbLastSetDataAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
		thisCombo.Data = data;
		this.select(selectFirstItem ? 0 : null);
		thisCombo.enable();
	})
}
// endregion

// Find default printer in jsript.net using lims
var defaultPrinterSettings = lims.CreateObject("System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Printing.PrinterSettings");
var sDefaultPrinter = defaultPrinterSettings.PrinterName;
var aLabelsPrinters = lims.CallServer("REPORTING_SUPPORT.getBarcodeLabelsPrinters", [aEntities, sFilterFieldName, aLimitTestCode, sDefaultPrinter]);


// SSL TREE à terminer (inutile car app serverexplorer : InterfaceIntegration.InterfaceTools.ServerExplorer)
:RETURN DoProc("tree", {[C:\Program Files (x86)\Microsoft Office]});

:PROCEDURE tree;
:PARAMETERS dir;
	this := CreateUDObject();
	this:SetProperty("content", {});
	content := Directory(dir, "D");
	:DECLARE i;
	:FOR i := 1 :TO len(content);
		:IF content[i][5] == "D";
			this:SetProperty(content[i][1], DoProc("tree", {dir + "/" + content[i][1]}));
		:ELSE;
			AAdd(this:GetProperty("content"), content[i][1]);
		:ENDIF;
	:NEXT;
	:RETURN this;
:ENDPROC;


// Starlims userguide / mobile userugide
http://tpi-dev-app01.primary/v12_user_guide/mobile_development.html?highlight=mobile

// Ajout de script dans l'html dynamiquement avec client script js javascript dynamic
https://htmldom.dev/load-a-javascript-file-dynamically/

const head = document.getElementsByTagName('head')[0];
const script = document.createElement('script');
script.type = 'text/javascript';
script.onload = function() {
    callFunctionFromScript();
}
script.src = 'path/to/your-script.js';
head.appendChild(script);


// Create new script element
const script = document.createElement('script');
script.src = '/path/to/js/file.js';
// Append to the `head` element
document.head.appendChild(script);

script.addEventListener('load', function() {
    // The script is loaded completely
    // Do something
});


// Utiliser les regex
https://learn.microsoft.com/fr-fr/dotnet/api/system.text.regularexpressions.regex?view=net-7.0
rgx := LimsNetConnect("", "System.Text.RegularExpressions.Regex", {string});

function insertCustomCss( code ) {
	const style = document.createElement('style');
	style.type = 'text/css';
	if (style.styleSheet) {
		// IE    style.styleSheet.cssText = code;
	} else {
		// Other browsers    style.innerHTML = code;
	}
	document.getElementsByTagName("head")[0].appendChild( style );
}

// Accéder aux dossiers server, file explorer server
// Ouvrir appl Diagnostic (XFD) et la lancer, aller dans File Explorer, server


// ImportData Extjs à l'aide
// Importer le package d'un autre client
// 'Standard' dispo dans ce dossier

// Date parse en SSL
DateTime := LimsNetConnect("", "System.DateTime", {}, .T.);
:RETURN DateTime:Parse('2022-10-14 09:00:00');

// Nombre de licences en ssl
GetFeaturesAndNumbers();

// Get phone platform, mobile tablet in SSL (PHONE, MOBILE, TABLET)
device := GetFromSession("sys_Mobile_Platform");

// print imprimer crystal report rapport server
async function btnPrint_OnClick( sender, eventArgs )
{
	const reportID = "BaseStaticTables.UnitsOfMeasure";
	const filter = "{UNITS_OF_MEASURE.UNIT_CODE} = 'df'";
	const parameters = null;
	const langID = null;
	const serverFileBase64 = await lims.CallServer("Runtime_Support.GetPDFasBase64String", [reportID, filter, parameters, langID]);
	/* The JSON object structure :
	  *  { 
	  *   fileContent :   base-64 string enconding of the pdf file
	  *   fileName :   a file name or empty -> this will map to a generated GUID,
	  *   printerName: local/remote printer or empty/null value --> default printer
	  *   pageCount:   empty - we will default it to 1 copy
	  */
	bridge.Printing.sendToPrinter({
		"fileContent": serverFileBase64,
		"fileName":"",
		"printerName":""
	});
}



// Ouvrir le mode of operation designer d'une popup
StarlimsForm.OpenWindow('ModesOfOperation.ModeSetEditorForm', [form], null, {
    maximizable: true
});

//version fonctionnelle
;(async () => {
	const colorMap = [
		{ env: 'starlims.dev.com', color: '#0057e7' },
		{ env: 'QUAL', color: '#ffa700' },
		{ env: 'REP', color: '#008744' },
	];
	const color = colorMap.find(c => document.location.href.toUpperCase().includes(c.env))?.color;
	if (color) document.styleSheets[0].insertRule(`.shell-topbar .x-panel-body-default { background-color: ${color} }`, 1);
})()


// Upload panel close on submit complete 
eOpts = {
	closeOnUploadComplete: true
}

// region QVA - OS005-268 - edit sum on aggregate function to work with textbox type
Ext.override(Ext.grid.feature.AbstractSummary, {
	getSummary: function (store, type, field, group) {
		if(type !== 'sum') return this.callParent(arguments)
		const item = !!group ? group : store
		const sum = item.data.items.reduce((acc, v) => {
			const val = v?.data?.[field];
			const numberV = isNaN(parseFloat(val)) ? 0 : parseFloat(typeof val === 'string' ? val.replace(Starlims.UserProfile.NumberDecimalSeparator, Utils.GetServerNumberFormatInfo().DecimalSeparator) : val);
			return acc + numberV;
		}, 0)
		const rounded = Math.floor(sum * Math.pow(10, 6)) / Math.pow(10, 6)
		const format = this.grid.columnManager.columns.find(c => c.Id === field).TotalFormatString
		return sum.ToString(format).replace(Utils.GetServerNumberFormatInfo().DecimalSeparator, Starlims.UserProfile.NumberDecimalSeparator)
	},
})
// endregion

// Cette putain de summaryrow summary row sur les frozen datagrid frozendatagrid qui crée un décalage decalage en fin de grille
Ext.override(Ext.view.Table, {
	initComponent() {
		this.callParent(arguments)
		if(this?.ownerGrid?.FrozenColumns > 0) this.summaryFeature.showSummaryRow = false
	}
})


// 2023-06-22 QVA OS005-268,282 - replace server decimal separator with local separator
const numberButStringFields = ['ACTUAL_AMOUNT', 'AMOUNT', 'Y_REAL_RATIO'];
const fourDecimalsFields = ['AMOUNT'];
dgdCompounding.Refresh = async function() {
	this.mask('Loading...');
	// Appelle la fonction de base mais avec getData
	const strDgdCompoundingData = await lims.GetDataAsync("F_BatchManager.getMatsToPickLots", [dgdBatches.GetCurrentRowData("BATCHID"), dgdBatches.GetCurrentRowData("RECIPECODE")])
	// Modification du dataset pour remplacer les nombres par des strings et changer le séparateur décimal
	const dgdCompoundingData = JSON.parse(strDgdCompoundingData);
	for(const field of numberButStringFields) {
		const col = dgdCompoundingData.Tables[0].Columns.find(c => c.ColumnName ===  field)
		if(!col) continue;
		col.DataType = 'string'
	}
	for(const row of dgdCompoundingData.Tables[0].Rows) {
		// Format with 4 decimals
		for(const field of fourDecimalsFields) {
			const val = row[field];
			if(!val) return;
			const number = typeof val === 'string' ? parseFloat(val.replace(Utils.GetServerNumberFormatInfo().DecimalSeparator, '.')) : val
			row[field] = number.toFixed(4)
		}
		// Format with right decimal sep
		for(const field of numberButStringFields)
			row[field] = row[field] ? row[field].toString().replace(Utils.GetServerNumberFormatInfo().DecimalSeparator, Starlims.UserProfile.NumberDecimalSeparator) : row[field];
	}
	// Affectation du dataset modifié
	dgdCompounding.Data = JSON.stringify(dgdCompoundingData);
	this.unmask();
}
dgdCompounding.Refresh();


// region VLE/QVA - 20210909 - CC-77 - Fix scroll loop with frozen datagrid
Ext.override(StarlimsDataGrid, {
	bufferedRenderer: false,
	runInViewport: false,
	reconfigure: function() {
		this.bufferedRenderer = this.DataSet.Tables[0].RowCount > 2000
		this.callParent(arguments)
	}
});
// endregion

// region 2022-07-13QVA Fix for the setstyle error that happened when the dom is null
Ext.override(Ext.dom.Element, {
	setStyle: function(prop, val) {
        return this.dom ? this.callParent([prop, val]) : this;
    }
})
// endregion

// region 2022-07-13 QVA Fix for when an editable cell is clicked then its column is hidden, used to cause an error
Ext.override(Ext.view.Table, {
    privates: {
        activateCell: function(b) {
            var a = this, g = b.view !== a ? a.lockingPartner : null, h = a.grid.actionables, n = h.length, l = a.getNavigationModel(), k = b.target, m, i, f, e, c, j, d;
            b = b.clone();
            m = b.record;
            b.view.grid.ensureVisible(m, {
                column: b.column
            });
            f = a.all.item(b.rowIdx, !0);
            if (a.actionPosition) {
                i = a.all.item(a.actionPosition.rowIdx, !0);
                if (i && f !== i) {
                    Ext.fly(i).saveTabbableState({
                        skipSelf: !0,
                        includeSaved: !1
                    })
                }
            }
            a.activating = !0;
            if (!g) {
                e = Ext.fly(b.getCell(!0));
                a.actionPosition = b;
                for (c = 0; c < n; c++) {
                    j = j || h[c].activateCell(b, null, !0)
                }
                e = Ext.fly(b.getCell(!0))
            }
            if (g || (e && (e.restoreTabbableState({
                skipSelf: !0
            }).length | (d = e.findTabbableElements()).length)) || j) {
                for (c = 0; c < n; c++) {
                    if (h[c].activateRow) {
                        h[c].activateRow(f)
                    }
                }
                if(!b.column.Visible) return;
                if (g || d.length) {
                    Ext.fly(f).restoreTabbableState({
                        skipSelf: !0
                    });
                    if (g) {
                        a.actionableMode = !0;
                        a.actionPosition = null;
                        a.activating = !1;
                        return !0
                    }
                    if (d) {
                        a.actionRow = a.actionRowFly.attach(f);
                        a.actionableMode = a.ownerGrid.actionableMode = !0;
                        l.setPosition();
                        l.actionPosition = a.actionPosition = b;
                        if (k && Ext.Array.contains(d, k)) {
                            Ext.fly(k).focus()
                        } else {
                            Ext.fly(d[0]).focus()
                        }
                        a.activating = !1;
                        return !0
                    }
                }
            }
            a.activating = !1
        }
    }
})
// endregion

// fix url matcher validator
Ext.override(Ext.data.validator.Url, {
	config: {
		matcher: /^(https?|ftp):\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/
	}
})


Ext.override(StarlimsDataGrid, {
	initComponent: function () {
		this.callParent(arguments);
		const me = this;
		// If there is no context menu client script, load it
		if (!window.contextMenuClientScript)
			return window.contextMenuClientScript = getClientScriptByName('Enterprise_Utilities.HTML_ContextMenu')
				.then(contextMenuClientScript => {
					document.head.appendChild(document.createElement('script')).innerHTML = contextMenuClientScript
					ApplyBaseContextMenu(this)
					window.contextMenuClientScript = true // So it's not a promise anymore
				});
		// If the context menu client script is not loaded yet, wait for it to load
		if (window.contextMenuClientScript instanceof Promise)
			return window.contextMenuClientScript.then(contextMenuClientScript => ApplyBaseContextMenu(me));
		// If the context menu client script is loaded, apply it
		ApplyBaseContextMenu(this);
	}
})

async function getClientScriptByName(name, doResolveIncludes = true) {
    const source = `Runtime_support.GetClientScriptByName.lims?ScriptName=${name}`
    const script = await Utils.GetTextAsync(source)
    if(!doResolveIncludes) return script
    const includePattern = /^#include\s+[\"<](?<cs>\w+\.\w+)[\">]\s*\r?$/gm
    const resolved = await resolveIncludes(script)
    return resolved
    
    function resolveIncludes(script) {
        return new Promise(async (resolve) => {
            const includes = script.match(includePattern)
            if(!includes) {
                resolve(script)
                return
            }
            const includeProms = []
            for(const include of includes) {
                const includeSource = includePattern.exec(include).groups.cs
                includeProms.push(Utils.GetTextAsync(`Runtime_support.GetClientScriptByName.lims?ScriptName=${includeSource}`).then(code => {
                    script = script.replace(include, code)
                }))
            }
            await Promise.all(includeProms)
            const remainingIncludes = script.match(includePattern)
            if(!remainingIncludes) resolve(script)
            resolve(resolveIncludes(script))
        })
    }
}
lims.GetCS = getClientScriptByName

//region AGCH05-179 - Rendering DataGrid ContextMenu Global 
if(Starlims.Version < "12.4.1") {
	//Ticket: Z00002399B0381 (Rendering DataGrid ContextMenu Global)
	Ext.override(StarlimsDataGrid, {
		statics: {
			_contextMenuClientScript: null,
			_getContextMenuClientScriptAsync: function _getContextMenuClientScriptAsync(callbackFunction){
				if(this._contextMenuClientScript != null) {
					callbackFunction(this._contextMenuClientScript);
					return;
				}
				
				Utils.GetTextAsync("Runtime_support.GetClientScriptByName.lims?ScriptName=Enterprise_Utilities.HTML_ContextMenu&ResolveIncludes=true", 
					function onSuccess(text) {
						var sandbox = 	'(function(){\n' +
										text + 
										'\nreturn {ApplyBaseContextMenu: ApplyBaseContextMenu};})();';
						this._contextMenuClientScript = eval(sandbox);
						callbackFunction(this._contextMenuClientScript);
						return;
					},
					function onFailure(text) {
						throw new Error("Unable to globally load context menu client script for DataGrid controls.");
					});
			}
		},
		
		_loadContextMenu: function _loadContextMenu() {
			var me = this;
			StarlimsDataGrid._getContextMenuClientScriptAsync(function(contextMenuClientScript){
				contextMenuClientScript.ApplyBaseContextMenu(me);
			});
		}
	});
	
	StarlimsDataGrid.prototype.onRender = 
		Ext.Function.createSequence(StarlimsDataGrid.prototype.onRender, function() {
			this._loadContextMenu();
		});
}
//endregion


// SI la dgd est rendered on crée le field et on assigne la précision décimale
dgd_OnChangingCell() {
	if(dgd.rendered && dgd.RowCount > 0) {
		const col = dgd.columnManager.columns.find(c => c.Id === 'AMOUNT');
		const editor = col.getEditor();
		editor.initField();
		if(col.field) col.field.decimalPrecision = 4
	}
}




// region Prepare Parameters get right timezone (Client Script Reports.HTML_ShowReport)
function PrepareParameters( reportParams )
{
	if(!reportParams)
		reportParams = [];
	
	var sep = Utils.GetLocalNumberFormatInfo();
	reportParams.push(["LocalNumberDecimalSeparator", sep.DecimalSeparator]);	
	reportParams.push(["LocalNumberGroupSeparator", sep.GroupSeparator]);	
//	reportParams.push(["PrintClientTimeZone", "#" + System.TimeZone.CurrentTimeZone.Id]);	
	
	// #119453 - Added configuration for outputting timezone on report - SBR - 24MAR2022
	let TimeZoneGMT=(new Date()).toLocaleDateString('en-US', {timeZoneName:'short'}).split(', ')[1];
	let TimeZone = GetTimeZonebyGMT(TimeZoneGMT) ?? TimeZoneGMT;
	reportParams.push(["PrintClientTimeZone", "#" + TimeZone]);

	return reportParams;
}

// For more timezeones : https://gitlab.com/Quentin-Vauthier/starlims-customs-and-utils/-/blob/main/Utilities/Timezones.xlsx
function GetTimeZonebyGMT(gmt)
{
	const zones = new Map([
		['GMT','Greenwich Standard Time'],
		['GMT+1','Central Europe Standard Time'],
		['GMT+2','Kaliningrad Standard Time'],
		['GMT+3','Russian Standard Time'],
		['GMT+4','Saratov Standard Time'],
		['GMT+5','West Asia Standard Time'],
		['GMT+5:30', 'India Standard Time'],
		['GMT+6','Central Asia Standard Time'],
		['GMT+7','North Asia Standard Time'],
		['GMT+8','North Asia East Standard Time'],
		['GMT+9','Yakutsk Standard Time'],
		['GMT+10','West Pacific Standard Time'],
		['GMT+11','Central Pacific Standard Time'],
		['GMT+12','Kamchatka Standard Time'],
		['GMT+13','UTC+13'],
		['GMT+14','Line Islands Standard Time'],
		['GMT-1','Cape Verde Standard Time'],
		['GMT-2','Mid-Atlantic Standard Time'],
		['GMT-3','E. South America Standard Time'],
		['GMT-4','Pacific SA Standard Time'],
		['GMT-5','Eastern Standard Time'],
		['GMT-6','Central Standard Time'],
		['GMT-7','Mountain Standard Time'],
		['GMT-8','Pacific Standard Time'],
		['GMT-9','Alaskan Standard Time'],
		['GMT-11','UTC-11']
	]);
return zones.get(gmt);
}
// endregion

// when in need of making a cell editable, how to edit a cell programmatically
function startEditCell(dg, rowIndex, col) {
	const context = dg.getView().getPosition(rowIndex, col)
	if (!context) return false
	dg.setActionableMode(true, context)
}

const context = dg.getView().getPosition(rowIndex, dg.columnManager.columns.find(c => c.Id === 'AnId'))
const cell = context.getCell()?.dom
if (cell) cell.click()
dg.setActionableMode(true, context)




// region QVA Perfs : Load the comboboxes async
const dsUrls = [
	lims.GetDataSource('SDMSMain.DS_GetProjects'),
	lims.GetDataSource('SDMSMain.DS_GetFileTypes4Upload'),
];
const cbs = [cbProject, cbFileType];
cbs.forEach(cb => {
	cb.disable();
	const onSelectionChanged = cb.OnSelectionChanged;
	cb.OnSelectionChanged = null;
	cb.getStore().insert(0, [['Loading data...', 'Loading data...']]);
	cb.select('Loading data...');
	cb.OnSelectionChanged = onSelectionChanged;
});
Ext.suspendLayouts();
lims.CallServerAsync('$ws.RunRESTActions', [dsUrls]).then(data => {
	// Assign data url and data to each combobox
	if (cbs.some(cb => cb.isDestroyed)) return;
	cbs.forEach(cb => cb.getStore().removeAt(0));
	for (let i = 0; i < dsUrls.length; i++) cbs[i + 1].AssignData(dsUrls[i], data[i]);
	cbs.forEach(cb => {
		cb.select(null);
		cb.enable();
	});
});
Ext.resumeLayouts(true);
// endregion


// region AsyncConsoleLoading
// TENTATIVE de chargement des console en vrai asynchrone, mais problème plus profond
// En fait le chargement des consoles est long ~2-3s sur le serveur et quand on envoie une autre requête
// le serveur fait d'abord la requête de 3s et bloque la deuxième envoyée donc inutile

Ext.Function.interceptBefore(window, "__StarlimsOnShellScriptFilesLoaded", async () => {
    Ext.override(STARLIMS.view.console.ConsoleController, {
        loadConsoleTree: async function (treePanel) {
            setTimeout(innerLoadConsoleTree.bind(this, treePanel), 50);

            async function innerLoadConsoleTree(treePanel) {
                var key =
                    (window["Starlims"] != undefined && Starlims.Navigator != null
                        ? Starlims.Navigator.Variables.Get("MYUSERNAME")
                        : "ANONYMOUS") +
                    "-" +
                    Starlims.Navigator.Variables.Get("myuserrole") +
                    "-console-items";
                var items = sessionStorage.getItem(key);
                var aParams = new Array("C");

                var me = this;
                if (items === null && window["lims"] != undefined) {
                    treePanel.mask(GetResource(null, "loading"));
                    const nodes = await lims.GetDataSetAsync("Console.HTML_GetConsole", aParams).catch((error) => {
                        if (treePanel) treePanel.unmask();
                    });
                    if (!nodes) return;
                    items = nodes;
                    sessionStorage.setItem(key, JSON.stringify(items));

                    me.setConsoleItems(treePanel, items);
                    treePanel.unmask();

                    me.fireViewEvent("onConsoleRecordsLoaded", treePanel.getStore().getData());
                } else {
                    items = JSON.parse(items);
                    me.setConsoleItems(treePanel, items);
                    me.fireViewEvent("onConsoleRecordsLoaded", treePanel.getStore().getData());
                }
            }
        },

        loadNotificationsTree: async function (treePanel) {
            setTimeout(innerLoadNotificationsTree.bind(this, treePanel), 50);

            async function innerLoadNotificationsTree(treePanel) {
                var me = this;
                var aParams = new Array("R", null, true);
                var bFirstRefresh = me.firstRefresh <= 1;

                if (bFirstRefresh) treePanel.mask(GetResource(null, "loading"));

                const nodes = await lims.GetDataSetAsync("Console.HTML_GetConsole", aParams).catch((e) => {
                    if (bFirstRefresh && treePanel) treePanel.unmask();
                });
                if (me.destroyed) return;

                //notifications - reminders
                var items = nodes[0];
                var nTotalNoOfNotifications = nodes[1];

                me.setConsoleItems(treePanel, items, true);

                me.fireViewEvent("onNewNotifications", nTotalNoOfNotifications);

                //fire event only when the reminders are first time loaded;
                if (bFirstRefresh) me.fireViewEvent("onConsoleRecordsLoaded", treePanel.getStore().getData());

                treePanel.expandAll();

                if (bFirstRefresh) treePanel.unmask();

                me.firstRefresh++;
            }
        },
    });
});

// endregion




// STARLIMS LOGO B64 BASE64 IMAGE
// Text + logo RGB
// data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj4AAADxCAYAAADLE73kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFoxJREFUeNrs3c9rHNmBB/DKMsnBBFZLmEAmh2kbcguMDDkusXwayEXyaY5qgVkmh2DpsqeALPIHWL0QGCYGtY5hYdy6LEwIuE32uMGaU+akaR8CgTWMwmwc58dktp7qCbe7q6Wq7upWV/fnAzXytFotdfWret96vypJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMv/23/+7Zi8M7ZPVdGvYE7Ac3rALYDm8e+391f/7r/9ZSSv5g/R/n6Vb+8N/fbO3pGEnBJ2NdFt//vLL1ke9F+HhnlICi+9rdgEsVfh5kH7543cPtk/f+O637sXK/jDdOmkIOl2CwBPCzma6raXb3oeffrGSfv3n8O/k7o1TJQQEH2Dxwk8z/bKbBp9WGoBCxX8vfqsTQlAagLoLFnZWY9gJ7zu83/2Pei+ePH/5ZQiBrTTw7CsVIPgAix1+Qhh4nG7H1374/cM3f/rerRgMgl6StQLVtissDTsr8f2EwLMaH+7+9vlf9377/C+78bGtNPR0lAYQfIDlCD8rMfyEENBOw8+TNATd6wsKZ2EhyVqB2jUJPOddWRt9D/c+/8s/9v7zsz+9k/57O4S9GHqOlQIQfIDlC0BhsHMz3cIYl1Yc/xNaRVb6nha+F1pHWmkImqvAkNOV1f83tz789Ivw9fz9hCB3x3geEHyA5Q4/oSXkQfzf3tff/vbeW7/4SSMGhkG9ECiSbEB074rCTggxoVVnsIXqXPuj3ouj5y+/3O37/n4aeHZ82iD4AITws5Z+eZS8ajXpfvNHP2h9a3t9sOuoX2gFOppVV1jsylpPXo1HGtQ9+eLvh7/+/Z/XB/7m0LXV9ikDgg/QH35C60jo+upvRWl/5+c/PvrG994KLUKNET86ta6wuObOvRhkRv3+3h//+o/WL0/+FELb7sDfddt4HkDwAUaFn5UYfjYGAkTr7V/9rH+8zCi9JOsKa4+7NlCBrqzX/q408JymwWfw7zqOocd4HkDwAS4NQKGFZ3sw1Hzje2/tfOfnP76V870852sDFZo2XqAr67XX/vXv/3x08sXfzxck7NdOtx2hBxB8gDLhJwSQg5xvdb+1vd765o9+cC8ndOQ5jWHkcLArLHZlhd8TAkyjwGsdp2GnlYaeWyMC0o5FCQHBBxg3/JwvdpjXvbX/1i9+8snX3/72bsHQchZckmyBxNPk1e0jigjP3/nw0y/C77mX8/eE71uUEBB8gInDT/9ih3mBZO/tX/1sZUQgqUJYgDAsRDgqYPWSbH0eg5gBwQeoLPyEcT/NEU85/sb33tr7zs9/XHSMThHd3/zhZet3p3+7qEutm1iUEBB8gCkFoP7FDvN03vzpe0fXfvj9Mt1Yg3rPX36581HvxWUhqp0Gni2fCiD4ANMMP2H2VRj0fFG31l68/UVorWkUfOmzbrMPP/0i/PuyafMWJQQEH2Bm4SdvscNBvTT4hPt/FRn/s/9R78WT5y+/fHBJULIoISD4AFcSfkKQCbe5WLvkqd1rP/z+4Zs/fW/wVhJn3/vd6d9av/nDyyJT40PYCeN5evY+IPgAVxWA8hY7zNNOw8+TNASdtf58/pd/hNla7xT92cSihIDgA8xJ+Gkm+YsdDjobx/Pxf/x7aL3pvynqRfbSwHPfXgYm9U92AVCFj1980E6/3IzB5iIh6IQur7UCoSe81h2hBxB8gHkMP6EV53qSjcWZVC/JBjFbiRkQfIC5DT/ZrKtsTM64ukloPTJzC6jYG3YBMKXws/XutffD/zZL/vhxGnhu24vANGjxAabpkzF+5shuAwQfoI7G6arq2W2A4AMIPgCCDzCv4lifcgsO3r3RtecAwQeoqzKtPj27CxB8AMEHQPABauBZiec+sbsAwQeoszItPm5ACgg+wNIEHys1A4IPUF8lZ3YJPoDgA9RekUBzmty9oasLEHyApQg+WnsAwQdYCH8UfADBB1gW3QLPeWY3AYIPsAh6BZ6jxQcQfID6+/jFB4IPIPgAS6V7wffM6AIEH2Ch9C74ntYeQPABFsozwQcQfIBl0R0zFAEIPkDt9C74nhYfQPABFsclM7sEH0DwARZON+cxM7oAwQdYSL2cx7T2ADPzhl1QkYcnq+l/N9LtnXRrpNvq0FVtdoIP2yfp1nGVyxJ6VjAMwdJ499r7K331x2qsQxo5T+3Gr09iXdL9+MUHp2P+zvvpl90R3+7G33H+/f309+zEnwt/39Pz56WP3xZ8livshMK6nW6bIwppv/DctbgFB+nPt9Ovh2kA6s4omD2Yu31498btOfgcm/Ez7Pck/dvu1yh0T/rZ9mIoycL53RvTCiPdnJPt2DO60pPwg5yLjFx1OEGn7yevLI6yk76n4xn8TYX38Qh76d/ZrfhvapydQycwD+Uhft73SuzftYGv4TU66Zej9P20xzjmuwOvd9x3kd4vhLKd+O/NpOYEn/Erm4144K1M8CrNsy0LQDtTbgFa6T9YeM1uTnBdTT+X/Zq0ylX/2T48CSfFcEJtVRyCehdcxY5jdcHKdaPE+1mZ0d806T7uTfgZ57lX5889DSvb8bxTxWcY6qKNGFBDyNwvGPxCvdOOf89XfWG6Gx/rv/BrhJaeGLRrf7wZ41O+QlhJtxB4HlV44gkB6Gm8cme2n2czyW+tW4mfy7IK+yScnD9L99HjdGtU8aIjZnb1FMSFthG7cip9zZoGnhAgQjfRgykE1/B6D8Lrx+6oqm3Glrba11OCT3mPL6kQQ1reS7c76XY7bnfiY51LKprHws/MbV5yVUl2hfc0hsQqvN6MPr1uNebDSpVBJVbqjbrthL6xMdM+x5/9ntiNVoVOX9jcGHislnR1lWsdOBhRaEN3SCvdLuoa6cTXOB8XtDviBPEofc7NKXSx9GL4msRuzmse1vjzvKwJv5E+Z20mY7CqF/7mJyWeP2pQfn/ZDOPSQlBpV1AWV/r+ThbfehK7VaZ8sTLPoedxMrvuyfNjugqf9J0bzuuAo6SmrW6CT7lKMnzIzREVzJ3CQSV73v309TpJ1l3WyKkUqpddVd+fcB8MB5+6DADOdy9n3zdynlPHynm8wdlZl1Yzvu+8k/SD9Dlh8PMkg2rDifTWVMs78+asu2vcGUgDmnV647F7aNahp30+C6sihzH4hPdwXPfjVldXcXmzZjpns5LGaZ3JKo6byevN/ntjvx7jVvCDB/dgyNmoanxLLYSAnAWm60lowRy2kkw+g6y/zLtH1xKFnwpCxMaMA0QVDmb8N4cp5lsVv2Zn4DxZa4JPsUpyI8lvmZmscGUBZytWBLdr3npSN3lXjfsjDurlG+sTyubdGzsjyvjaWRfg+PqvFruK4tKo4jhar9MbjuNs1mb4K0NdcmecsBS304HjNDzWi5MS2vH/O/F53aSmi4/q6hr/YNurpGXmVcsPV3sSbsfPsx279BoDIWlnKfdSGM/z8GQ952p97C7AMCU2rRDyQhCLbTV0+1xyz7aLQkSlg6RnZHfGoef2ON2JeWsa9U93j/8/eBF0u64FUYtPwQM2t0KgnrLZSYNNz62+fw+2+qxUOKOpjvJC36RXscfxOBJ8lsvGhD9bm26uMWefhdCyHwPM1/q3eIG8M+JiIfzcnYrGUC08wWe84NO1S2ptd+jzfH2wbl6oXd6p7Vk46eaEwcYEr/rMcbSUJpmRtV6z91o25IUupOthUHLeStehpTQsTphuYfzdVvKqW+o0BiUXEYLPVEnVdZWNTRmssA9zKvrOUPidbFxL3eVNjZ8k+IRw6eakS3gROc7iejXt5rpV4rkh1BRusYndUKGrqRdDj2OpBGN8BJ9lMzyFPb/bspVzog1Xq127cHJnJ/i7H9gRi6WbvJryfJHNMUJv0dBznMzPysJlLgz2xjiGwnu9rtiVp8Vn+gWaeZF1zQyeQPOnZmaLFvYGHm3GBSiBfEVW9B2n5aZIV3N7zi5Ky9QTPUVH8Jk3p4LPQsg7eV50Q79WzmPbS7rv3lZ8KOCoSCAo091V4v5QRzXeby6oBJ+5czwUfNxTq16ylprm0BXixUsStHMe21zSPbhW4LhgyX384oNOUqzVpcxxVKSF6DT+7rraVHoEn3mTN7Dzgd1SK82cq6qLVyA9X9dnOPQ2lyw0NpO8BTytME6+TsHjsah7Ff3OWeuVOT9VeFNRBJ9KtHOvgB+ebNs1tTF48jwuePPR1lJfnWUtZQ9qUtEwH4p0Oa3E209cqMRaOPPYzdUr+fyD9P0+jl17CD5XLH96c5JkN2w8sIPmvvLOu+VIq+Bnf5wMd+msLcX9u7L3OOrmii0FizwluruKrMtT5CJjXru5xglja+n2WQxAzTiNH8HnyuyMOJjDTJ+nS77Gy7wbbO05Lbnydl4lv7vQeyxrzXya5A8q3bPiMpeoanbXRkW/a173wUUBKFxUf56Gn0cxBDUUK8FntrIT/aj7Na2eXRk/PHm85Lc2mMcKfDUZHphbtrUi7wp2Y6GmtofWndAy9vAktGJ+nmTdW3nv79jNdCmgaHfXyPNl7AprVPS7Zq7vxp6T2oghKLQEPU23B+MsAskrFjAsF37CDRuTWAhHpfS1s8ojqyyP0p8xFuJq5Q2MbJf83E/TzzT8TP+YrvNZYvtz+r53481Wq3R2E0RFigKVfietnE+Ty6dpr19wPBbpCpv32Vw7SbX3GFuN23a6f8Px2IqrOFOCFp9xws+rpcJHXsnESvFRWvl8lW6PzroOlmFcyHy1Yoyawt4b49VaBUPVotpP99tNM7kooVB31wXjWIp0c811pR9vQRHqi2kcNyEAHcRWoDXFTfCZdvjpJtmdcvcKFuhwAIdWoM/ieCArAM9G3qy7wzE/8xCWugOPNpZgbFf7rKzfvbGjOFFS0S6ooYATu7mKnCMP530nxFtLTCv8nAegMBjaEiuCz9TDz2kc63B+p9zjEoX0IIag+wLQVI07hb3MSXbRWn2OY9gJZfpf0v21NXDneiha4Rftgtos+NigXl1uztl3X61pdsuF7i+zjAWfmQWg9lk3QNYKFMZ89Ar8ZAg8YQyGGWHTkA0yHwyVrQk/63bOZ7sxp12Y7XiVOWrLG5t03p21Fcu0bi0mVaSiX+ufsVTiTuy1Gj8Zur3CHdjj8ded0q9pavkRfGYdgo7PugTu3rievOoKu+yKJBzwZoNVb9Ip7KPUpdXn2Vnr1qgtv5t2261YqNg43V0bExyLdQhA3XQ7vwBpT+FXbBvzI/hcZQi6H1uCrscr7IuuoA+En4pkLWiDFXhVJ5i812nWsHyGspg3bsfVIlUap7trobq5LglAWZdyktxJqr27vC4vwefKK5lebAkKBXzrgsJ94Iq7EnktMK3KPsvhk/lKLUNr1gLWHXh0TQCnwsr9tGD4WQ3dXbHLq0hrRWeR9lEYDxVCULpVFYIa1voRfOatsrl+QQuEpD6JbLzNxtBJstqVhhdpant+q49B91SnaHfXvWTBu7kKBqH+ELSVjD8eaEPRE3zmKfycng0gzQ8/qwY7T2R6rT2vPr9wIuotxOeWzdgaHOh8PvAeqlC0dSZU1EvRzVUiBLXjeKDQClS2BeiWopfPys1XW+lsxRaKwQpzPZneqP/Flb9gYfA4rrg9bZs1/dz24n7rb+UJA52PJpz+D2ddOe9ee79ToAWiUfAlW0u4D8NK2N0ku2lw0S4srbYjaPGZj0onGWo9YBwbV3yw13NhSgOdmb4q76e1lLcBiuOltkr8iHpE8JnbSiek+N7Ao2t2zFjmoXtmu6blsJ0ML72wGu/SDvMSVo7jzT+XUuzis6Co4LMQenbBhMKdxYs3lU/TZo334k5umDTQmckr7KKzuy5T60HNYXHGC+5NVpSFRSdkjM98UJAnlzeo+c4M9u3BQOBqnE0Hr2axxNkKrY/ZXeibfY+Gk3To8tpSxJhQ6O6adKZRbbu5YuB5HP99O4bBcRTtwuoqcoLPPHNFPYn8AeJhCntnBr+7lQyPhQmtPu2a7s2dZHisVBi7dGigMxWElkmW66h7N9ejvtASbip6p+z7SX9mu0R94YJ6BF1dgs8iyBvbM6uZH+2cE8zanN6/63LZQOe8AfcGOjORCrq7atvNFW8e2n9xFgLQ0/TxZsnQU+Y4/ESpy6fFp9zV/Upf5VDVazaS4aZLV9blPpPBk0dvZq0ToSw8POnk/A0hjNWze+jujf30PW0OlMtsoHP4Xn0rn68qfLnQVeE4LW+S7q5OTcvdQZK/zEY4dx2k39+Noa4zuD5R30rW4TmNZdhfs6DFp3gFGwrf06T6mUN5B4RR+8XlzTram/HfkNe6tFHzQcGjBjo3FDkmMG5lXMturthK07zkaY1Yr4QWoK/S7bN0exqD+mfJ8DjCIpZmkUfBZ1otCg9PQvPi41j4tuMMoipee3VEkDq04wsbnEVV1eyR4rLVj49zruaatd2rWYtZJ+c96fJibBN0d9Vu0cLYjTXO8RLqmUnX4NlT2gSfcYNJKIBPc1oVJr+ZaNYa8CjnO91YkXL5PmzmXAm1K+2KnOzEfK/me3gnGR6/tOGWKkxonMUMa9VtE0PPVd13MbSOtRUzwWdcp0n+yPiVs0A07uJuWWh6muQ3X0rqxU3/vlxFZdPXT4eu3OocErIbu+btTzfSZRJlQ0xngqnfVxF6wvn9qlpGy67uLPgwdOIPheh2MnpaYLiL9ePCXV+hBenhycEFoWfflOHC4TEEitWhE2q1d2Evq10wnNXpGLifDC+wGcrxfYWQcYzR3XVUw7d5VUFtx9iey5nVVST8PDwJ4efRiLCylmTTl88P5jCFsL/grcQKej25uN82dNHs2OGF5a2QfNXjAMLvH2wF3DgLvFcbyCYVriAfDwW6sNhhvd8XV6fM7K5adXOF4PHutfdvxjpjbZbHqS6uYrT4FAs/IciEgty94Fnng1nPB0Kfb6Hw7xYIPZoni8rGXjUHHu1deWtZFgLy/oa6t/p0c96Xgc5MomiYqVU3V1/4OU23cMGcN06uauG8c1PoEXymcfI/TbeqC3J2V2yhp6y8IDEvY6PyZuQ1F2Cf55VRA50ZOxgUDD9HNX+f+/GieRqh5Hyx0Zu6t8rR1VU+AO3H+xltxAp4dcwCG7pF9q9oBtK4Bq/6r+pga+T8LZ05KR/ttHyEbs3X1/AJAWF6LVKnOfujV/H76qXvIYT+9YHvbCZXu+DmVZXBaR23vRL783TO9nHZz+IouXzV+k5NysNF4Sd8plvvXnt/L14EbSaT3VC5F+uPdh1bw+bB1+yCCb26T9St5NX6Cys5J6jztV6ezOQeUgDMpb4Vmd+JdUZjRBjqxS3UHWH8aLfm9ysDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABq7f8FGAAgcw0CCzOV4AAAAABJRU5ErkJggg==
// Text + logo white
// data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj4AAADxCAYAAADLE73kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAE1JJREFUeNrs3Y912si+AGD5nlsAt4LHVnBJBUsqCKkgpII4FcSpgKQCnArsVABbgb0VmFQQvwr8NJvhLYtHWP8ACb7vHB0n2AhpGGl+mr9ZBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8MvT09NYKjxLk1G+DaUEnId/SQI4nwI+/zHIf87z7eqcC/tw7vl2mW+L/L8hHUZyCAh8gBNycXFxn//4Pd9+5Ntjvi1CwZ9v03wbnEnAM8m3m/yfd/GlP2KaLOUQADjNwj8EOg+xxiPU/PyM2/wUm8JiU9YsnuNT/PdknQZyBACcfvAzioHAusZn/vS3h743hYUarBjY3W2cVzjXcfwZzn0iJwDA+QQ/g43AYB4DoM1AYR0sTHt0Tn81ZW2dw0M8t1n8/13s7wQAnGEAtK7t+Rlrei43moWeNn4372LAkGjKetpxPotz6c8EABQHD5eJGpKrp7R1/6DhEY93UFBD9bRRgzXZ+v3MNw0ArIOJ8VatyaKg6WjTzSGbwuLxzHccz7rP0vYxT33DAMB2YDFK1KLMN0ZBFdlbU1icc2f2wudvjlLbPi79eQCAwkBjkKgx2dX/pygIGTQ8hl1NWWWO605/HgCgbPAxKwhqJgW/K2oKm1T4zJeasp41s8XmrVQfH0EPAFAp+Jnu6EszKQg6impmZqlmp9iUdfVCU9Z2Tc50R4BkUkIAoHbwM9rRvDXbmAn6qULgcrmjtmZX8DTdmG069XuTEgIAjYOfwY7+Nj+3lr/Yh6sXAqwHnZgBgLaDn/kLNTlV+uiUUaZJzaSEAMDeAqDLBp2Oy3ooGUTNfSMAwL6Dn0mJZq31MPOHiv14LksOm5/6JgCAQwU/oxLz7DxU6P8zKzFRokkJAYCjBT+Dkk1aRUtJVB0af3fMNcIAALIKExrON2ZkXi+GWuW9OjEDAJ0IfqYV+/GMKwx/v5LCAEDXgp9RyWBmkVhQ9MmkhMC+/UsSAG25uLi4z3/8lm/3LexulW+v833eSllA4AN0Nfh5DAFLvl032M0y317FQAoAoPt2TEC4q6nrTsoB+6LGB9inP2u857tkAwQ+QB/VaapaSTZA4AMIfAAEPkBXxY7OjxXfs5RygMAH6KsqtT4ryQUIfACBD4DAB+iBHxX+9g/JBQh8gD6rUuPzKLkAgQ9wLoGPmZoBgQ/QXxVHdgl8AIEP0HtlAprHGCQBCHyAkw981PYAAh/gJPyvwAcQ+ADnYlnib35IJkDgA5yCVYm/UeMDCHyA/ru4uBD4AAIf4Kwsd/zOiC5A4AOclNWO36ntAQQ+wEn5IfABBD7AuVjWDIoABD5A76x2/E6NDyDwAU7HCyO7BD6AwAc4OcvEa0Z0AQIf4CStEq+p7QEEPn3z9PQ0yrerfLvJt7un537m2yLfZvk2zbeBVOMM/SgZDME5lR+DWC7MYjnx8JS2iFsoayZNypG4j6cdn7P5+9lWWff/f9fH9P63LNcss+Y/LvPtXb4NX/jz8LfjuAXz/P3X+c9vFxcXy0MEZvmPWdfSMD/31x34HqfxO9z0R35sV30Julv4blcxKAm1L/clZ1quI+T1TyWCobLnHs571Je8VjMvFvmYn9P9AY6pdBoX+Nz2PS4/pnC/nZ/IvedDhfQdb/0M+7jNf3zPz+e6xjW/3NpfyE+P2fNa2EnIb/Hf7zLONuiZxFqcNsz3XQOU73/81EEd+S4fCmroBj3Ji/v4bh/i0+ew5WMdJp4oxw32t+hTXmv4FL5tfKBjWjS9v+0jGOvzvSf/+MsWy4/Ne9ZlzeN5lqcSeXEUX7/re42Ppq4atTzxQr6JtThtCFH/3TpjcfAnrmFBDd30jJMmpEm4iT7Egq+VAKigJmklJ560yR4eIiY9vd+EwP8u+1VD23aahP3NYmCyj7LkXbwP9L6cEvhUt3ihQFzm2+d8e5tvr+P2Nr52+0JBsxD8HNyuatsPkucv4xiYtxUI3pcIhjgdgzYDlXiPHPYtEeJx3x0gcBi1fL3ebgSbk63Xekkfn2oZd16QaUOb6Nd8+7JjWO5t3Me6X9CnghtE6Bz9ag/De1cx+GriU2Kf33r8fY6yjbbyVDAaqn4P0QdrD8Ix/1Hh7/8bC5PRjsIrNMlmNfoSpPLiYOM4OX1v8u26pX31ro9JvNcssvZreV66ptvw58a9YV0GfM96WutGtYw72dH7fVDnQijoW7Loat+S1LH2PZBN9GvZdtOD80j18bmqua9hbNv/uaMfwajh8V5t9PGZN9yXPj77P6ZFS31QBi0dz88+9S+M19TPA3efnNe8t48TefEq9klau9u45+jjc+JSo2Zuw8iAOrUzcTTGq+yf1f6f6+6P6jej7HmT5bdEDcSk7Q6+XRaaneJott/y7UtBzU/TEWSbed4aXedj0sJ1OzlwrUkb5gc+5mV+Db9veZ+3W/fJXhP4lL/Ytgu/Vb41ylwxwHkfC4LXfRk+fSKmide+FFzUZ9fXJ+TNfPtYkMfHDWsbVps3aVnxbLRxHb3pWdkR7jPjA35kKEve1gmW4va4dZ2G11axH951/P9t/LtlZvLRkw585omqxOkZpsPJNHUlqp7nG7/bbvL62fFzaa2pq2D/N203AW40dQ1b2I+mrv0e06LFJphhg+MYtNlkdKC0ezhg89adiXHV+LRplHgivpYsvQ16QtC6fYP4uvHv7VqfwTkGuhs+pmp9WngyNaLr/Ewavrc3BXvN0WehJiXUPIcWgH/IfnWN+Jilp38I73urm4TAZ5+Bz1KS9Nr26LTl1gy4qaD2bIe2x+BkmQgGm9TW/HAdnaUmI7Le9OxcqwZ5oQnpt9DEnBpJGu5R+RZGDof+d++zv5ulHmOg5CFC4LNXouqeis0D2wX2t0RBvz1PxehQTQsdlRoa3yTwCcGl/gFn+BBZZ1RgbMLp2/Dp3yv8bQhqStfYxBaHMEfcKgY9riWBj8CHQts1N6uCZsuvLT+t8s8b96Nq+ZOzLHlvrHMdlQ16uhQAVHkw+FzjGgrB0m+CHoFPFzM0HRGbZrZvoN8KbirL7Hlb+lTnQdipzIy+dWpuyjQ1X3fsobRKObGSdQQ+XfMo8DkJqZvnlx1/n6r1uTzTtPsf2YcSvpcJCKo0d1VYH+p7j9PNA5XAp3Pum1y4HF+sqZluPyG+0NxynXjtXJu7xiWuC85cfj3dZu03d5WpIXqMn91XmtEFPp2T6tg5kyy9Mk08VX174Sb+mAh+huc2tL1gBfuVPjoUuC15PZb1oaXPPLRVlfvTmU+ZIfDpoNSTf5g07lLS9Mb2zfO+5OKjZ93JOdaUzXpS0NANZZqcBnFG/JfyX9m5cLrYzLWq+PfzOFnkUBYS+BxdwfDmYNZ0kUUOUninlhz5WvK7D80594mgd3gG6RbOsWhF6a9yFgXXTNnmrjLz8pR5yOhqM1edYGycbw8xADKYQuBzdB8LLubperVaSdRZHxI3yusK708V8p9OPOgJtZl3WbpT6WeTpfGCtkZ3TVr6rK6mwa4AKDxU/4xLxkzVBAl8jvEUs8rSU/dnsXBYrKN0qdWpAnyUPe+YW7W2IvUEOzmlp7FwUw01Y/k2i2uTheat1PndW0yXEso2d0135MlUTW3dzzpWmXHdwq4mMQh6iA/ZM4NrOHQBMS2xWNzPuLDp5MTOvXeLlBYsMDussZ9ZYj+XHTnH8bkugmiR0k6l8WLrfWUWFL2peO0+7VpAuGv5oe2FVRPXowdtNT4HieJDBL+eKrzwSSb7NWphvap1+HmpqvLghUjREPZVjd2laonOaf2usEbQKyO5qKBUc9eOYLrMg+N1lxMgXi+hvNjHdRNqfea6Wgh8DpWZl9mvlXI/l8zQ4QIOTQfrqkqd1g4jVSPzreZ3HoKl5dbLwzO44YSCJQQ8H2UnKirbBDVJPLSUXYn9W9cTIQ6Q2Ffwsw6AQk2XKVYEPvuP5GNfh/VKufcVMum6vfZKALRXdYewV7nJnlqtz30MdkKe/k+eXu+tBUTNe2TZzr3vSr62bdWXvBmPM5QV++yIfWmUMQcXOpzFviAPFdppH/pSa9CnPj4FfbGmLez3oY0+Qy2fa6qPzzy+XrSl+izNenjN6ePTnTReJN57U/K9w433DEq+Z9bH/BCvv8Ue++Kp+eGoQdBVbNoqY9qDc+pT4HO3qxNkywXT7Mjnmgp8rl54T1Gny1HPrjOBT7cDn2nJ917WeM+oz/khXrfzPQU/44xCmrr2JFRthqaw0CE0+1XFGRbD3NXGO9dDv70bSvZ8/pnrlnaf2s+0h/kz5MVUvx1Pi7SpTnPXSTVz7bgGl6EpOf/nf/Ltbdbu6vKavAQ+R8/g4SL9mG8hg7/fkbnn5mdoRarfzde2vsvEzXzQx6A1jlBcbr08FoDTcoBdJvgZxbmkhll6Qdy6AVUv0ij0h4r96doKgiykLfDpXGHz244aCJF6A/HGuT1K5LblmYZPaWh7stZHp3taVHZ014es3BD24NupJtZWEPQ+8XBS1kTWE/h0LcJ/XxD8jLTPNrK32p6N7y/ciFan8L3F5oIvWy+HoOeTrERLytbOhIL6LJq5qjwo51sYCh9qgarWAP0u6wl8upipi6L5N1KnuoIJC4PWR1Bk6an0+7pqe2o+qksBOG096JUMfsI1VaZ55usZpmFIv9BSUCXgU2sr8Ol0obNN22w9kyNf7L2cmFJHZw6gzfW0bs8xAeN1+r7CW5QjAp/OZuZl9rzZxJN2PV1onrnsaT68TjxNjrqyHhm911awct9yf72+Xaf3WbVaHwQ+nbWSBM1UWMl53971OBlTtT6fdHSmhQK7bHPXS3rdqTnOn9X0erJeXkP/lgSdICM3l+rU/PYAaTvfCrjCMNJprEHpW+G0zI89HPd04+Vwkw5NXu9lMRoKzV1NRxr1tpkrBjyL+O/XDRb8LduEtZTlBD5d5om62Q0lBB7j7RtkhbWCmnz21+x5X5hQ63Pd0+T8mD3vKxX6Ln1ruM4Z3GbNpuvoezPXzUbQEgZcvK16PrHpuWx54YG6gKYugc8pSPXtOdTIj+vEDWZ87PW76opPoakO9zo600beavIw0ttmrrh46ObDWQiA7qpMFhqDnirX4Z9yncCnjcw7aLu/Qywgt6suPVlX+E6y50PYV4eqndhxM+/tPDj5OYV5fU6uo7O1kDqhyeiu257mu3mWnmYj3LvmceHjq4K1x4Zx7bKHGg8ft7KbwKdp5g03urs9FGipC0Kv/fJShfHnAx9DqnZp0vNOwUUdnYeyHEcIXnrZzBUfFqYv/NkwlivrhZUf1oss56+FgGe7H2EZZzPJo8BnTzUKcfXtRcx8l3EEURv7HhUEUt+kfGnbo6jaGj1SpYYkNcR0kPVw8dKNc1om0nGQafKiWb6qe332btLC2IxV53oJ5UzTOXg+y20Cn7oZN2TAu0StQuPFRGNtwE3iV0uReqUby/aT0HWD0RJNnNL6XWsfs+f9lyaaeWioTnNXr5pt4r3pWOsu3vdxVKnApzses3TP+BC03NXt8xCDprssXX0pUi9v7+tyVXiSvU7klWGfg4TYtJBKTwvp0kTVIOb2SA8zdYOecH8/Vs1o1dmdBT48u/GHTPQ6Kx4WGFaxXpRt+ood1eY7gp4vhgyXvrmEgGKUuEGujnhY1yWDsz5dA1fZ8wk2Qz6+kgtpcF+tEvx87+FpHitQ+6jFQODTZvBTVKCGAvgmLwh+hqAm1AKFQnljm8Qe+yHYCR3VpkWFZv5ZH6V4aakZko/dD6Cok/Ow52mdeoL8oKMzDVQJZnrVzBUDj1fZ4UfnvtfERds1DINYu7MP856kwbbFkY5jmDiWh46kUSqPzPb8mePEZ14d4LxuOprehzDe0/lcdW1IfYU0XlS8n5Zxs6/8cKC0u4wPxfv00LTPqRofCmt+8i3U/KQ6fNb116rY+X61yVaTaj7qSt+o1Ii86Qmk+fuC2qyx7Eid+2nJmpzvPT/PL7H2Zx81MevJRl9p3qrGkhU1MnJcz2gSC+BRzQwbmkW+9KnTXva86vZYF9swcSy3Hckf13n+eJNtzcYdAoQ99t96TKTHquXzWuXnEIL+N1u/epcdd8LNY+XBfV23qwrp+dixNK76XXzPXp61/rYn+WHntRMeHPLr53N8CHqXNVtQeRXLj+uelR/d+U4kQTMb60T9nv09/8IgcYNaz/XyxyHWkAKg8+XGf2OZMSwIhlZxC2VHWIJi2fP1ygAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgF77PwEGAImDz8+8vg3FAAAAAElFTkSuQmCC


// Decrease group header size (height)
document.styleSheets[0].insertRule('.x-grid-group-hd { padding: 2px 10px !important }', 1);


// Show create table like en DS
exec sp_columns YourTableName

// Variables globales SSL
WorkPath: GlbDefaultWorkPath
WorkPath Temp : GlbDefaultTempDirectory
App base folder : GetAppBaseFolder()



// region Add an image to popup ShowDialog title
Ext.override(StarlimsInnerForm, {
	_ApplyText: function (value, justText) {
		const parentWnd = this._GetParentWindow();
		const frame = this.up('StarlimsFrame');
		if (document.title === 'Remote Form' && value) {
			document.title = value;
		}
		value = Ext.String.htmlEncode(value);
		if (parentWnd) {
			value = {
				html: `<div style="display:flex; gap: 1rem; margin-left: 8px; margin-top: 3px"><img height="30px" src="RUNTIME_SUPPORT.GetImageById.lims?ImageId=EB259C99-E18B-496C-8847-2E951DF9D38F" /><div style="display:flex; align-items: end; font-size: 0.8rem; line-height: 50px">${value}</div></div>`,
			};
			parentWnd.setTitle(value, justText);
		} else if (frame) {
			frame._OnRetitleRequest(this, value);
		} else {
			if (this.getBubbleParent()) {
				this.fireEvent('formtextupdate', this, value);
			} else if (window.Shell) {
				window.Shell.onFormTextUpdated(this, value);
			}
		}
	},
	_SetText: function (value) {
		if (this.m_dto) {
			const textEl = document.createElement('p');
			textEl.innerHTML = value;
			const justText = textEl.textContent;
			this.m_dto.Text = justText;
			if (!this.rendered) {
				this.on('render', _ => this._ApplyText(value, justText), this, {
					single: true,
				});
				return;
			} else {
				this._ApplyText(value, justText);
			}
		}
	},
});
Ext.override(Ext.window.Window, {
	setTitle: function (title, justText) {
		var me = this,
			oldTitle = me.title;
		if (title !== oldTitle && me.headingEl) {
			me.headingEl.setHtml(title);
		}
		this.setHeaderConfig(title, 'title', 'setTitle', false);
		this.title = justText;
	},
});
// endregion



// region logo + user info in topbar
Ext.Function.interceptAfter(window, "__StarlimsOnShellScriptFilesLoaded", () => {
	tb = Shell.getView().getReferences()['app-topbar'];
	navBar = tb.items.items[0].items.items[1];
	
	//SLIM-408 - display user infos in navbar
	const {starlimsdept, myusername, fullname, myrolename} = Starlims.Navigator.Variables;
	navBar.insert(0, {
		xtype: 'panel',
		reference: 'topbaruserinfos',
		flex: 1,
		items: [{
			xtype: 'label',
			html: `${fullname} (${myusername}) - ${starlimsdept} - ${myrolename}`,
			style: 'color: white;',
			left: 0
		}]
	})
	
	tb.insert(1, {
		xtype: 'image',
		reference: 'topbarlogo',
		src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAoCAQAAADUSyVDAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAACYktHRAD/h4/MvwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB+cFGQ4gEznAXokAAAAvdEVYdENvbW1lbnQAR0lGIHJlc2l6ZWQgb24gaHR0cHM6Ly9lemdpZi5jb20vcmVzaXploju4sgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMy0wNS0yNVQxNDozMjoxNSswMDowMPs4GTwAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjMtMDUtMjVUMTQ6MzI6MTUrMDA6MDCKZaGAAAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDIzLTA1LTI1VDE0OjMyOjE5KzAwOjAwGtDqKwAAABJ0RVh0U29mdHdhcmUAZXpnaWYuY29toMOzWAAACURJREFUWMPN2XmY1eV1B/DP786+wjADDDADhEXDsGZQ3EAEJa4QldjGVJrFUttYKyZGTU0fydIsapMYn5SY0KdqYvKQxAWrWB2s7Eu0IATEQVaVVRmGGWCYYea+/eNe7uzg0/YpnvvPu51zvu95z3vOue8vchYpnGr0cYtmM4y2x+1WJwajswntdKBDvzAr3B6uD1UhhBBWh8NhaZgWegWhdUsfLwpCUagKPwt3hKYQQgjxEEIIx8K6MDf07B507GxCxjC5Vvq6DJzyiCDuUn2650w/e6DBVGt9QnkS7CEnve0Fkzxi68cXdE+bfDHZbnG/JUaoVqz+dExnF3SGAif0bYNlq8n6+Ymajy/oSItSZan+p72j1NM+OD3bWbyIaHFcicxU/yrlFiYuZXSaOH22QS+2w3otyX6eSkdlnimx/J+DDqlf+1778RSdUO5udzuY7F+h6Mw6YroQ/r+B3EUvUuJcFcpkt9WFdFPdb77d/tLboI9izR/FHImBtFAcssOAkP4/h50UOiSMTSmIwqTw87Ah7AsHw/bwTDg/fCqUh4KQEXJDcfjrcDyEEMK+cE0YFV4ILaEmXBlGhu4kR6F3iIU20eMik9S52aMW6AL2mcuXJM84V3oiOTTMrWYrTvZ6W+4O0x1S41UH5RgrB5SaZ7abzXS+Wo2EzvoyFZjuLv9qXivoLO9YIdt1gr2OaNCkSZNGDeIJSB+h7hrsi/7FfjDdN8X0TM0tVOvvpOlpqIMO2iQ/NTfQdHst8YKbrbetC9gXu9Mow9xnWSvotb5stB8rco4SA0SakS4mps5bCUHdAw+Jjd/m5WQCHusRi50rLbmg2gr3pXpxac5JVhzs9Uv7/UGuwwZ6w212dIK9109VeNjTtrWCPupx082xzTZ7xGQrUKhQngwDnO99vzl9csXVmrwMYm6XabWbkjPNfu3qlKPQzxZFAmq9ZLkJviYf5ah1s5esa3XSCGo12uw6jznaNiPWecogD5nmqEimLJnSxTSrs8NJFb7heOjS1gGKXOtH4qDclTYICpMLVmp2aRuG4V50QIbnLDXEP7TJirT4jWsNVqUeMfEAx0x0zEF7U2k8OqV4t7VuauOHkKlEiWCCP5l/WjsX25lsVyjzvCgZUpu94IZ2JUOBmAWqTTfHoA5yLpHut673T14XrLcZXOp8U9TLbFd7RAnYLd1AimT4e1V2d7Z1SMCYZr0h3gKfEFOrXlwMmzQZn1r+oVVec0SBqW7rQlO+WQ6o96r37XUI9PWPxsrFHe5P76CaDP/mz+R1CXy0Ob4m3qWLXO6YJ11hu0YUo8jQpNAlJsjCERsstVOZ613geVWauyjZ3vWE9zS1G/u0CclLfJmeKZZw6tiCe+12j9wuYX9JlUXtb3aAHDf5td12+ZwqhxVijB1OyNNks1sstto2BSabrRR8xhZrTOyko9q7TiYONyk/5spU3PmkUeltFKeZ4HPyXeOwX/ibRNLtQD18y1t2tcJObvZGmZbhP13gRllGoN5aB+zzqqXeN0COI9hpsJ6ykeV2TxsqX24KEo2ecrJDaO1hZKpdoKz1cCJ/YZDfe0+eMg1WqexwIRN0nn/2FQfaZc0p7vVNxxCs8bo074gstssXpKswS7Y7zbUQFLnIZco1Omi5f3eFQtmCQ6YZoVZ1J50ZDvmhWfpjidVRytJDzfRwMmAldjdcpXGG6atAhuCEQwpV4D/Mtc5JRPqa6T7Pm5M4UmTI1MNA5SaqNMpuL5ohw2dtagMkltLVR6VeGmW6xi32mmxbB+fr734HZLtOkxu8F6UmxrnJ64Y4YYN1GlJc2fLkyhTXqM45nlOOGqts1WKA8QZa4C618pWrMM5IB1Q76iozsdoz5qjxt1Zq71JtKUd/37PRLfqbYl0H0Bc7aag092p0ubooFTNmu1ukSqPLvOkhG3VN33abGo366YMDqrxksyEuVC6uwThPWSjLJA/L9Su7fEG1OYnk3m1igj9XqditZnqmdWUg22e8pllkkU+aYl0sCfke93jOTlu9aZF0T7rw1EWMUj/wIxPd4EWbbbJNrVrTLTBP3Hd92QI/8BO1Zpii0O9scJ8af9U95DZ/rFbo401xF3TY0FB7DDHdIQtlqyTdGP2dZ6q5enlRmZiFKuV60EJHLVDbwSK1gp+5yLMK7ZXlIgV+oVJPk31JhTnoLU9/WzzgRjmOJBLE6WrESGCPPZptNc1DPmwzNcjr7jLIE5b6QCkxQw12ueNy5ZhoqgkuFMyXocESxzvILnedn2v2bav81jO2e9JxI/3AA0rMdMA+lKHYd2zzqDsNNLMbT+5IKwy3xJi2thaTZqB8W5yr1EzzSPesXmZ4RKa4Ga62z/e8rVCj5k7Bp9RX1aswz3suNViJtfZ5zRg7HPd9v1TrBIaIqbUYDX5qo7sd9fyZMVvjEu+KG9wmfbWo83krvekaMU8n9pEIUVlqHVKMXvpptNxqBZ3+yB/0HasctEuZwWaZ4piVHtOiH5rtdwLZSuXZmnq9WGK+B1WeHm+y/Fyo0TK92k1tVSNfvWKFsiWrsDqNzrHfMa/4L4tUK3SZT6UqtlZqUWOHGhu9ZYsa9Zr0N0qOuNbLWi5diV1t/Hi5Dw3/CJbmHcX+oDf0P8W/T705+tqtR6K4iKHBKwYYrsFiD3rMEdU+lG1NR1tEsF+eWY55w1zfsMt6pZodaLc0ZuCpVBPBSdu7LAo627pBH/v10Yc9rbfgjwYYarSrEqVuIuT9yggTrVDkj9JVudZXzbe387sF6jxnBHjFeltMMc0Cx7Wu2K/B7xMvdMmRMWYo7foVpIP0JmuM19PX9UjwJ+2/X5lIhoggXbFR+LHPe9RuG43Wy+VW225qt0c42nwP6aG/Ol/xJ7tMSs0GhcZa5mLnaVAsINs6FS5uUxZ1R3F5JltutmHm6CcDwQQDTDDYTqOdUB2FkW6VbbVlrjJOmh62W+SwWQq6OcUjfme8qQpl+cDLNvms/DYRLZIjLsOzilwiLmGctA4VcneUJttxOU563BS9BeTartFQz5oix+NRuMADskXidnjDUWmGOE/eaZ6aIpF6mx0S9DayHeBT1m717taRj/blJ7Rrt34byBAXxDT4brosJXIEkd7GaxKTJXbGTFBioCZkSvt//aITOSEzCpkKP65fv7qkoO6/AXzveVftcbPeAAAAAElFTkSuQmCC',
		listeners: {
			el: {
				click: function() {
					Shell.refreshConsole()
					Shell.refreshNotifications()
				}
			}
		},
		style: 'cursor: pointer;'
	})

	// // Change icons color
	// addCssRule(`
	// 	.x-btn-icon-el-default-small {
	// 		color: #475569 !important;
	// 	}
	// `);

	// // Change logo color
	// addCssRule(`
	// 	.shell-starlims-logo {
	// 		filter: invert(0.8);
	// 	}
	// `);
	
	Shell.getView().addListener('userInfoSwitch', (infos) => {
		const topbar = tb = Shell.getView().getReferences()['app-topbar'];
		const navbar = tb.items.items[0].items.items[1];
		const currentInfos = navbar.items.items.find(e => e.reference === 'topbaruserinfos')
		if(currentInfos) navbar.remove(currentInfos)
		const {starlimsdept, myusername, fullname, myrolename} = {...Starlims.Navigator.Variables, ...infos};
		navBar.insert(0, {
			xtype: 'panel',
			reference: 'topbaruserinfos',
			flex: 1,
			items: [{
				xtype: 'label',
				html: `${fullname} (${myusername}) - ${starlimsdept} - ${myrolename}`,
				style: 'color: white;',
				left: 0
			}]
		})
	})
	
	// region fix profile menu position
	Ext.override(STARLIMS.view.main.MainController, {
		showUserProfile: function (userProfileButton) {
			this._userProfileAnimationInProgress = true;
	
			this.userProfileForm.show();
			const [x, y] = userProfileButton.getPosition();
			const {width, height} = this.userProfileForm.getSize();
			this.userProfileForm.setPosition(x - width + userProfileButton.getWidth() + 40,
				userProfileButton.y + userProfileButton.getHeight() + 10);
			this.userProfileForm.focus();
			this.userProfileTip.setPosition(this.userProfileForm.x + STARLIMS.view.userProfile.UserProfileView.prototype.width - 65,
				this.userProfileForm.y - (Ext.isIE) ? 24 : 17);
			this.userProfileTip.show();
			this.userProfileTip.getEl().fadeIn({
				opacity: 1,
				easing: 'easeOut',
				duration: 300
			});
			var me = this;
			this.userProfileForm.getEl().fadeIn({
				opacity: 1,
				easing: 'easeOut',
				duration: 300,
				callback: function () {
					me._userProfileAnimationInProgress = false;
				}
			});
		},
	})
	// endregion
});
//endregion

// Add a border to the topbar
addCssRule(`
	.shell-topbar > div > div {
		border-bottom: 1px solid
	}
`);

// Change icons color
addCssRule(`
	.x-btn-icon-el-default-small {
		color: #475569 !important;
	}
`);

// Change logo color
addCssRule(`
	.shell-starlims-logo {
		filter: invert(0.8);
	}
`);

// region fix narrow rows on grids with a checkbox column
addCssRule(`
	.starlims-narrow-grid-row .x-grid-checkcolumn-cell-inner {
		padding: 0 !important;
	}
`);
// endregion



// lockfree / read only session, allows requests to be not blocking
// <add key="EnableReadOnlySession" value="true"/>
// https://tpi-dev-app01.primary/v12_user_guide/configuration.html#read-only-web-sessions





// region VLE - call server info
	//! NE PAS UTILISER, CA FAIT PLANTER LE LOGIN et les callserver en general
	if (!(lims.__hasInterceptor ?? false)) {
		Ext.Function.interceptAfter(
			lims,
			'CallServer',
			(scriptName, params, longProcessing, returnType, onSuccessCallBack, onFailureCallBack, eOpts) => {
				console.log('server call', [
					scriptName,
					params,
					longProcessing,
					returnType,
					onSuccessCallBack,
					onFailureCallBack,
					eOpts,
				]);
				lims.__hasInterceptor = true;
			},
		);
	}
	//endregion














// region QVA Update 2023-02-17 Enlève le chargement inutile des consoles lors de l'ouverture en NEWTAB
	Ext.Function.interceptAfter(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
		// region QVA 2022-07-28 PART 1/2 do not load console and reminders on NEWTAB mode opening as they are not visible and buttons to show them either
		const originalLaunchApp = Shell.LaunchApp;
		Shell.LaunchApp = async function (appFormId, args, mode, appName, parentCaption, menuCaption) {
			if (['NEWTAB', 'UNIQUETAB'].includes(mode)) {
				const newTab = await window.open(getTabURL(appFormId, args), '_blank');
				newTab.webOpeningMode = mode.toUpperCase();
				newTab.parentCaption = parentCaption;
				newTab.tabName = menuCaption ?? appName;
				newTab.name = String.IsNullOrEmpty(appName) ? appFormId : appName;
				newTab.expectedTitle = menuCaption;
				newTab.appFormId = appFormId;
				newTab.appArgs = args;
				newTab.focus();
				return newTab;
			}
			return originalLaunchApp(appFormId, args, mode, appName);
			function getTabURL(appFormId, args) {
				let hostRoot = window.location.href.replace('#', '');
				const hostRootIndex = hostRoot.toLowerCase().indexOf('/starthtml.lims');
				let formArgs = '';
				if (hostRootIndex >= 0) hostRoot = hostRoot.substring(0, hostRootIndex);
				if (args !== null && args) formArgs = '&formargs=' + encodeURIComponent(lims.ToJson(args));
				const isPortal = Starlims.Portal ? '&isPortal=true' : '';
				return hostRoot + '/starthtml.lims?FormId=' + appFormId + formArgs + isPortal;
			}
		};

		Shell.getOpenAsNewTabCall = function (record) {
			return `Shell.openConsoleItemAsNewTab({data: {origrec: ${record.data.origrec},applicationNavigateTo: \`${record.data.applicationNavigateTo}\`,caption: \`${record.data.caption}\`},parentNode: {data: {caption: \`${record.parentNode.data.caption}\`}}});event.stopPropagation(); return false;`;
		};

		Shell.openConsoleItemAsNewTab = async function (record) {
			const commandArgs = await this.getCommandParameters([record.data.origrec]);
			const applicationNavigateTo = record.data.applicationNavigateTo;
			const itemCaption = record.data.caption;
			const parentCaption = record.parentNode.data.caption;
			this.LaunchApp(
				applicationNavigateTo,
				commandArgs,
				'NEWTAB',
				applicationNavigateTo + ' ' + itemCaption,
				parentCaption,
				itemCaption,
			);
		};

		Shell.openConsoleItemUniqueTab = async function (record) {
			const commandArgs = await this.getCommandParameters([record.data.origrec]);
			const applicationNavigateTo = record.data.applicationNavigateTo;
			const itemCaption = record.data.caption;
			const parentCaption = record.parentNode.data.caption;
			this.LaunchApp(
				applicationNavigateTo,
				commandArgs,
				'UNIQUETAB',
				applicationNavigateTo + ' ' + itemCaption,
				parentCaption,
				itemCaption,
			);
		};
		// endregion
	});

	// region QVA 2022-07-28 PART 2/2 do not load console and reminders on NEWTAB mode opening as they are not visible and buttons to show them either
	Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
		Ext.override(STARLIMS.view.console.ConsoleController, {
			onConsoleTreeAfterRender: function (treePanel) {
				if (['NEWTAB', 'UNIQUETAB'].includes(window.webOpeningMode)) {
					Shell.navigateToForm(window.appFormId, window.appArgs);
					window.formFound = true;
				}
				this.callParent([treePanel]);
			},
		});

		Ext.override(STARLIMS.view.main.MainController, {
			onConsoleRecordsLoaded: function (view, records) {
				switch (view.getConsoleType()) {
					case 'C':
						this.getViewModel().set('consoleRecords', records);
						break;
					case 'R':
						this.getViewModel().set('reminderRecords', records);
						break;
				}
				if (window.formFound) return;
				var found = false;
				if (this.openInNewTab) {
					if (!this.isNavigating) {
						for (var i = 0; i < records.items.length; i++) {
							var children = records.items[i].data.children;
							for (var j = 0; j < children.length; j++) {
								var expression =
									this.redirectToForm != null &&
									children[j].applicationNavigateTo.toLowerCase() ==
										this.redirectToForm.toLowerCase();
								expression = this.cio
									? expression && children[j].origrec == parseInt(this.cio)
									: expression;
								if (expression) {
									found = true;
									var applicationNavigateTo = children[j].applicationNavigateTo,
										parentCaption = records.items[i].data.caption,
										itemCaption = children[j].caption,
										commandParameters = children[j].commandParameters,
										origrec = children[j].origrec;
									this.selectConsoleItem(
										applicationNavigateTo,
										parentCaption,
										itemCaption,
										commandParameters,
										origrec,
										view,
									);
									this.isNavigating = true;
									break;
								}
							}
							if (found) break;
						}
					}
					return;
				}
				if (this.redirectToForm != null) {
					if (view.getConsoleType() == 'C') {
						var bRemoteConsoleVisible = Starlims.Navigator.Variables.Get('HTML_REMOTE_CONSOLE_VISIBLE');
						var appTopBar = this.lookupReference('app-topbar');
						var showConsoleButton = appTopBar.lookupReference('showConsoleButton');
						if (!bRemoteConsoleVisible) {
							view.hide();
							showConsoleButton.hide();
						}
					} else {
						var bRemoteNotificationsVisible = Starlims.Navigator.Variables.Get(
							'HTML_REMOTE_NOTIFICATION_VISIBLE',
						);
						var appTopBar = this.lookupReference('app-topbar');
						var btnNotifications = appTopBar.lookupReference('btnNotifications');
						if (!bRemoteNotificationsVisible) {
							view.hide();
							btnNotifications.hide();
						}
					}
					if (!this.isNavigating) {
						var breadcrumbs = this.lookupReference('app-breadcrumbs');
						for (var i = 0; i < records.items.length; i++) {
							var children = records.items[i].data.children;
							for (var j = 0; j < children.length; j++)
								if (
									children[j].applicationNavigateTo.toLowerCase() == this.redirectToForm.toLowerCase()
								) {
									breadcrumbs.getViewModel().setData({
										catname: records.items[i].data.caption,
										appname: children[j].caption,
									});
									document.title = children[j].caption;
									found = true;
									break;
								}
							if (found) break;
						}
						if (found) {
							this.isNavigating = true;
							this.navigateToForm(this.redirectToForm, this.redirectToFormArgs, false, null);
							return;
						}
						if (view.getConsoleType() == 'C') return;
						if (
							lims.CallServer('RemoteAccessManager.ssAccessAllowed', [
								'APP_FRM',
								this.redirectToForm,
								'Access',
							])
						) {
							breadcrumbs.getViewModel().setData({
								catname: GetResource(null, 'remoteform'),
								appname: null,
							});
							document.title = GetResource(null, 'remoteform');
							this.isNavigating = true;
							this.navigateToForm(this.redirectToForm, this.redirectToFormArgs, false, null);
							return;
						}
						Dialogs.MessageBox(
							String.Format(
								GetResource(
									null,
									'You do not have the permissions to launch {0} remotely. Please contact your administrator.',
								),
								this.redirectToForm,
							),
							GetResource(null, 'Warning'),
							'OK',
							'WARNING',
						);
					}
				}
			},
		});
	});
	// endregion
	// endregion

// bouton dans cellule de datagrid - datagrid button in cell
let col = dgdOrders.FindColumn("DOSSIER");
col.renderer = (val, metadata, record) => {
	return `<btn style="color:#F5600F;text-decoration: underline;" onclick="Shell.LaunchApp('SampleLogin_General.MainLoginForm', ['LOGIN_ONE', '${record.data.FOLDERNO}'], 'MODAL')">
		Ouvrir dossier
</btn>`
}
