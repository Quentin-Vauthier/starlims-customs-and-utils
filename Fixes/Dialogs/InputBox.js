// region Change Dialogs.InputBox layout to be vertical instead of 'form' + 'ok' to 'OK' or value isn't returned
Ext.override(Dialogs, {
	statics: {
		_InputBox: function (defaultValue, prompt = 'Input data', caption = 'Input', data, callBack) {
			const inputOkHandler = function msgBox_Handler(buttonText, selectedText) {
				if (Ext.isFunction(callBack)) {
					const result = buttonText.toUpperCase() === 'OK' ? selectedText : null;
					callBack.call(null, buttonText, result);
				}
				Dialogs.__FocusCurrentControl();
			};
			if (!data) Ext.Msg.prompt(caption, prompt, inputOkHandler, null, false, defaultValue);
			else {
				const inputWindow = Ext.create('Ext.window.Window', {
					autoShow: true,
					width: 300,
					layout: {
						type: 'vbox',
						align: 'stretch',
					},
					title: caption,
					referenceHolder: true,
					defaultFocus: 'cmbfield',
					defaultButton: 'okButton',
					modal: true,
					items: [
						{
							xtype: 'label',
							text: prompt,
							height: 30,
							anchor: '100%',
						},
						{
							xtype: 'StarlimsComboBox',
							reference: 'cmbfield',
							fieldLabel: prompt,
							height: 30,
						},
					],
					buttons: [
						{
							reference: 'okButton',
							text: 'Ok',
						},
					],
				});
				const btn = inputWindow.lookupReference('okButton');
				btn.on(
					'click',
					function (theButton) {
						const buttonId = theButton.getText().toUpperCase();
						let selectedText = this.items.items[1] !== null ? this.items.items[1].SelectedText : '';
						if (selectedText === undefined) selectedText = '';
						inputOkHandler(buttonId, selectedText);
						this.close();
					},
					inputWindow,
				);
				const combo = inputWindow.items.items[1];
				const dataset = this.GetDataSet(data);
				if (dataset && dataset.Tables[0] && dataset.Tables[0].Columns[0]) {
					combo.DisplayMember = dataset.Tables[0].Columns[0].ColumnName;
				}
				combo.DataSet = dataset;
				if (defaultValue !== null && defaultValue !== undefined) {
					combo.setValue(defaultValue);
				}
				inputWindow.show();
			}
		},
	},
});
// endregion
