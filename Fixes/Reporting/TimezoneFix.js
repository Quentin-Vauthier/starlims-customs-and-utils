// REPLACE the existing function of the Client Script Reports.HTML_ShowReport with the following code:
// region Prepare Parameters get right timezone
function PrepareParameters(reportParams) {
	if (reportParams === null) reportParams = [];

	const sep = Utils.GetLocalNumberFormatInfo();
	reportParams.push(['LocalNumberDecimalSeparator', sep.DecimalSeparator]);
	reportParams.push(['LocalNumberGroupSeparator', sep.GroupSeparator]);

	const TimeZoneGMT = new Date().toLocaleDateString('en-US', { timeZoneName: 'short' }).split(', ')[1];
	const TimeZone = GetTimeZonebyGMT(TimeZoneGMT) ?? TimeZoneGMT;
	reportParams.push(['PrintClientTimeZone', `#${TimeZone}`]);

	return reportParams;
}

// For more timezeones : https://gitlab.com/Quentin-Vauthier/starlims-customs-and-utils/-/blob/main/Utilities/Timezones.xlsx
function GetTimeZonebyGMT(gmt) {
	const zones = new Map([
		['GMT', 'Greenwich Standard Time'],
		['GMT+1', 'Central Europe Standard Time'],
		['GMT+2', 'Kaliningrad Standard Time'],
		['GMT+3', 'Russian Standard Time'],
		['GMT+4', 'Saratov Standard Time'],
		['GMT+5', 'West Asia Standard Time'],
		['GMT+5:30', 'India Standard Time'],
		['GMT+6', 'Central Asia Standard Time'],
		['GMT+7', 'North Asia Standard Time'],
		['GMT+8', 'North Asia East Standard Time'],
		['GMT+9', 'Yakutsk Standard Time'],
		['GMT+10', 'West Pacific Standard Time'],
		['GMT+11', 'Central Pacific Standard Time'],
		['GMT+12', 'Kamchatka Standard Time'],
		['GMT+13', 'UTC+13'],
		['GMT+14', 'Line Islands Standard Time'],
		['GMT-1', 'Cape Verde Standard Time'],
		['GMT-2', 'Mid-Atlantic Standard Time'],
		['GMT-3', 'E. South America Standard Time'],
		['GMT-4', 'Pacific SA Standard Time'],
		['GMT-5', 'Eastern Standard Time'],
		['GMT-6', 'Central Standard Time'],
		['GMT-7', 'Mountain Standard Time'],
		['GMT-8', 'Pacific Standard Time'],
		['GMT-9', 'Alaskan Standard Time'],
		['GMT-11', 'UTC-11'],
	]);
	return zones.get(gmt);
}
// endregion
