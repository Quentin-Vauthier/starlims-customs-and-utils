// QVA/TBE 2023-04-11 remove empty row in datagrids when no Agregation
Ext.override(StarlimsDataGrid, {
	initComponent: function () {
		this.callParent(arguments);
		if (!this?.m_dto?.RootTable) return (this._getFeatureById('starlims-summary').showSummaryRow = false);
		this._getFeatureById('starlims-summary').showSummaryRow = this.m_dto.RootTable.Columns.some(c => c.AggregateFunction);
	},
});

// Specific code for frozen datagrids, behavior was different, the right table
// could have the summary row but not the left one, causing a misalignment
Ext.override(Ext.view.Table, {
	initComponent() {
		this.callParent(arguments);
		if (this?.ownerGrid?.FrozenColumns > 0) this.summaryFeature.showSummaryRow = false;
	},
});
