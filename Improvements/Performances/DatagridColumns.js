// region Methods used to improve performances to not redraw grid each time you call one of these methods, in a loop for example
	// region normal column
	Ext.override(StarlimsDataGridColumn, {	
		_SetCaptionNoRefresh(value) {
			Utils.checkArguments(arguments, ['String']);
			if (this.m_dto.Caption !== value) {
				this.m_dto.Caption = value;
				value = Utils.htmlEncode(value);
				if (this.setText) {
					this.setText(value);
				} else
					this.text = value;
			}
		},

		_SetWidthNoRefresh(value) {
			this.m_dto.Width = value;
			if (this.setWidth) {
				this.setWidth(value);
			} else
				this.width = value;
		},
		
		_SetVisibleNoRefresh(value) {
			Utils.checkArguments(arguments, ['Boolean']);
			if(this.Visible === value) return; // Already in the right state
			this.m_dto.Visible = value;
			if (this.rendered && this.setVisibleNoRefresh)
				this.setVisibleNoRefresh(value);
			else
				this.hidden = !value;
		},
		
		setVisibleNoRefresh: function(visible) {
			return this[visible ? 'show' : 'hide'](false);
		},
		
		hide: function(refresh = true) {
			const me = this,
				rootHeaderCt = me.getRootHeaderCt(),
				owner = me.getRefOwner();
			// During object construction, so just set the hidden flag and jump out
			if (owner.constructing) {
				me.callParent();
				return me;
			}
			if (me.rendered && !me.isVisible()) {
				// Already hidden
				return me;
			}
			// Save our last shown width so we can gain space when shown back into fully flexed HeaderContainer.
			// If we are, say, flex: 1 and all others are fixed width, then removing will do a layout which will
			// convert all widths to flexes which will mean this flex value is too small.
			if (rootHeaderCt.forceFit) {
				me.visibleSiblingCount = rootHeaderCt.getVisibleGridColumns().length - 1;
				if (me.flex) {
					me.savedWidth = me.getWidth();
					me.flex = null;
				}
			}
			rootHeaderCt.beginChildHide();
			Ext.suspendLayouts();
			// owner is a group, hide call didn't come from the owner
			if (owner.isGroupHeader) {
				// The owner only has one item that isn't hidden and it's me; hide the owner.
				if (me.isNestedGroupHeader()) {
					owner.hide();
				}
				if (me.isSubHeader && !me.isGroupHeader && owner.query('>gridcolumn:not([hidden])').length === 1) {
					owner.lastHiddenHeader = me;
				}
			}
			me.callParent();
			
			if(refresh) {
				// Notify owning HeaderContainer. Will trigger a layout and a view refresh.
				rootHeaderCt.endChildHide();
				rootHeaderCt.onHeaderHide(me);
			}
			
			Ext.resumeLayouts(true);
			return me;
		},
		
		MakeVisibleIfModeAllows: function(refresh = true) {
			if (this.Visible) {
				return;
			}
			const frm = this.ParentForm;
			if (frm === null) {
				return;
			}
			const modeAllow = frm.GetModePropertyValue(this, 'Visible');
			if (modeAllow === null || modeAllow === true) {
				if(refresh) this.Visible = true;
				else this.setVisibleNoRefresh(true);
			}
		}
	})
	//endregion
	// region checkboxcolumn
	Ext.override(StarlimsDataGridCheckBoxColumn, {
		_SetCaptionNoRefresh(value) {
			Utils.checkArguments(arguments, ['String']);
			if (this.m_dto.Caption !== value) {
				this.m_dto.Caption = value;
				value = Utils.htmlEncode(value);
				if (this.setText) {
					this.setText(value);
				} else
					this.text = value;
			}
		},

		_SetWidthNoRefresh(value) {
			this.m_dto.Width = value;
			if (this.setWidth) {
				this.setWidth(value);
			} else
				this.width = value;
		},
		
		_SetVisibleNoRefresh(value) {
			Utils.checkArguments(arguments, ['Boolean']);
			if(this.Visible === value) return; // Already in the right state
			this.m_dto.Visible = value;
			if (this.rendered && this.setVisibleNoRefresh)
				this.setVisibleNoRefresh(value);
			else
				this.hidden = !value;
		},
		
		setVisibleNoRefresh: function(visible) {
			return this[visible ? 'show' : 'hide'](false)
		},
		
		hide: function(refresh = true) {
			const me = this,
				rootHeaderCt = me.getRootHeaderCt(),
				owner = me.getRefOwner();
			// During object construction, so just set the hidden flag and jump out
			if (owner.constructing) {
				me.callParent();
				return me;
			}
			if (me.rendered && !me.isVisible()) {
				// Already hidden
				return me;
			}
			// Save our last shown width so we can gain space when shown back into fully flexed HeaderContainer.
			// If we are, say, flex: 1 and all others are fixed width, then removing will do a layout which will
			// convert all widths to flexes which will mean this flex value is too small.
			if (rootHeaderCt.forceFit) {
				me.visibleSiblingCount = rootHeaderCt.getVisibleGridColumns().length - 1;
				if (me.flex) {
					me.savedWidth = me.getWidth();
					me.flex = null;
				}
			}
			rootHeaderCt.beginChildHide();
			Ext.suspendLayouts();
			// owner is a group, hide call didn't come from the owner
			if (owner.isGroupHeader) {
				// The owner only has one item that isn't hidden and it's me; hide the owner.
				if (me.isNestedGroupHeader()) {
					owner.hide();
				}
				if (me.isSubHeader && !me.isGroupHeader && owner.query('>gridcolumn:not([hidden])').length === 1) {
					owner.lastHiddenHeader = me;
				}
			}
			me.callParent();
			
			if(refresh) {
				// Notify owning HeaderContainer. Will trigger a layout and a view refresh.
				rootHeaderCt.endChildHide();
				rootHeaderCt.onHeaderHide(me);
			}
			
			Ext.resumeLayouts(true);
			return me;
		},
		
		MakeVisibleIfModeAllows: function(refresh = true) {
			if (this.Visible) {
				return;
			}
			const frm = this.ParentForm;
			if (frm === null) {
				return;
			}
			const modeAllow = frm.GetModePropertyValue(this, 'Visible');
			if (modeAllow === null || modeAllow === true) {
				if(refresh) this.Visible = true;
				else this.setVisibleNoRefresh(true);
			}
		}
	})
	// endregion
// endregion