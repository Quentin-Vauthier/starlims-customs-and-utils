// region QVA Update 2023-02-17 Enlève le chargement inutile des consoles lors de l'ouverture en NEWTAB
Ext.Function.interceptAfter(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
	// region QVA 2022-07-28 PART 1/2 do not load console and reminders on NEWTAB mode opening as they are not visible and buttons to show them either
	const originalLaunchApp = Shell.LaunchApp;
	Shell.LaunchApp = async function (appFormId, args, mode, appName, parentCaption, menuCaption) {
		if (['NEWTAB', 'UNIQUETAB'].includes(mode)) {
			const newTab = await window.open(getTabURL(appFormId, args), '_blank');
			newTab.webOpeningMode = mode.toUpperCase();
			newTab.parentCaption = parentCaption;
			newTab.tabName = menuCaption ?? appName;
			newTab.name = String.IsNullOrEmpty(appName) ? appFormId : appName;
			newTab.expectedTitle = menuCaption;
			newTab.focus();
			return newTab;
		}
		return originalLaunchApp(appFormId, args, mode, appName);
		function getTabURL(appFormId, args) {
			let hostRoot = window.location.href.replace('#', '');
			const hostRootIndex = hostRoot.toLowerCase().indexOf('/starthtml.lims');
			let formArgs = '';
			if (hostRootIndex >= 0) hostRoot = hostRoot.substring(0, hostRootIndex);
			if (args !== null && args) formArgs = '&formargs=' + encodeURIComponent(lims.ToJson(args));
			const isPortal = Starlims.Portal ? '&isPortal=true' : '';
			return hostRoot + '/starthtml.lims?FormId=' + appFormId + formArgs + isPortal;
		}
	};
	
	Shell.getOpenAsNewTabCall = function (record) {
		return `Shell.openConsoleItemAsNewTab({data: {origrec: ${record.data.origrec},applicationNavigateTo: \`${record.data.applicationNavigateTo}\`,caption: \`${record.data.caption}\`},parentNode: {data: {caption: \`${record.parentNode.data.caption}\`}}});event.stopPropagation(); return false;`
	};
	
	Shell.openConsoleItemAsNewTab = async function (record) {
		const commandArgs = await this.getCommandParameters([record.data.origrec]);
		const applicationNavigateTo = record.data.applicationNavigateTo;
		const itemCaption = record.data.caption;
		const parentCaption = record.parentNode.data.caption;
		this.LaunchApp(applicationNavigateTo, commandArgs, 'NEWTAB', applicationNavigateTo + ' ' + itemCaption, parentCaption, itemCaption);
	};
	
	Shell.openConsoleItemUniqueTab = async function (record) {
		const commandArgs = await this.getCommandParameters([record.data.origrec]);
		const applicationNavigateTo = record.data.applicationNavigateTo;
		const itemCaption = record.data.caption;
		const parentCaption = record.parentNode.data.caption;
		this.LaunchApp(applicationNavigateTo, commandArgs, 'UNIQUETAB', applicationNavigateTo + ' ' + itemCaption, parentCaption, itemCaption);
	};
	// endregion
});

// region QVA 2022-07-28 PART 2/2 do not load console and reminders on NEWTAB mode opening as they are not visible and buttons to show them either
Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
	Ext.override(STARLIMS.view.console.ConsoleController, {
		onConsoleTreeAfterRender: function (treePanel) {
			if (['NEWTAB', 'UNIQUETAB'].includes(window.webOpeningMode)) {
				return this.fireViewEvent('onConsoleRecordsLoaded', treePanel.getStore().getData());
			}
			this.callParent([treePanel]);
		}
	});

	Ext.override(STARLIMS.view.main.MainController, {
		onConsoleRecordsLoaded: function (view, records) {
			this.callParent([view, records]);
			if (['NEWTAB', 'UNIQUETAB'].includes(window.webOpeningMode)) {
				const breadcrumbs = this.lookupReference('app-breadcrumbs');
				document.title = window.expectedTitle;
				breadcrumbs.getViewModel().setData({
					catname: window.parentCaption,
					appname: window.tabName
				});
			}
		}
	});
});
// endregion
// endregion