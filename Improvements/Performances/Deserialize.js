// region R&D Perfs Deserialize
if(Starlims.Version < "12.4.1") {

	Ext.override(DataSet, {
	
		Deserialize: function Deserialize(dsObject) {
	
			Utils.checkArguments(arguments, ['Object']);
			
			// Add fields to the model - prototype !
			// if the ORIGREC field is present between the columns in the columns array then THIS will be the ID-Field
			// otherwise we will create the id field as __starlimsIDField__
			const idFieldName = 'ORIGREC';
			let idField = { ColumnName: '__starlimsIDField__' };
		
			if(dsObject.Tables) {
				//console.log("Table Count=" + dsObject.Tables.Count);
		
				for(var t = 0; t < dsObject.Tables.length; t++){
					var currentTable = dsObject.Tables[t];
					
					const dataTable = new DataTable(currentTable.TableName);
					dataTable.suspendEvents(false);
					var columns = currentTable.Columns;
					if(columns) {
						//console.log("Columns Count=" + columns.Count);
						
						for(var i = 0; i < columns.Count; i++){
							var currentColumn = columns[i];
							const dataCol = new DataColumn(currentColumn);
		
							dataTable.Columns.Add(dataCol);
		
							if(currentColumn.ColumnName.toUpperCase() == idFieldName)
								idField = currentColumn;
						}   
		
						var rows = currentTable.Rows;
						if(rows) {
							//console.log("Rows Count=" + rows.Count);
							
							var columnNames = Array.prototype.map.call(dataTable.Columns.Items, x => x.ColumnName); //Pre-computing column names for performance
							var dataTableRows = dataTable.Rows; //Pre-computing property for performance
							
							for(var j = 0; j < rows.Count; j++){
								
								var currentRow = rows[j];
								var rowKeys = Object.keys(currentRow);
								for(var k = 0; k < rowKeys.length; k++){
									var key = rowKeys[k];
									
									if(columnNames.indexOf(key) == -1)
										throw new HtmlException("Column '" + key + "' does not belong to the table");
								}
								
								let dataRow = dataTable.NewRow();
								dataRow.set(currentRow);
								
								//Mark the record as not dirty after initial load
								dataRow.commit(true);
								
								dataTableRows.Add(dataRow);		
							}
						}
						
						var pks = [];
		
						if(currentTable.PrimaryKey) {
							currentTable.PrimaryKey.forEach(pkCol => {
								pks.push(dataTable.Columns[pkCol.ColumnName]);
							});
						}
		
						dataTable.PrimaryKey = pks;
		
						dataTable.resumeEvents(true);
						
						this.Tables.Add(dataTable);
					}
				}
			}
			
			if(dsObject.Relations) {
				dsObject.Relations.forEach(currentRelation => {
		
					var dsRelation = new DataRelation();
		
					dsRelation.RelationName = currentRelation.RelationName;
					dsRelation.ParentTable = currentRelation.ParentTable;
					dsRelation.ChildTable = currentRelation.ChildTable;
		
		
					currentRelation.ParentColumns.forEach(column => {
						dsRelation.ParentColumns.push({
							'ColumnName': column.ColumnName
						});
					});
		
		
					currentRelation.ChildColumns.forEach(column => {
						dsRelation.ChildColumns.push({
							'ColumnName': column.ColumnName
						})
					});
		
		
					this.Relations.Add(dsRelation);
				});
			}
			
			return this;
		}
	
	});
	
	//This is technically an addition
	Ext.override(DataColumn, {
	
		_isDefined: function _isDefined(value){
			return value != null && value != undefined;
		}
		
	});
	
	Ext.override(DataColumn, {
	
		_initDataColumn: function _initDataColumn(config){
			var me = this;
		
			me._defineProperties();
		
			let dataType = config.DataType,
				columnName = config.ColumnName;
		
			var _isDefined = this._isDefined;
			
			let extjsType = DataColumn._getExtDataType(dataType),
				defaultValByType = DataColumn._getDefaultValueByDataType(extjsType),
				defaultVal = _isDefined(config.DefaultValue) ? config.DefaultValue : null;
		
			//Initialize 
			me.m_dataType = dataType || "string"; //this is .NET data type for compatibility with xfd
			me.m_defaultValue = defaultVal;
		
			me.m_MaxLength = _isDefined(config.MaxLength) ? config.MaxLength : -1;
			me.m_AllowDBNull = _isDefined(config.AllowDBNull) ? config.AllowDBNull : true;
			me.m_DateTimeMode = _isDefined(config.DateTimeMode) ? config.DateTimeMode : 'Local';
			me.m_ReadOnly = _isDefined(config.ReadOnly) ? config.ReadOnly : false;
			me.m_Unique = _isDefined(config.Unique) ? config.Unique : false;
			me.m_DecimalScale = _isDefined(config.DecimalScale) ? config.DecimalScale : -1;
			me.m_DecimalPrecision = _isDefined(config.DecimalPrecision) ? config.DecimalPrecision : -1;
			me._setExtendedProperties(config.ExtendedProperties);
		
			let validators = [];
			if(!me.m_AllowDBNull)
				validators.push(Ext.create('Ext.data.validator.NotNull'));
			
			if(extjsType === 'string' && me.m_MaxLength >= 0)
				validators.push(Ext.create('Ext.data.validator.Length', { min: 0, max: me.m_MaxLength }));
		
			me.m_extjsField = Ext.data.field.Field.create({
				name: columnName || "Column-" + Ext.id(),
				type: extjsType,
				defaultValue: defaultVal,
				allowNull: true, //this is not the same as .NET AllowDBNull, see validators above
				unique: me.m_Unique,
				validators: validators
			});
			
			me.m_extjsField.DataColumn = me;
		}
		
	});
	
	
	Ext.override(DataColumnCollection, {
		Contains: function Contains(name){
			Utils.checkArguments(arguments, ['String']);
	
			return this.IndexOfString(name) >= 0;
		},
		
		IndexOf: function IndexOf(column) {
			if(!column)
				return -1;
		
			if(column instanceof DataColumn)
				return (column.Table)?this.Items.indexOf(column):-1;
			else if(typeof column === "string")
				return this.IndexOfString(column);
				
			return -1;
		},
		
		//This is technically an addition
		IndexOfString: function IndexOfString(columnName) {
			if(!columnName.length)
				return -1;
		
			var upperCaseColumnName = columnName.toUpperCase();
			
			var items = this.Items; //Pre-resolving property for performance
			var itemsCount = items.length;
			for(var i = 0; i < itemsCount; i++)
				if(items[i].m_extjsField.name.toUpperCase() === upperCaseColumnName) //Accessing field for performance
					return i;
		
			return -1;
		}
	});
	
	
		
	
	Ext.override(DataRow, {
	
		_GetItemArray: function _GetItemArray() {
			var data = this.getData();
			
			//Just delete those fields, don't take time to check if they're present as it's costy
			delete data["id"];
			delete data["__starlimsIDField__"];
			
			return Object.values(data);
		}
		
	});
	
	
	// Good
	Ext.override(DataRowCollection, {
	
		Add: function Add() {
		
			var me = this;
			var dataRow = null;
	
			if(arguments.length > 0) {
				if(arguments[0] instanceof DataRow && arguments.length == 1) {
					dataRow = arguments[0];
				}
				else {
					var fields = this.Store.getModel().getFields();
					if(fields.length > 0) {
	
						let params = Array.isArray(arguments[0]) ? arguments[0] : arguments;
	
						dataRow = this.Table.NewRow();
						for(var i = 0; i < fields.length; i++) {
							var field = fields[i];
							dataRow[field.name] = params[i];
						}
					}
				}
			}
			else {
				throw new HtmlException("Invalid Argument");
			}
	
			if(dataRow) {
				//var t0 = performance.now();
				
				this._isValidRowData(dataRow);
	
				//t1 = performance.now(); console.log(`_isValidRowData: ${t1 - t0}`); t0 = performance.now();		
	
				//Define property accessor
				var rowPosition = this.Count;
	
				//Set Table of the DataRow
				dataRow.Table = this.Table;
	
				//Add data row to the collection
				this.Store.add(dataRow);
	
				//t1 = performance.now(); console.log(`Store Add Row: ${t1 - t0}`); t0 = performance.now();		
				
				dataRow.IsRowBelongsToTable = true;
				Object.defineProperty(me, rowPosition, {
					get: function () {
						return this._getByIndex(this.Store, rowPosition);
					},
					enumerable: true,
					configurable: true
				});
	
				//t1 = performance.now(); console.log(`defineProperty: ${t1 - t0}`); t0 = performance.now();	
	
				if(this.OnChanged instanceof Function)
					this.OnChanged();
	
				//t1 = performance.now(); console.log(`OnChanged: ${t1 - t0}`); t0 = performance.now();	
			}
		},
		
		_isValidRowData: function _isValidRowData(dataRow) {
			let dataColumn, dataColumnName;
			
			let pkCollection = new Object();
			
			//var data = dataRow.ItemArray; //Pre-computing property
			var data = dataRow.data; //Pre-computing property (accessing internal field)
			//var itemCount = data.length; //Pre-computing property (moved below)
			
			var columns = this.Table.Columns.Items;  //Pre-computing property
			var itemCount = columns.length; //Pre-computing property
			//var validateAllowDBNull = dataRow.ValidateAllowDBNull; //Pre-computing property
			
			for(var i = 0; i < itemCount; i++) {
				dataColumn = columns[i];
				dataColumnName = dataColumn.m_extjsField.name;//Using field for performance
				
				if(dataColumn.m_IsPrimaryKey)//Using field for performance
					pkCollection[dataColumnName] = dataRow[i];
			
				ValidateAllowDBNull(dataColumn,	data[dataColumnName]);
			}
		
			//Validate PK
			dataRow.ValidatePrimaryKey(pkCollection);
		
			//the function doesn't use 'this' so it doesn't make sense to be at the class level
			function ValidateAllowDBNull(dataColumn, value) {
				if(dataColumn && !dataColumn.AllowDBNull &&
					(value === null || value === undefined)) {
					var dType = dataColumn.DataType.Name ? dataColumn.DataType.Name : dataColumn.DataType;
					throw new HtmlException("Cannot set default value 'null' for Column '" + dataColumn.ColumnName + "' having data type '" + dType + "'");
				}
			}
		}
		
	});
	
	
	// Good
	Ext.override(DataTable, {
		
		NewRow: function NewRow() {
			var me = this;
			
			let handler = {
				get: function (target, name) {
					if(typeof name === "symbol") return;
		
					if(name in target) {
						return target[name];
					} else {
						var colName = name;
						if(me._stringIsValidIndex(colName, target.Table.Columns)) { //return by index
							colName = target.Table.Columns.Items[parseInt(colName)].ColumnName;
						}
		
						var data = target.getData();
						if(colName in data) {
							return data[colName];
						} else {
							//This is to ignore case.
							for(var prop in data) {
								if(prop.toUpperCase() === colName.toUpperCase()) { return data[prop]; }
							}
						}
						return null;
					}
				},
				set: function (obj, prop, value) {
					if(prop in obj) {
						obj[prop] = value;
						return true;
					}
		
					var colName = prop;
					if(me._stringIsValidIndex(colName, obj.Table.Columns)) { //return by index
						colName = obj.Table.Columns.Items[parseInt(colName)].ColumnName;
					}
		
					const columnIndex = obj.Table.Columns.IndexOf(colName);
					if(columnIndex >= 0) {
						// get the proper name of the column (in case it was sent with a different casing)
						colName = obj.Table.Columns.Items[columnIndex].ColumnName;
		
						//need to do "obj.m_RowProxy._SetByName" instead of "obj._SetByName"
						//otherwise the chmage event is not triggered and the grid/control is not updated
						obj.m_RowProxy._SetByName(colName, value);
		
					} else {
						obj[prop] = value;
					}
					return true;
				}
			};
		
			var model = this.getModel().create();
			
			var dr = new DataRow(model, this);
			
			var dataRowProxy = new Proxy(dr, handler);
			dr.m_RowProxy = dataRowProxy;
			
			this._assignAutoIncrementValues(dataRowProxy);
		
			return dataRowProxy;
		},
	
		_stringIsValidIndex: function _stringIsValidIndex(stringIdx, targetCollection){
			let idx = parseInt(stringIdx);
			return !isNaN(idx)
				&& (idx >= 0 && idx < targetCollection.Count)
				&& String(idx) === stringIdx;
		},
		
		_assignAutoIncrementValues: function _assignAutoIncrementValues(row){
			var columns = row.Table.Columns.Items;
			for(var i = 0; i < columns.length; i++) {
				var col = columns[i];
		
				if(!col.AutoIncrement)
					continue;
		
				row[i] = col._getNextAutoIncrementValue();//Access getter directly for performance
			}
		}
		
	});
	
	
	//Not crucial. To be added to source manually
	//	Ext.override(Utils.prototype.IsDataSet = function IsDataSet(value) {
	//		console.log("IsDataSet");
	//		if(typeof value === "string")
	//				return value.trim().slice(0, 1) === "{";
	//	
	//		return false;
	//	});
}
// endregion
