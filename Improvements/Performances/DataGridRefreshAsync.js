// region QVA - DataGrid RefreshAsync
Ext.override(StarlimsDataGrid, {
	/**
	 * Refreshes the data of the datagrid asynchronously
	 * @param {*} options
	 * @param {boolean} options.keepScrollX
	 * @param {boolean} options.keepScrollY
	 * @param {string[]} options.keepSelectionFields
	 * @param {string} options.maskText
	 * @param {boolean} options.keepGroups
	 * @param {boolean} options.keepGroupsState
	 * @param {boolean} options.keepFilters
	 * @param {boolean} options.keepSorters
	 * @param {boolean} options.triggerRowChange
	 * @returns
	 */
	RefreshAsync: async function ({
		keepSelectionFields = [],
		maskText = 'Refreshing data...',
		keepGroups = true,
		keepGroupsState = true,
		keepFilters = true,
		keepSorters = true,
		keepScrollX = false,
		keepScrollY = false,
		triggerRowChange = true,
	} = {}) {
		if (!this.Data) return;
		try {
			if (this.rendered) this.mask(maskText);
			// Save current config
			let grouper, sorters, filters, expandedGroups, scrollX, scrollY, currentSelectionData, currentRowData;
			if (keepGroups) grouper = this.store?.getGrouper();
			if (keepSorters) sorters = this.store?.getSorters()?.items;
			if (keepFilters) filters = this.store?.getFilters()?.items;
			if (keepFilters && filters) filters = [...filters]; //copy
			if (keepGroupsState)
				expandedGroups = this.store
					.getGroups()
					?.items?.map(i => i.config.groupKey)
					?.map(gk => [gk, this._getGroupByFeature()?.getMetaGroup(gk)?.isCollapsed])
					.filter(([_, collapsed]) => !collapsed)
					.map(([gk, _]) => gk);
			if (keepScrollX) scrollX = this.getScrollX();
			if (keepScrollY) scrollY = this.getScrollY();
			const currentRecord = this.m_currentRecord ?? this.getSelection()?.[0] ?? null;
			if (keepSelectionFields?.length && currentRecord) {
				currentRowData = keepSelectionFields.map(f => currentRecord.data[f]);
				const currentSelection = this.getSelection();
				if (currentSelection?.length)
					currentSelectionData = currentSelection.map(r => keepSelectionFields.map(f => r.data[f]));
				this.suspendEvent('selectionchange');
			} else if (!triggerRowChange) this.suspendEvent('selectionchange');

			const data = await Utils.GetTextAsync(this.Data);
			// Replace current config
			this.DataSet = new DataSet(JSON.parse(data));

			// Restore config
			if (!this.store)
				if (this.rendered) return this.unmask();
				else return;

			if (keepGroups && grouper) {
				this.suspendEvents();
				this.store.group(grouper);
				this.resumeEvents();
				if (keepGroupsState) {
					this.CollapseRows();
					const gbf = this._getGroupByFeature();
					expandedGroups?.forEach(g => {
						try {
							gbf.doCollapseExpand(false, g);
						} catch (ign) {
							/*ignore, group doesn't exist anymore*/
						}
					});
				}
			}
			if (keepSorters && sorters?.Length) this.store.setSorters(sorters);
			if (keepFilters && filters?.Length) this.store.setFilters(filters);
			if (keepSelectionFields?.length) {
				const selection = currentSelectionData?.length
					? currentSelectionData
							.map(data =>
								this.store.data.items.find(r => keepSelectionFields.every((f, i) => r[f] === data[i])),
							)
							.filter(r => r)
					: [];
				if (triggerRowChange) this.resumeEvent('selectionchange');
				if (selection.length) {
					this.setSelection(selection);
					this.m_currentRecord =
						this.store.data.items.find(r =>
							keepSelectionFields.every((f, i) => r[f] === currentRowData[i]),
						) ?? selection[0];
					this.ensureVisible(selection[0], { focus: false });
				}
				if (!triggerRowChange) this.resumeEvent('selectionchange');
			} else if (!triggerRowChange) this.resumeEvent('selectionchange');
			if (keepScrollX || keepScrollY) {
				if (keepScrollX === false) scrollX = this.getScrollX();
				if (keepScrollY === false) scrollY = this.getScrollY();
				this.scrollTo(scrollX, scrollY);
			}
			if (this.rendered) this.unmask();
		} catch (e) {
			if (this.rendered) this.unmask();
			console.error(e);
			// Display an error popup, uncomment the following line to enable it
			// Dialogs.MessageBox('Unable to refresh datagrid data', 'Error', 'OK', 'ERROR');
		}
	},
});
// endregion
