//region glboal datasetasync functions
const globalSetDataSetAsync = function (map) {
	return async function (dsName, parameters) {
		// Store the parameters to prevent multiple calls ambiguity
		map.set(this, parameters);
		// Mask the control, if it throws an error, log it and continue as it should not stop the process
		try {
			this.mask('Loading...');
		} catch (err) {
			console.warn('Error masking element', err);
		}
		const thisObj = this;
		// Convert the parameters to string to mimic the behavior of lims.GetDataSource
		await lims
			.GetDataSetAsync(
				dsName,
				parameters?.map(p => p?.toString()),
			)
			.then(ds => {
				if (thisObj.isDestroyed) return; // if the grid is destroyed before the async call is finished
				if (map.get(thisObj) !== parameters) return; // Prevent multiple calls ambiguity
				// Assign Data
				thisObj.DataSet = ds;
				thisObj.m_dto.Data = lims.GetDataSource(dsName, parameters);
				// Same as mask above
				try {
					thisObj.Enabled === false ? thisObj.mask() : thisObj.unmask();
				} catch (err) {
					console.warn('Error unmasking element', err);
				}
			});
	};
};
const globalSetDataAsync = function (map) {
	return async function (dsName, parameters) {
		map.set(this, parameters);
		try {
			this.mask('Loading...');
		} catch (err) {
			console.warn('Error masking element', err);
		}
		const thisObj = this;
		await lims.GetDataAsync(dsName, parameters).then(data => {
			if (thisObj.isDestroyed) return; // if the grid is destroyed before the async call is finished
			if (map.get(thisObj) !== parameters) return; // Prevent multiple calls ambiguity
			thisObj.Data = data;
			try {
				thisObj.Enabled === false ? thisObj.mask() : thisObj.unmask();
			} catch (err) {
				console.warn('Error unmasking element', err);
			}
		});
	};
};
//endregion

// region allows changing datagrid data asynchrounously easily
const dsLastSetDataSetAsyncArgs = new Map();
StarlimsDataGrid.prototype.SetDataSetAsync = globalSetDataSetAsync(dsLastSetDataSetAsyncArgs);
StarlimsHDataGrid.prototype.SetDataSetAsync = StarlimsDataGrid.prototype.SetDataSetAsync;

const dsLastSetDataAsyncArgs = new Map();
StarlimsDataGrid.prototype.SetDataAsync = globalSetDataAsync(dsLastSetDataAsyncArgs);
StarlimsHDataGrid.prototype.SetDataAsync = StarlimsDataGrid.prototype.SetDataSetAsync;
// endregion

// region allows changing datagrid column data asynchrounously easily
const dsColLastSetDataSetAsyncArgs = new Map();
StarlimsDataGridColumn.prototype.SetDataSetAsync = async function SetDataSetAsync(
	dsName,
	parameters,
	maskGrid = false,
) {
	dsColLastSetDataSetAsyncArgs.set(this, parameters);
	// Mask the entire grid if needed
	if (maskGrid) {
		try {
			this.Parent.mask('Loading...');
		} catch (err) {
			console.warn('Error masking element', err);
		}
	}
	// Disable the column while loading
	this.disable();
	// Create stylesheet if not already created
	if (!window.dgColStyleSheet) {
		window.dgColStyleSheet = document.createElement('style');
		document.head.appendChild(window.dgColStyleSheet);
	}
	// Add loading style to the column
	window.dgColStyleSheet.sheet.insertRule(
		`.x-grid-cell-${this.DataMember} { background-color: rgba(209, 209, 209, 0.5)}`,
		0,
	);
	const rule = window.dgColStyleSheet.sheet.cssRules[0];

	const thisCol = this;
	await lims
		.GetDataSetAsync(
			dsName,
			parameters?.map(p => p?.toString()),
		)
		.then(ds => {
			// if the column is destroyed before the async call is finished
			// Prevent multiple calls ambiguity
			if (thisCol.isDestroyed || dsColLastSetDataSetAsyncArgs.get(thisCol) !== parameters) {
				// Remove loading style from stylesheet
				const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
				window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
				return;
			}
			thisCol.DataSet = ds;
			thisCol.m_dto.Data = lims.GetDataSource(dsName, parameters);
			// Remove loading style
			const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
			window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
			if (maskGrid)
				try {
					this.Parent.Enabled === false ? this.Parent.mask() : this.Parent.unmask();
				} catch (err) {
					console.warn('Error unmasking element', err);
				}
			thisCol.enable();
		});
};
StarlimsDataGridCheckBoxColumn.prototype.SetDataSetAsync = StarlimsDataGridColumn.prototype.SetDataSetAsync;
const dsColLastSetDataAsyncArgs = new Map();
StarlimsDataGridColumn.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters, maskGrid = false) {
	dsColLastSetDataAsyncArgs.set(this, parameters);
	if (maskGrid)
		try {
			this.Parent.mask('Loading...');
		} catch (err) {
			console.warn('Error masking element', err);
		}
	this.disable();

	if (!window.dgColStyleSheet) {
		window.dgColStyleSheet = document.createElement('style');
		document.head.appendChild(window.dgColStyleSheet);
	}
	window.dgColStyleSheet.sheet.insertRule(
		`.x-grid-cell-${this.DataMember} { background-color: rgba(209, 209, 209, 0.5)}`,
		0,
	);
	const rule = window.dgColStyleSheet.sheet.cssRules[0];

	const thisCol = this;
	await lims.GetDataAsync(dsName, parameters).then(data => {
		if (thisCol.isDestroyed || dsColLastSetDataSetAsyncArgs.get(thisCol) !== parameters) {
			const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
			window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
			return;
		}
		thisCol.Data = data;
		const ruleIndex = Array.from(window.dgColStyleSheet.sheet.cssRules).indexOf(rule);
		window.dgColStyleSheet.sheet.deleteRule(ruleIndex);
		if (maskGrid)
			try {
				this.Parent.Enabled === false ? this.Parent.mask() : this.Parent.unmask();
			} catch (err) {
				console.warn('Error unmasking element', err);
			}
		thisCol.enable();
	});
};
StarlimsDataGridCheckBoxColumn.prototype.SetDataAsync = StarlimsDataGridColumn.prototype.SetDataAsync;
// endregion

// region allow changing combobox data asynchrounously easily
const cbLastSetDataSetAsyncArgs = new Map();
StarlimsComboBox.prototype.SetDataSetAsync = async function SetDataSetAsync(
	dsName,
	parameters,
	selectFirstItem = false,
) {
	cbLastSetDataSetAsyncArgs.set(this, parameters);
	this.disable();
	this.suspendEvents();
	this.getStore().insert(0, [['Loading data...', null]]);
	this.select('Loading data...');
	const thisCombo = this;
	await lims
		.GetDataSetAsync(
			dsName,
			parameters?.map(p => p?.toString()),
		)
		.then(ds => {
			if (thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
			if (cbLastSetDataSetAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
			thisCombo.getStore().removeAt(0);
			thisCombo.select(null);
			thisCombo.resumeEvents();
			thisCombo.DataSet = ds;
			thisCombo.m_dto.Data = lims.GetDataSource(dsName, parameters);
			if (thisCombo.Items.length) thisCombo.Items[0].Selected = selectFirstItem;
			thisCombo.Enabled && thisCombo.enable();
		});
};

const cbLastSetDataAsyncArgs = new Map();
StarlimsComboBox.prototype.SetDataAsync = async function SetDataAsync(dsName, parameters, selectFirstItem = false) {
	cbLastSetDataAsyncArgs.set(this, parameters);
	this.disable();
	this.suspendEvents();
	// Insert Loading item to show loading state
	this.getStore().insert(0, [['Loading data...', null]]);
	this.select('Loading data...');
	const thisCombo = this;
	await lims.GetDataAsync(dsName, parameters).then(data => {
		if (thisCombo.isDestroyed) return; // if the combobox is destroyed before the async call is finished
		if (cbLastSetDataAsyncArgs.get(thisCombo) !== parameters) return; // Prevent multiple calls ambiguity
		// remove loading data item
		thisCombo.getStore().removeAt(0);
		thisCombo.select(null);
		thisCombo.resumeEvents();
		thisCombo.Data = data;
		if (thisCombo.Items.length) thisCombo.Items[0].Selected = selectFirstItem;
		thisCombo.Enabled && thisCombo.enable();
	});
};
// endregion

// region allows changing panel data asynchrounously easily
const pnlLastSetDataSetAsyncArgs = new Map();
StarlimsPanel.prototype.SetDataSetAsync = globalSetDataSetAsync(pnlLastSetDataSetAsyncArgs);

const pnlLastSetDataAsyncArgs = new Map();
StarlimsPanel.prototype.SetDataAsync = globalSetDataAsync(pnlLastSetDataAsyncArgs);
// endregion

// region allows changing groupbox data asynchrounously easily
const gbLastSetDataSetAsyncArgs = new Map();
StarlimsGroupBox.prototype.SetDataSetAsync = globalSetDataSetAsync(gbLastSetDataSetAsyncArgs);

const gbLastSetDataAsyncArgs = new Map();
StarlimsGroupBox.prototype.SetDataAsync = globalSetDataAsync(gbLastSetDataAsyncArgs);
// endregion

// region TabPage setDataAsync
const tpLastSetDataSetAsyncArgs = new Map();
StarlimsTabPage.prototype.SetDataSetAsync = globalSetDataSetAsync(tpLastSetDataSetAsyncArgs);

const tpLastSetDataAsyncArgs = new Map();
StarlimsTabPage.prototype.SetDataAsync = globalSetDataAsync(tpLastSetDataAsyncArgs);
// endregion

// region Multichoice SetDataSetAsync
const mcLastSetDataSetAsyncArgs = new Map();
StarlimsMultiChoice.prototype.SetDataSetAsync = globalSetDataAsync(mcLastSetDataSetAsyncArgs);

const mcLastSetDataAsyncArgs = new Map();
StarlimsMultiChoice.prototype.SetDataAsync = globalSetDataAsync(mcLastSetDataAsyncArgs);
// endregion

// region ListBox SetDataSetAsync
const lbLastSetDataSetAsyncArgs = new Map();
StarlimsListBox.prototype.SetDataSetAsync = globalSetDataAsync(lbLastSetDataSetAsyncArgs);

const lbLastSetDataAsyncArgs = new Map();
StarlimsListBox.prototype.SetDataAsync = globalSetDataAsync(lbLastSetDataAsyncArgs);
// endregion

// region CheckedListBox SetDataSetAsync
const clbLastSetDataSetAsyncArgs = new Map();
StarlimsCheckedListBox.prototype.SetDataSetAsync = globalSetDataAsync(clbLastSetDataSetAsyncArgs);

const clbLastSetDataAsyncArgs = new Map();
StarlimsCheckedListBox.prototype.SetDataAsync = globalSetDataAsync(clbLastSetDataAsyncArgs);
// endregion
