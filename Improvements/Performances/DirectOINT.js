//! This requires the `DirectOINT` package to be installed in the designer
// region QVA 2024-04-05 - Load forms directly from URL and do not wait for the console to load when possible
Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
	Ext.override(STARLIMS.view.console.ConsoleController, {
		onConsoleTreeAfterRender: function (treePanel) {
			if (window.lookingForForm) return this.callParent([treePanel]);
			window.lookingForForm = true;
			const searchParams = new URLSearchParams(window.location.search);
			const FormId = searchParams.get('FormId');
			const formArgs = searchParams.get('formargs');
			const cio = searchParams.get('cio');
			if (FormId && formArgs && !window.formFound) {
				//! This bypasses remote form access verification (originally made clientside)
				//! If you want to keep the remote form access verification, comment the two following lines (and lose the performance gain)
				window.formFound = true;
				Shell.navigateToForm(FormId, JSON.parse(formArgs));
			} else if (FormId && cio) {
				lims.CallServerAsync('Console.GetItemDetails', [parseInt(cio)], res => {
					if (window.formFound || Shell.isNavigating || Shell.getCurrentForm() !== null) return;
					if (res === 'Unauthorized' || res.length < 3) return;
					const parentCaption = res[0];
					const itemCaption = res[1];
					const args = res[2];
					window.formFound = true;
					Shell.navigateToForm(FormId, args);
					Shell.lookupReference('app-breadcrumbs').getViewModel().setData({
						catname: parentCaption,
						appname: itemCaption,
					});
				});
			}
			this.callParent([treePanel]);
		},
	});

	Ext.override(STARLIMS.view.main.MainController, {
		onConsoleRecordsLoaded: function (view, records) {
			if (window.formFound) {
				switch (view.getConsoleType()) {
					case 'C':
						this.getViewModel().set('consoleRecords', records);
						break;
					case 'R':
						this.getViewModel().set('reminderRecords', records);
						break;
				}
				return;
			}
			this.callParent([view, records]);
		},
	});
});
// endregion
