// region AsyncDataLoader

Ext.override(StarlimsDataLoader, {
	/**
	 * Sets the loading state of a component, can be called before the component is rendered,
	 * in that case the loading state will be set on the afterrender event.
	 * @param {Ext.Component} component The component to set the loading state
	 * @param {*} isLoading The loading state
	 */
	SetLoadingState: function (component, isLoading) {
		if (!component.rendered) {
			if (isLoading) {
				// Set the function to be called after the component is rendered
				// Saves the function in the component to be able to remove it later
				const dl = this;
				const setLoadingAfterRender = () => dl.SetLoadingState(component, true);
				component.on('afterrender', setLoadingAfterRender);
				component.m_dataLoading = isLoading;
				component.m_dataLoadingFn = setLoadingAfterRender;
			} else {
				// If the component is not rendered when the loading state is set to false remove the listener
				// Used when the data is loaded before the component is rendered
				component.removeListener('afterrender', component.m_dataLoadingFn);
			}
			return;
		}

		// Defines all the actions to be taken depending on the component type and the loading state
		switch (true) {
			// Form, Datagrid, Panel, GroupBox behavior
			case component instanceof StarlimsForm:
			case component instanceof StarlimsDataGrid:
			case component instanceof StarlimsHDataGrid:
			case component instanceof StarlimsPanel:
			case component instanceof StarlimsGroupBox:
			case component instanceof StarlimsTreeListView:
			case component instanceof StarlimsListView:
			case component instanceof StarlimsLinkBar:
			case component instanceof StarlimsTreeView:
			case component instanceof StarlimsMultiChoice:
			case component instanceof StarlimsCheckedListBox:
				isLoading ? component.mask('Loading data...') : component.unmask();
				break;
			// StarlimsCombobox behavior
			case component instanceof StarlimsComboBox && isLoading: {
				// Disables the combobox, saves the initial enabled state and sets it to false so that
				// the combobox isn't enabled when the form is loaded
				component.disable();
				component.m_dto.initialEnabled = component.m_dto.Enabled;
				component.m_dto.Enabled = false;
				// Adds a loading item to the combobox and selects it
				const onSelectionChanged = component.OnSelectionChanged;
				component.OnSelectionChanged = null;
				component.getStore().insert(0, [['Loading data...', null]]);
				component.select('Loading data...');
				component.OnSelectionChanged = onSelectionChanged;
				break;
			}
			case component instanceof StarlimsComboBox:
				// Removes the loading item from the combobox and select blank otherwise the loading
				// item is still selected (only on APPEND behavior or we would remove some data)
				if (component.m_dto.DataBindingBehavior === 'APPEND') component.getStore().removeAt(0);
				// Do not select blank if a data has been selected when assigning the data
				// When we remove the store item, the selectedValue will be undefined if no data has been selected
				if (component.SelectedValue === undefined) component.select(null);
				// Enables the combobox if it was enabled in the designer and sets back the initial
				// enabled state assigned in the designer
				if (component.m_dto.initialEnabled) component.enable();
				component.m_dto.Enabled = component.m_dto.initialEnabled;
				break;

			case component instanceof StarlimsDataGridColumn && isLoading:
			case component instanceof StarlimsDataGridCheckBoxColumn && isLoading: {
				// Disables the column
				component.disable();
				// Not really useful because it will load as the same time as the datagrid where no data
				// is currently loaded but we never know (maybe when streaming data will be implemented)
				const styleSheet = document.dataLoaderStylesheet;
				if (!styleSheet) {
					const style = document.createElement('style');
					document.head.appendChild(style);
					document.dataLoaderStylesheet = style.sheet;
				}
				document.dataLoaderStylesheet.insertRule(
					`.x-grid-cell-${component.DataMember} {
						background-color: rgba(209, 209, 209, 0.5)
					}`,
					0,
				);
				break;
			}
			case component instanceof StarlimsDataGridColumn:
			case component instanceof StarlimsDataGridCheckBoxColumn:
				// Reenables the column
				component.enable();
				// Remove the style rule added in the loading state
				document.dataLoaderStylesheet.deleteRule(0);
				break;

			// default
			case isLoading:
				component.mask('Loading data...');
				break;
			default:
				component.unmask();
				break;
		}
	},

	Process: async function () {
		if (this.m_controls.length <= 0) return;
		const lims = new StarlimsAppFunctions(this);

		// Process DatagridColumns differently as they can be sent in background to increase performances
		const dataGridColumns = this.m_controls.filter(
			c => c instanceof StarlimsDataGridColumn || c instanceof StarlimsDataGridCheckBoxColumn,
		);
		const otherControls = this.m_controls.filter(c => !dataGridColumns.includes(c));

		const dgColsUrls = this.m_urls.filter((url, i) => dataGridColumns.includes(this.m_controls[i]));
		const otherControlsUrls = this.m_urls.filter((url, i) => otherControls.includes(this.m_controls[i]));

		for (let i = 0; i < dataGridColumns.length; i++) {
			const component = dataGridColumns[i];
			const url = dgColsUrls[i];
			// Set the loading state of the column
			this.SetLoadingState(component, true);
			// Call a serverscript to get all the data (Same as default behavior) and assign the data to the column
			Utils.GetTextAsync(url).then(data => {
				if (component.destroyed) return;
				component.AssignData(url, data);
				component.fireEvent('OnDataLoaded', component, data);
				// Set the loading state of the column
				this.SetLoadingState(component, false);
			});
		}

		// Set all the controls to a loading state
		for (const component of otherControls) this.SetLoadingState(component, true);
		// Call a serverscript to get all the data (Same as default behavior) and assign the data to the controls
		// eslint-disable-next-line @quintaaa/starlims/check-server-functions
		const data = await lims.CallServerAsync('$ws.RunRESTActions', [otherControlsUrls]);
		for (let i = 0; i < otherControls.length; i++) {
			if (otherControls[i].destroyed) continue;
			otherControls[i].AssignData(otherControlsUrls[i], data[i]);
		}
		// Set all the controls to a non-loading state
		for (const component of otherControls) {
			if (component.destroyed) continue;
			this.SetLoadingState(component, false);
		}
	},
});

Ext.override(StarlimsForm, {
	// Function used by the form to load the data of components from the designer assigned Data
	_LoadData: async function () {
		if (!this.DataLoader || !this.DataLoader.m_controls.length) return;
		// Create form properties to know that the form is loading data
		this.m_dataLoading = true;
		this.m_dataLoaded = false;
		await this.DataLoader.Process();
		// Call the OnLoad and OnShow events, usually called in the render function
		// but since we are now asynchronously loading the data we need to call them here
		// and make sure we do not call them from the render functions otherwise they will be called twice
		// or before the data is loaded which can create some bugs if some controls are manipulated in the events
		if (this.rendered && this.hasListener('OnLoad')) {
			this.m_firedOnLoad = true;
			this.fireEvent('OnLoad', this, null);
		}
		this.DataLoader = null;
		// Update the data loading properties of the form
		this.m_dataLoading = false;
		this.m_dataLoaded = true;
	},

	// Edits in the render functions to make sure we don't call OnLoad and OnShow twice or before the data is loaded
	_Render: function (content) {
		if (this.destroying || this.destroyed) return;
		this.removeAll();
		const cfg = Ext.decode(content);
		cfg.extend = 'StarlimsInnerForm';
		cfg.form = this;
		if (cfg.xtype) cfg.xtype = null;
		this.__starlims_form_width__ = cfg.Width;
		this.__starlims_form_height__ = cfg.Height;
		this._InitResources(cfg);
		this.DataLoader = Ext.create('StarlimsDataLoader', this);
		this.$ActiveCtrl = {
			ZeroTabIndexId: null,
			MinTabIndexId: null,
			MinTabIndex: Number.MAX_VALUE.toString(),
		};
		this._SetForm(cfg);
		Ext.define(cfg.FormType, cfg);
		const innerForm = Ext.create(cfg.FormType, cfg);

		// QVA Edits here, retrieve the firedOnLoad and firedOnShow properties if they have been set earlier
		// or set them to their default values
		this.m_firedOnShow = this.m_firedOnShow ?? false;
		this.m_firedOnLoad = this.m_firedOnLoad ?? false;
		if (!this.m_firedOnShow && this.rendered && this.hasListener('OnShow')) {
			this.m_firedOnShow = true;
			this.fireEvent('OnShow', this, null);
		}

		if (this.opener && this.opener.StickyVariables) this.opener.StickyVariables.CopyTo(this.StickyVariables);
		this._LoadData();
		this._InitKeyEvents();
		this._FocusActiveControl();

		// If the data is loading or has been loaded, do not call the OnLoad event
		// as it has already been called in the _LoadData function
		if (this.m_dataLoading || this.m_dataLoaded) return;
		if (!this.m_firedOnLoad && this.rendered && this.hasListener('OnLoad')) {
			this.m_firedOnLoad = true;
			this.fireEvent('OnLoad', this, null);
		}
		// End of QVA Edits
	},

	// Edit in the ShowDialog as dialogs render functions can be called from here
	_ShowDialog: async function _ShowDialog(url, args, onCloseCallback, additionalWindowConfig) {
		const me = this;
		const windowConfig = {
			modal: true,
			constrainHeader: true,
			layout: 'fit',
			tabGuard: false,
		};
		Ext.apply(windowConfig, additionalWindowConfig);
		const dialogWindow = Ext.create('Ext.Window', windowConfig);
		const form = Ext.create('StarlimsForm');
		form.opener = this;
		if (typeof onCloseCallback === 'function')
			form._CloseCallbackInfo = {
				fn: onCloseCallback,
				scope: me,
			};
		else form._CloseCallbackInfo = onCloseCallback;
		return new Promise(function (resolve, reject) {
			form.Navigate(url, args, false)
				.then(function () {
					dialogWindow.title = form.Text;
					const loadMask = Ext.ComponentQuery.query('viewport')[0].loadMask;
					const loadMaskState = loadMask ? loadMask.isVisible() : false;
					dialogWindow.__OriginalClose = dialogWindow.close;
					dialogWindow.close = async function () {
						if (form.Closed) dialogWindow.__OriginalClose();
						else {
							const r = await form.__CanClose();
							if (r) {
								try {
									if (!dialogWindow.destroyed) dialogWindow.removeAll(true);
								} catch (ex) {
									/* empty */
								}
								dialogWindow.__OriginalClose();
							}
						}
						if (loadMask && loadMaskState) loadMask.setVisible(true);
					};
					dialogWindow.on('close', function (sender, opts) {
						if (form.opener && form.opener.Container && form.opener.Container.Dialogs)
							form.opener.Container.Dialogs.Remove(form);
						if (!form.Closed && form._CloseCallbackInfo && form._CloseCallbackInfo.fn)
							form._CloseCallbackInfo.fn.call(form._CloseCallbackInfo.scope, form.returnValue);
						form.Closed = true;
						me.__refocusControl();
					});
					dialogWindow.add(form);
					dialogWindow.show();
					const dlgWidth = dialogWindow.getWidth();
					const dlgHeight = dialogWindow.getHeight();
					if (loadMask && loadMaskState) loadMask.setVisible(false);
					form._FocusActiveControl();
					// QVA Edits here, only fire OnShow
					if (!form.m_firedOnShow && form.hasListener('OnShow')) {
						form.m_firedOnShow = true;
						form._FireOnShow(true);
					}
					// QVA Edits here, only fire the OnLoad event if the data is not loaded
					if (!form.m_dataLoading && !form.m_dataLoaded) {
						if (!form.m_firedOnLoad && form.hasListener('OnLoad')) {
							form.m_firedOnLoad = true;
							form._FireOnLoad(true);
						}
					}
					// End of QVA Edits
					try {
						if (dlgWidth !== dialogWindow.getWidth() || dlgHeight !== dialogWindow.getHeight()) {
							const x = Math.ceil((Ext.getViewportWidth() - dialogWindow.getWidth()) / 2);
							const y = Math.ceil((Ext.getViewportHeight() - dialogWindow.getHeight()) / 2);
							dialogWindow.setLocalXY(x, y);
						}
					} catch (ex) {
						console.error(ex);
					}
					if (form.CloseOnEscape) dialogWindow.closable = true;
					else dialogWindow.onEsc = Ext.emptyFn;
					if (this.Container && this.Container.Dialogs) this.Container.Dialogs.Add(form);
					resolve(form);
				})
				.catch(function (err) {
					reject(err);
				});
		});
	},

	// Same here, OnLoad and OnShow can be called from here so we make sure they are not called twice
	_InitEvents: function () {
		const me = this;
		async function handleBeforeUnload(e) {
			const canClose = await me.__CanClose();
			if (!canClose) {
				e.preventDefault();
				e.returnValue = '';
			}
		}
		me.on('afterrender', function () {
			// QVA Edits here, only fire the OnLoad and OnShow events if the data is not loaded
			if (!me.m_dataLoaded && !me.m_dataLoading) {
				if (!me.m_firedOnLoad && me.hasListener('OnLoad')) {
					me.m_firedOnLoad = true;
					me.fireEvent('OnLoad', me, null);
				}
				if (!me.m_firedOnShow && me.hasListener('OnShow')) {
					me.m_firedOnShow = true;
					me.fireEvent('OnShow', me, null);
				}
			}
			// End of QVA Edits
			me._InitKeyEvents();
			if (!me.hasListener('OnClose')) {
				window.addEventListener('beforeunload', handleBeforeUnload);
			}
		});
		me.on('formClosed', function () {
			const parent = me.up();
			if (Ext.getClassName(parent) === 'StarlimsFrame') parent._CloseInternalForm();
			else if (Ext.getClassName(parent) === 'Ext.window.Window') parent.close();
			window.removeEventListener('beforeunload', handleBeforeUnload);
		});
		me.on('resize', function (thisForm, width, height, oldWidth, oldHeight, eOpts) {
			if (!me.m_firedOnResize) me.m_firedOnResize = true;
			else
				me.fireEvent('OnResized', me, {
					OldWidth: oldWidth,
					OldHeight: oldHeight,
					Width: width,
					Height: height,
				});
		});
	},
});

// endregion
