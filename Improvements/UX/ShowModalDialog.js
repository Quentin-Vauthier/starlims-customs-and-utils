// region QVA Display loading message each time ShowModalDialog is called to prevent any other action
Ext.override(StarlimsForm, {
	ShowModalDialog: async function ShowModalDialog(url, args, onCloseCallback, windowConfig) {
		const me = this;
		try {
			this.mask('Loading...');
		} catch (ign) {
			/* empty */
		}

		if (typeof onCloseCallback !== 'function' && !windowConfig) {
			windowConfig = onCloseCallback;
			onCloseCallback = null;
		}

		if (onCloseCallback) {
			return this.callParent([
				url,
				args,
				() => {
					onCloseCallback();
					try {
						me.unmask();
					} catch (ign) {
						/* empty */
					}
				},
				windowConfig,
			]);
		}

		const ret = await this.callParent([url, args, null, windowConfig]);
		try {
			me.unmask();
		} catch (ign) {
			/* empty */
		}
		return ret;
	},
});
// endregion
