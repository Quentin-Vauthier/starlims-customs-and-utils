// region QVA 2022-04-21 - override combobox to match V10/V11 behavior
// When pressing a key multiple times, it will select rows that starts wth this key
Ext.override(StarlimsComboBox, {
    _InitEvents: function() {
        this.callParent(arguments);
        this.on('keypress', function() {
            if(this.AllowTextEdit) return
            const pressedKey = event.key.toUpperCase()
            if(!this.lastPressedKey || this.lastPressedKey !== pressedKey) {
                this.lastPressedKey = pressedKey;
                this.pressedKeyCount = 0;
                return;
            }
            const filteredItems = this.Items.filter(i => i.m_text.toUpperCase().startsWith(pressedKey))
            if(++this.pressedKeyCount > filteredItems.length) this.pressedKeyCount = 0;
            const index = this.Items.indexOf(filteredItems[this.pressedKeyCount])
            this.setSelection(index)
            this.expand()
        })
        this.on('focusleave', function() {this.lastPressedKey = null})
    }
})
// endregion