// region 2024-04 QVA Datagrid FilterBar
/**
 * Adds a filter bar to the datagrid columns
 * @author Quentin Vauthier
 * @version 1.0.7
 */
class StarlimsDgdFilterBar {
	// #region all listeners used in the filters
	// Listener used to prevent the grid from sorting when clicking on the filter field
	static ignoreClickListener = {
		element: 'el',
		fn: function (e) {
			// Prevent the grid from sorting when clicking on the filter field
			e.stopPropagation();
		},
	};

	// Listener used to filter based on text (case insensitive and partial match)
	simplefieldApplyFilters(column, checkKeyChar, exactMatch, e) {
		if (checkKeyChar && e.keyCode !== 13 /* Enter key */) return;
		const value = e.target.value;
		if (column.filterBar.currentValue === value) return;
		if (!value) {
			this.state[column.dataIndex] = null;
			return this.grid.store.removeFilter(column.dataIndex);
		}
		column.filterBar.currentValue = value;
		this.state[column.dataIndex] = value;
		const filter = new Ext.util.Filter({
			id: column.dataIndex,
			property: column.dataIndex,
			filterFn: record => {
				if (this.config.caseSensitive)
					return exactMatch
						? record.get(column.dataIndex)?.toString() === value.toString()
						: record.get(column.dataIndex)?.toString().includes(value);
				else
					return exactMatch
						? record.get(column.dataIndex)?.toString().toLowerCase() === value.toLowerCase()
						: record.get(column.dataIndex)?.toString().toLowerCase().includes(value.toLowerCase());
			},
		});
		this.grid.store.addFilter(filter);
	}
	textfieldFocusOutListener(column, e) {
		this.simplefieldApplyFilters(column, false, false, e);
	}
	numericfieldFocusOutListener(column, e) {
		this.simplefieldApplyFilters(column, false, true, e);
	}
	textfieldChangeListener(column, e) {
		this.simplefieldApplyFilters(column, true, false, e);
	}
	numericfieldChangeListener(column, e) {
		this.simplefieldApplyFilters(column, true, true, e);
	}

	// Listener used to filter based on combo box value (exact match)
	directChangeListener(column, e) {
		const value = e.value;
		if (column.filterBar.currentValue === value) return;
		if (!value) {
			this.state[column.dataIndex] = null;
			return this.grid.store.removeFilter(column.dataIndex);
		}
		column.filterBar.currentValue = value;
		this.state[column.dataIndex] = value;
		const filter = new Ext.util.Filter({
			id: column.dataIndex,
			property: column.dataIndex,
			value: value,
		});
		this.grid.store.addFilter(filter);
	}

	// Listener used to filter based on dates, will filter on the same day
	dateChangeListener(column, el) {
		const value = el.value !== el.getValue() ? el.getValue() : el.value;
		if (el.getValue() === null) {
			this.state[column.dataIndex] = null;
			return this.grid.store.removeFilter(column.dataIndex);
		}
		if (!(value instanceof Date)) return;
		if (this.state[column.dataIndex]?.toUTCString() === value?.toUTCString()) return;
		column.filterBar.currentValue = value;
		this.state[column.dataIndex] = value;
		const filter = new Ext.util.Filter({
			id: column.dataIndex,
			property: column.dataIndex,
			filterFn: record =>
				record.get(column.dataIndex) && StarlimsDgdFilterBar.isSameDay(value, record.get(column.dataIndex)),
		});
		this.grid.store.addFilter(filter);
	}
	// #endregion

	// #region utilities functions
	// Check if two dates are the same day, used for date filters
	static isSameDay(date1, date2) {
		return (
			date1.getFullYear() === date2.getFullYear() &&
			date1.getMonth() === date2.getMonth() &&
			date1.getDate() === date2.getDate()
		);
	}

	//! Change these to match the date format that you want
	// All the formats are available here : https://docs.sencha.com/extjs/6.5.2/modern/Ext.Date.html
	// A function is used to create all possible combinations of date formats based on the following parts
	// Make sure to use an array, even if there is only one element, the default date format will use the first element of each array
	static senchaDateFormats = {
		day: ['d', 'j'],
		month: ['m', 'n'],
		year: ['Y'],
	};

	static generateDateFormats(parts, currentIndex = 0, currentFormat = '') {
		// Base case: if currentIndex reaches the length of parts, add currentFormat to the result
		if (currentIndex === parts.length) {
			return [currentFormat];
		}

		const part = parts[currentIndex];
		//                                                      this for literals
		const substitutesForPart = StarlimsDgdFilterBar.senchaDateFormats[part.type] ?? [part.value];
		let formats = [];

		// Iterate through each substitute for the current part
		for (const substitute of substitutesForPart) {
			// Recursively generate formats by appending the current substitute to the currentFormat
			const newFormat = currentFormat + substitute;
			formats = formats.concat(StarlimsDgdFilterBar.generateDateFormats(parts, currentIndex + 1, newFormat));
		}
		return formats;
	}

	getFilterField(column, grid) {
		switch (column.Type) {
			case 'COMBOBOX':
			case 'EDITABLECOMBOBOX':
				let comboItems = [];
				if (!column.DataSet && !column.hasListeners.ondataloaded) {
					// Custom event fired by AsyncDataLoader, in default Starlims implementation the data will already be loaded
					column.addListener('OnDataLoaded', () => {
						this.updateData(column);
					});
				} else if (column.DataSet) {
					comboItems = column.DataSet?.Tables[0]?.Rows.ToArray().map(r => ({
						text: r.get(column.DisplayMember),
						value: r.get(column.ValueMember),
					}));
				}
				return {
					xtype: 'combo',
					editable: column.Type === 'EDITABLECOMBOBOX',
					allowBlank: true,
					displayField: 'text',
					valueField: 'value',
					value: column.filterBar?.currentValue,
					store: [{ text: 'All', value: null }, ...(comboItems ?? [])],
					listeners: {
						click: StarlimsDgdFilterBar.ignoreClickListener,
						change: this.directChangeListener.bind(this, column),
						// keypress listener to allow selecting items by typing the first letters
						keydown: {
							element: 'el',
							fn: function (e) {
								const SAME_SEARCH_DELAY = 500;
								const combo = this.component;
								if (e.keyCode === 13) combo.fireEvent('change', combo, combo.getValue());
								if (combo.editable) return;
								const pressedKey = e.key()?.toUpperCase();
								if (!pressedKey || pressedKey.length > 1) return;
								const now = new Date().getTime();
								if (now - combo.currentSelectLastPress < SAME_SEARCH_DELAY) {
									combo.currentSelectLastPress = now;
									combo.currentSelection += pressedKey;
								} else {
									combo.currentSelectLastPress = now;
									combo.currentSelection = pressedKey;
								}
								const filteredItem = combo.store.data.items.find(i =>
									i.data.text?.toUpperCase().startsWith(combo.currentSelection),
								);
								if (!filteredItem) return;
								combo.select(filteredItem);
								combo.expand();
							},
						},
					},
				};
			case 'NUMERIC':
				return {
					xtype: 'numberfield',
					value: column.filterBar?.currentValue,
					listeners: {
						click: StarlimsDgdFilterBar.ignoreClickListener,
						keydown: {
							element: 'el',
							fn: this.numericfieldChangeListener.bind(this, column),
						},
						focusleave: {
							element: 'el',
							fn: this.numericfieldFocusOutListener.bind(this, column),
						},
					},
				};
			case 'CHECKBOX':
				return {
					xtype: 'combo',
					allowBlank: true,
					displayField: 'text',
					valueField: 'value',
					store: [
						{ text: 'All', value: null },
						{ text: 'Yes', value: column.m_dto.CheckBoxTrueValue ?? 'Y' },
						{ text: 'No', value: column.m_dto.CheckBoxFalseValue ?? 'N' },
					],
					value: column.filterBar?.currentValue,
					listeners: {
						click: StarlimsDgdFilterBar.ignoreClickListener,
						change: this.directChangeListener.bind(this, column),
					},
				};
			case 'DATETIME':
				return {
					xtype: 'datefield',
					value: column.filterBar?.currentValue,
					clearable: true,
					format: new Intl.DateTimeFormat()
						.formatToParts()
						.map(part => {
							if (part.type === 'literal') return part.value;
							return StarlimsDgdFilterBar.senchaDateFormats[part.type][0];
						})
						.join(''),
					altFormats: ['Y-m-d']
						.concat(
							StarlimsDgdFilterBar.generateDateFormats(
								new Intl.DateTimeFormat().formatToParts(new Date()),
							),
						)
						.join('|'),
					emptyText: new Date().toLocaleDateString().slice(0, 10),
					listeners: {
						click: StarlimsDgdFilterBar.ignoreClickListener,
						change: this.dateChangeListener.bind(this, column),
						focusleave: this.dateChangeListener.bind(this, column),
						keydown: {
							element: 'el',
							fn: e => e.keyCode === 13 && e.target.blur(),
						},
					},
				};
			default:
				return {
					xtype: 'textfield',
					value: column.filterBar?.currentValue,
					listeners: {
						click: StarlimsDgdFilterBar.ignoreClickListener,
						keydown: {
							element: 'el',
							fn: this.textfieldChangeListener.bind(this, column),
						},
						focusleave: {
							element: 'el',
							fn: this.textfieldFocusOutListener.bind(this, column),
						},
					},
				};
		}
	}
	// #endregion

	/**
	 * @constructor StarlimsDgdFilterBar
	 * @param {StarlimsDataGrid} grid - The grid to add the filter bar to
	 * @param {Object} [config] - Configuration object
	 * @param {boolean} [config.caseSensitive=false] - Whether the text filter should be case sensitive
	 */
	constructor(
		grid,
		config = {
			caseSensitive: false,
		},
	) {
		grid.filterBar = this;
		this.grid = grid;
		this.fields = [];
		this.state = {};
		this.config = config;

		this.grid.on('afterrender', () => {
			this.render();
		});

		this.grid.on('beforereconfigure', () => {
			this.destroy();
			this.render();
		});

		this.grid.on('destroy', () => {
			this.destroy();
		});

		this.grid.on('columnresize', () => {
			this.updateLayout();
		});
	}

	/**
	 * Render the filter bar on the grid columns
	 * @private
	 * @returns {void}
	 */
	render() {
		if (!this.grid.FilterRowVisible) return;
		// TODO: Find a way not to render the filter bar if the grid is empty
		// if (this.grid.RowCount === 0) return;
		Ext.suspendLayouts();
		const columns = this.grid.getColumns();
		for (const column of columns) {
			if (!column.Caption || column.Hidden) continue;
			const field = this.getFilterField(column, this.grid);
			column.filterBar = {
				currentValue: column.filterBar?.currentValue ?? null,
				field,
			};
			const targetDom = column?.el?.dom;
			if (!targetDom) continue;
			const fieldEl = Ext.create({
				height: 24,
				width: column.getWidth() - 1,
				renderTo: targetDom,
				...field,
			});
			this.fields.push(fieldEl);
			column.filterBarField = fieldEl;
			// Fix the filter field not being displayed columns that have a one row header text when other columns from the same grid have a multi row header text \n and <br/>
			if (column.el.dom?.firstChild) column.el.dom.firstChild.style.height = `${column.getHeight() - 24}px`;
		}
		this.rendered = true;
		Ext.resumeLayouts(true);
	}

	/**
	 * Destroy the filter bar
	 * @private
	 * @returns {void}
	 */
	destroy() {
		if (!this.rendered || this.grid.destroying || this.grid.destroyed) return;
		Ext.suspendLayouts();
		for (const column of this.grid.getColumns()) {
			column.filterBarField?.destroy();
		}
		this.fields = [];
		this.rendered = false;
		Ext.resumeLayouts(true);
	}

	/**
	 * Update the layout of the filter bar
	 * @private
	 */
	updateLayout() {
		if (!this.rendered) return;
		Ext.suspendLayouts();
		for (const column of this.grid.getColumns()) {
			column.filterBarField?.setWidth(column.el.getWidth() - 1);
		}
		Ext.resumeLayouts(true);
	}

	/**
	 * Updates the data of a combo box filter field
	 * @param {StarlimsDataGridColumn} column
	 */
	updateData(column) {
		const comboItems = column.DataSet?.Tables[0]?.Rows.ToArray().map(r => ({
			text: r.get(column.DisplayMember),
			value: r.get(column.ValueMember),
		}));
		if (!comboItems) return;
		const store = Ext.create('Ext.data.Store', {
			fields: ['text', 'value'],
			data: [{ text: 'All', value: null }, ...comboItems],
		});
		column.filterBarField.setStore(store);
	}
}

// Override the StarlimsDataGrid to add the filter bar by default (only rendered if FilterRowVisible is true)
Ext.override(StarlimsDataGrid, {
	initComponent: function () {
		this.callParent(arguments);
		new StarlimsDgdFilterBar(this);
	},
});

// fix issue with focus out of some filter fields
Ext.override(Ext.view.Table, {
	onFocusLeave: function () {
		try {
			this.callParent(arguments);
		} catch (err) {
			/* empty */
		}
	},
});
// endregion
