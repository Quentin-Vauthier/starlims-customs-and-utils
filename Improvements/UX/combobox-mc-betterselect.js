// region QVA 2023-04-19 - override combobox and multichoice behavior to allow selecting items by typing the first letters
Ext.override(StarlimsComboBox, {
	SAME_SEARCH_DELAY: 500, // Delay in ms to consider the same search
	_InitEvents: function () {
		this.callParent(arguments);
		this.on('keypress', function () {
			event.preventDefault();
			event.stopPropagation();
			if (this.AllowTextEdit) return;
			if (event.key.length > 1) return;
			const pressedKey = event.key.toUpperCase();
			const now = new Date().getTime();
			if (now - this.currentSelectLastPress < this.SAME_SEARCH_DELAY) {
				this.currentSelectLastPress = now;
				this.currentSelection += pressedKey;
			} else {
				this.currentSelectLastPress = now;
				this.currentSelection = pressedKey;
			}
			const filteredItems = this.Items.filter(i => i.m_text.toUpperCase().startsWith(this.currentSelection));
			const index = filteredItems.length ? this.Items.indexOf(filteredItems[0]) : this.currentSelectionIndex ?? 0;
			this.currentSelectionIndex = index;
			this.setSelection(index);
			this.expand();
		});
	},
});

Ext.override(Ext.view.BoundList, {
	privates: {
		finishRenderChildren: function () {
			this.callParent(arguments);
			// Clear the keypress listener added by default
			this.events.itemkeypress?.clearListeners();
		},
	},
	ITEM_HEIGHT: 32, // Default item height, change if needed
	SAME_SEARCH_DELAY: 500, // Delay in ms to consider the same search
	onItemKeyDown: function () {
		if (event.key.length > 1) return;
		const pressedKey = event.key.toUpperCase();
		const now = new Date().getTime();
		if (now - this.currentSelectLastPress < this.SAME_SEARCH_DELAY) {
			this.currentSelectLastPress = now;
			this.currentSelection += pressedKey;
		} else {
			this.currentSelectLastPress = now;
			this.currentSelection = pressedKey;
		}
		const filteredItems = this.getStore().data.items.filter(t =>
			t.data.m_text.toUpperCase().startsWith(this.currentSelection),
		);
		const index = filteredItems.length
			? this.getStore().data.items.indexOf(filteredItems[0])
			: this.currentSelectionIndex ?? 0;
		this.currentSelectionIndex = index;
		this.setScrollY(this.ITEM_HEIGHT * index);
		this.select(filteredItems[0]);
		event.stopPropagation();
	},
});
// endregion
