// region QVA 2024-04-23 - Improve console/notification searchbar
Ext.Function.interceptBefore(window, '__StarlimsOnShellScriptFilesLoaded', async () => {
	Ext.override(STARLIMS.view.console.ConsoleController, {
		filterStore: function (value) {
			const me = this;
			const searchString = value.toLowerCase();
			const store = this.view.items.items[0].getStore();
			if (!searchString) return store.clearFilter();

			const filterFn = function (node) {
				if (node.childNodes?.some(filterFn)) {
					node.expand();
					return true;
				}
				if (!node.childNodes?.length)
					return (
						node.data.text?.toLowerCase().includes(searchString) ||
						node.parentNode?.data.text?.toLowerCase().includes(searchString)
					);
				if (node.data.text.toLowerCase().includes(searchString)) return true;
				return false;
			};

			store.getFilters().replaceAll({
				filterFn,
			});
		},
	});
});
// endregion
