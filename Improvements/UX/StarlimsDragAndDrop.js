/********************************************************************************
Description.. :	Example usage for SDMS Import Queue can be found at https://pastebin.com/aTmvqyQ5 (Password : slqva)
				Example usage for Folders (Attahments) can be found at https://pastebin.com/WWyKB2km (Password : slqva)
Author....... :	Quentin Vauthier (QVA)
Date......... : 2023-07-31
********************************************************************************
_______________________________________________________________________________

Modification. : 	
Author....... :		
Date......... :	
_______________________________________________________________________________
********************************************************************************/

/**
 * Creates a drag and drop zone in an element allowing to upload a file and handle it in a callback
 */
class StarlimsDragAndDrop {
	/**
	 * Instanciate a new StarlimsDragAndDrop
	 * @param {HTMLElement | Ext.Component} target the target element where the drag and drop zone will be created
	 * @param {Function} callback A function that will be called when the file is uploaded. Parameters are:
	 * @param {string} callback.success If the upload was successful
	 * @param {string} callback.msg The message returned by the server
	 * @param {string} callback.file The file name
	 * @param {string} callback.serverPath The server path of the file
	 * @param {string} callback.scriptName The name of the script that handled the uploaded file
	 * @param {string} callback.result The result of the script that handled the uploaded file
	 * @param {object} options
	 * @param {string} options.scriptName The name of the script that will handle the file upload. Default is 'Enterprise_Utilities.EchoFile'
	 */
	constructor(target, callback, options = {}) {
		if (target instanceof Ext.Component) target = target.getEl().dom;
		this.target = target;
		this.callback = callback;
		this.options = options;

		if (!Array.from(document.styleSheets[0].cssRules).some(r => r.selectorText === '.drag-over')) this.initCss();
		target.classList.add('drop-zone');

		// Add the required event listeners
		target.addEventListener('dragleave', this.handleDragLeave.bind(this), false);
		target.addEventListener('dragend', this.handleDragLeave.bind(this), false);
		target.addEventListener('dragover', this.handleDragOver.bind(this), false);
		target.addEventListener('drop', this.handleFileSelect.bind(this), false);
	}

	initCss() {
		if (document.dndCss) return;
		const styleSheet = document.dndCss;
		if (!styleSheet) {
			const style = document.createElement('style');
			document.head.appendChild(style);
			document.dndCss = style.sheet;
		}
		// Css for the drag and drop zone, make a gray zone with text inside
		document.dndCss.insertRule(
			`.drag-nok::after {
				content: 'Cannot currently drag files here';
				background-color: rgba(200, 50, 50, 0.3);
				font-size: 2em;
				color: #fff;
				width: 100%;
				height: 100%;
				display: flex;
				justify-content: center;
				align-items: center;
				position: absolute;
				top: 0;
				left: 0;
				cursor: not-allowed;
				z-index: 1000;
			}`,
		);
		document.dndCss.insertRule(
			`.drag-over::after {
				content: 'Drop files here';
				background-color: rgba(0, 0, 0, 0.3);
				font-size: 2em;
				color: #fff;
				width: 120%;
				height: 120%;
				transform: translate(-10%, -10%);
				display: flex;
				justify-content: center;
				align-items: center;
				position: absolute;
				top: -10%;
				left: -10%;
				z-index: 1000;
				animation: pulse 1s infinite ease-in-out alternate;
			}`,
		);
		document.dndCss.insertRule(
			`@keyframes pulse {	
				from { transform: scale(1); }
				to { transform: scale(1.2); }
			}`,
		);
		window.dndCss = true;
	}

	handleDragOver(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		if (this.options.isOkToDrag && typeof this.options.isOkToDrag === 'function' && !this.options.isOkToDrag()) {
			this.target.classList.add('drag-nok');
			evt.dataTransfer.effectAllowed = 'none';
			evt.dataTransfer.dropEffect = 'none';
			return;
		}
		// If the target element does not have the class 'drag-over', add it
		if (!this.target.classList.contains('drag-over')) this.target.classList.add('drag-over');
		evt.dataTransfer.dropEffect = 'copy';
	}

	handleDragLeave(evt) {
		// Do not remove the class if the drag leave event is fired from a child element
		if (evt.target !== this.target) return;
		// If the drag leave event is fired from the target element, remove the class
		this.target.classList.remove('drag-over');
		this.target.classList.remove('drag-nok');
		evt.stopPropagation();
		evt.preventDefault();
	}

	async handleFileSelect(evt) {
		if (this.options.isOkToDrag && typeof this.options.isOkToDrag === 'function' && !this.options.isOkToDrag()) {
			this.target.classList.remove('drag-nok');
			return;
		}
		this.target.classList.remove('drag-over');
		const maskTarget = form.All(this.target.getAttribute('automation-id')) ?? form;
		evt.stopPropagation();
		evt.preventDefault();
		const files = evt.dataTransfer.files;
		maskTarget.mask(`Uploading files...`);
		const formData = new FormData();
		for (const file of files) formData.append('files', file);
		const url = `Runtime_Support.SaveFileFromHTML.lims?${
			this.options.scriptName ? `ScriptName=${this.options.scriptName}&` : ''
		}multiple=true`;
		const res = await fetch(url, {
			method: 'POST',
			body: formData,
		});
		maskTarget.mask('Finishing upload...');
		const json = await res.json();
		maskTarget.unmask();
		this.callback(json);
	}
}
