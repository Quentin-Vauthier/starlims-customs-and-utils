// region amélioration du displayWaitMessage
Ext.override(StarlimsForm, {
	DisplayWaitMessage: async function (mask, useOld = false) {
		if (useOld === true) return this.callParent([mask]);
		if (!this.isVisible())
			return console.warn(
				'DisplayWaitMessage: form is not visible, please do not use this method in `OnInit` but only in `OnLoad`',
			);
		// Do not change conditions, we need to check the strict equality
		if (mask === false)
			try {
				this.unmask();
			} catch (e) {
				// ignored, throw an error only when the mask is already gone
				return;
			}
		else this.mask(mask === true ? 'Loading...' : mask);
		await Application.DoEvents();
	},
});
// endregion
