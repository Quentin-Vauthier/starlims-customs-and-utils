/********************************************************************************
Description.. :	utility function for working with context menus
Author....... :	OVIDIU
Date......... :    		
********************************************************************************
_______________________________________________________________________________

Modification. : Refactored + added new functions
Author....... :	Quentin Vauthier (QVA)	
Date......... :	2023-07-03
_______________________________________________________________________________
********************************************************************************/

// Uncomment this line to make excel export work
// commented by default in case the client script isn't installed
// #include "Utilities.ExcelJS"

const __contextMenuLabels = {
	ENG: {
		COLUMN_INFO: 'Get Column Information',
		REFRESH_DATA: 'Refresh Data',
		GROUPING: 'Grouping',
		CLEAR_GROUPING: 'Clear',
		ADD_TO_GROUP: 'Add to fields',
		REMOVE_FIELD_FROM_GROUP: 'Remove field',
		SORT_GROUP_THIS_FIELD: 'Sort by this field',
		COLLAPSE_ALL_GROUPS: 'Collapse All',
		EXPAND_ALL_GROUPS: 'Expand All',
		FILTER_ROW: 'Toggle Filter Row',
		HIDE_COLUMN: 'Hide Column',
		SHOW_ALL_COLUMNS: 'Show All Columns',
		COPY: 'Copy',
		COPY_SELECTED_ROWS: 'Selected Rows',
		COPY_TO_EXCEL: 'To Excel (Visible content)',
		COPY_TO_EXCEL_PAGINATION: 'All To Excel (Paging)',
		COPY_TO_XML: 'To XML',
		COPY_SELECTED_CELL: 'Selected Cell',

		// Other labels
		PAGINATION_ERROR: 'Pagination full export is not available for this table',
		LOADING: 'Loading, please wait...',
	},
	FR: {
		COLUMN_INFO: 'Obtenir les informations de la colonne',
		REFRESH_DATA: 'Rafraîchir les données',
		GROUPING: 'Groupage',
		CLEAR_GROUPING: 'Effacer le regroupement',
		ADD_TO_GROUP: 'Ajouter des champs au groupe',
		REMOVE_FIELD_FROM_GROUP: 'Supprimer le champ du groupe',
		SORT_GROUP_THIS_FIELD: 'Trier le regroupement par ce champ',
		COLLAPSE_ALL_GROUPS: 'Réduire tous les groupes',
		EXPAND_ALL_GROUPS: 'Développer tous les groupes',
		FILTER_ROW: 'Filtres',
		HIDE_COLUMN: 'Masquer la colonne',
		SHOW_ALL_COLUMNS: 'Afficher toutes les colonnes',
		COPY: 'Copier',
		COPY_SELECTED_ROWS: 'Les lignes sélectionnées',
		COPY_TO_EXCEL: 'Vers Excel',
		COPY_TO_EXCEL_PAGINATION: 'Tout Vers Excel (Pagination)',
		COPY_TO_XML: 'Vers XML',
		COPY_SELECTED_CELL: 'La cellule sélectionnée',

		// Other labels
		PAGINATION_ERROR: "L'export complet de pagination n'est pas disponible pour ce tableau",
		LOADING: 'Chargement, veuillez patienter...',
	},
	// Add more languages here
};
const contextMenuLabels = new Proxy(__contextMenuLabels, {
	get: function (target, prop) {
		const translation = target[Starlims.Navigator.Variables.myuserlang ?? 'ENG'][prop];
		return translation ?? prop;
	},
});

const contextMenuItems = {
	COLUMN_INFO: __dialogColumnInfo,
	REFRESH_DATA: __refresh_data,
	separator1: null,
	GROUPING: {
		CLEAR_GROUPING: __clear_grouping,
		ADD_TO_GROUP: __add_to_group,
		REMOVE_FIELD_FROM_GROUP: __remove_field_from_grouper,
		SORT_GROUP_THIS_FIELD: __sort_grouping,
		COLLAPSE_ALL_GROUPS: __collapse_groups,
		EXPAND_ALL_GROUPS: __expand_groups,
	},
	FILTER_ROW: __filter_row,
	HIDE_COLUMN: __hide_column,
	SHOW_ALL_COLUMNS: __show_all_columns,
	separator2: null,
	COPY: {
		COPY_SELECTED_ROWS: __copySelectedRows,
		COPY_TO_EXCEL: __copy_excel,
		// COPY_TO_EXCEL_PAGINATION: __copy_excel_pagination,
		COPY_TO_XML: __copy_xml,
		COPY_SELECTED_CELL: __copy_cell,
	},
};

// extends a context menu with base functionality
/**
 * Applies a base context menu to a datagrid
 * @param {Datagrid} control
 */
function ApplyBaseContextMenu(control) {
	if (!(control instanceof DataGrid)) {
		console.warn('ApplyBaseContextMenu should be called only on DataGrids');
		return;
	}

	if (control.ContextMenu?.isBaseContextMenu) return;

	control.EnableContextMenu = true;

	let oMenu = control.ContextMenu;
	if (oMenu) {
		__addSeparator(oMenu);
	} else {
		oMenu = new Menu();
		control.ContextMenu = oMenu;
	}

	control.ContextMenu.isBaseContextMenu = true;

	// add menu items
	for (const [key, value] of Object.entries(contextMenuItems)) {
		if (value === null) {
			__addSeparator(oMenu);
			continue;
		}
		if (typeof value === 'function') {
			__addItem(oMenu, contextMenuLabels[key], value);
			continue;
		}
		if (typeof value === 'object') {
			const oParent = __addItem(oMenu, contextMenuLabels[key]);
			for (const [subKey, subValue] of Object.entries(value)) {
				__addItem(oParent, contextMenuLabels[subKey], subValue);
			}
		}
	}

	__removeLastItemIfSeparator(oMenu); // needed in case this function is being called multiple times

	// add event handlers
	const auxClick = control.OnContextMenuClick;
	const auxPopup = control.OnContextMenuPopup;
	control.OnContextMenuClick =
		auxClick && auxClick !== __base_menu_OnContextMenuClick
			? async function (s, e) {
					if (!(await __base_menu_OnContextMenuClick(s, e))) auxClick(s, e);
				}
			: __base_menu_OnContextMenuClick;

	control.OnContextMenuPopup =
		auxPopup && auxPopup !== __base_menu_OnContextMenuPopup
			? function (s) {
					__base_menu_OnContextMenuPopup(s);
					auxPopup(s);
				}
			: __base_menu_OnContextMenuPopup;
}

function __findByText(oMenu, sText, aAlternateTexts = []) {
	aAlternateTexts.push(sText);
	for (const i of oMenu.Items) {
		for (const j of aAlternateTexts) {
			if (i.Text === aAlternateTexts[j]) return i;
		}
	}
	return null;
}

function __addItem(oMenu, sText, actionFunction, bAllowDuplicates, aAlternateTexts, sGroup, enabled = true) {
	if (!bAllowDuplicates && __findByText(oMenu, sText, aAlternateTexts)) return null;
	let oParent = oMenu;
	if (sGroup) {
		oParent = __findByText(oMenu, sGroup, []);
		if (!oParent) {
			oParent = new MenuItem();
			oParent.Id = sGroup;
			oParent.Text = sGroup;
			oMenu.Items.Add(oParent);
		}
		if (!bAllowDuplicates && __findByText(oParent, sText, aAlternateTexts)) {
			return null;
		}
	}
	if (!oParent) oParent = oMenu;
	const oItem = new MenuItem();
	oItem.Id = sText;
	oItem.Text = sText;
	oItem.Tag = { actionFunction: actionFunction ?? null };
	oItem.Enabled = enabled;
	oParent.Items.Add(oItem);
	return oItem;
}

function __addSeparator(oMenu) {
	if (oMenu.Items.Count > 0) {
		const oLastItem = oMenu.Items[oMenu.Items.Count - 1];
		// do not add 2 consecutive separators
		if (oLastItem.Text.ToCharArray().every(c => c === '-')) return;
	}
	return __addItem(oMenu, '-----', null, true, null, '', false);
}

function __removeLastItemIfSeparator(oMenu) {
	if (oMenu.Items.Count > 0) {
		const oLastItem = oMenu.Items[oMenu.Items.Count - 1];
		if (oLastItem.Text.ToCharArray().every(c => c === '-')) {
			oMenu.Items.RemoveAt(oMenu.Items.Count - 1);
		}
	}
}

async function __base_menu_OnContextMenuClick(sender, eventArgs) {
	if (Array.isArray(sender)) sender = sender[0];
	const bHandled = false;

	const oMenuItem = eventArgs.Menu;

	if (!oMenuItem.Tag) return false;

	if (oMenuItem.Tag?.actionFunction) {
		try {
			sender.mask(contextMenuLabels.LOADING);
			await oMenuItem.Tag.actionFunction.apply(sender, [sender, eventArgs]);
		} finally {
			sender.unmask();
		}
		return true;
	} else if (oMenuItem.Tag?.actionFunction === null) {
		return true; // If the action function is null, it means that the menu item is just a separator or a group
	}

	return bHandled;
}

function __base_menu_OnContextMenuPopup(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	for (const oItem of sender.ContextMenu.Items) {
		if (oItem.Tag && oItem.Tag.sFunction) {
			switch (oItem.Id) {
				case contextMenuLabels.FILTER_ROW:
					oItem.Checked = sender.FilterRowVisible;
					break;
				case contextMenuLabels.COPY_SELECTED_CELL:
					oItem.Enabled = sender.RowCount > 0;
					break;
				case contextMenuLabels.HIDE_COLUMN:
					oItem.Enabled = true;
					break;
				case contextMenuLabels.SHOW_ALL_COLUMNS:
					oItem.Enabled = !!(sender.Tag && sender.Tag.oHiddenColumns && sender.Tag.oHiddenColumns.Count > 0);
					break;
				case contextMenuLabels.REFRESH_DATA:
					oItem.Enabled = !!sender.Data;
					break;
				case contextMenuLabels.CLEAR_GROUPING:
				case contextMenuLabels.REMOVE_FIELD_FROM_GROUP:
					oItem.Enabled = sender.grouper?.columns?.length > 0;
					break;
				case contextMenuLabels.ADD_TO_GROUP:
				case contextMenuLabels.SORT_GROUP_THIS_FIELD:
					oItem.Enabled = sender.grouper?.columns?.length !== 0;
					break;
				case contextMenuLabels.COLLAPSE_ALL_GROUPS:
				case contextMenuLabels.EXPAND_ALL_GROUPS:
					oItem.Enabled = sender.grouper?.columns && sender.RowCount > 0;
					break;
			}
		}
	}
}

function clickOnMenuHeader(sender, doClick) {
	if (Array.isArray(sender)) sender = sender[0];
	if (doClick) {
		let currentColumn = null;
		if (sender.ContextMenu.Args.cellIndex !== undefined)
			currentColumn = sender.RootTable.Columns[sender.ContextMenu.Args.cellIndex];
		else currentColumn = sender.ContextMenu.Args.column;
		if (currentColumn) {
			currentColumn.getView().grid.headerCt.showMenuBy(null, currentColumn.el, currentColumn);
		}
	}
}

/**
 * Collapse all groups in a datagrid
 * @param {Datagrid} sender
 */
function __collapse_groups(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	sender.GroupMode = 'COLLAPSED';
	sender.CollapseRows();
}

/**
 * Expand all groups in a datagrid
 * @param {Datagrid} sender
 */
function __expand_groups(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	sender.GroupMode = 'EXPANDED';
	sender.ExpandRows();
}

function __filter_row(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	if (sender.filterBar instanceof StarlimsDgdFilterBar) {
		sender.FilterRowVisible = !sender.FilterRowVisible;
		sender.Redraw();
		return;
	}
	sender.FilterRowVisible = true; //for this to true all the time and just show the header menu
	clickOnMenuHeader(sender, sender.FilterRowVisible);
}

/**
 * Export the datagrid visible content to excel
 * @param {Datagrid} sender
 */
async function __copy_excel(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	csExportDatagridExcelJS(sender, {
		directDownload: true,
		colMinWidth: 15,
		colMaxWidth: 70,
		headerStyle: {
			font: {
				bold: true,
			},
			alignment: {
				horizontal: 'center',
				vertical: 'center',
				wrapText: true,
			},
		},
	});
}

/**
 * Export the datagrid to excel using pagination scripts if available
 * @param {Datagrid} sender
 */
async function __copy_excel_pagination(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	const rootTable = sender.RootTable;
	if (!rootTable) return __copy_excel(sender);
	const f = Shell.getCurrentForm();
	const sqlScript = f.Variables.__pagination_ssqlscript_;
	const sqlParams = f.Variables.__pagination_aParameters_;
	if (!sqlScript || !sqlParams) return Ext.Msg.alert('Error', contextMenuLabels.PAGINATION_ERROR);
	const dataSet = await lims.GetDataSetAsync('BatchInventory.GetExportExcelDS', [sqlScript, sqlParams]);
	csExportDataSetExcelJS(dataSet, {
		directDownload: true,
		rootTable: rootTable,
		colMinWidth: 15,
		colMaxWidth: 70,
		headerStyle: {
			font: {
				bold: true,
			},
			alignment: {
				horizontal: 'center',
				vertical: 'center',
				wrapText: true,
			},
		},
	});
}

function __copy_xml(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	lims.CallServerAsync(
		'Enterprise_Server.DataSetSupport.DSJSONToXMLFile',
		[sender.DataSet.Serialize()],
		function onSuccess(serverFile) {
			lims.DownloadFile('Enterprise_Utilities.EchoFile', [serverFile]);
		},
	);
}
function __hide_column(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	let currentColumn = null;
	if (sender.ContextMenu.Args.cellIndex !== undefined)
		currentColumn = sender.RootTable.Columns[sender.ContextMenu.Args.cellIndex];
	else currentColumn = sender.ContextMenu.Args.column;

	if (!sender.Tag) {
		sender.Tag = {};
	}
	let oHiddenColumns = sender.Tag.oHiddenColumns;
	if (!oHiddenColumns) {
		oHiddenColumns = [];
		sender.Tag.oHiddenColumns = oHiddenColumns;
	}
	oHiddenColumns.push(currentColumn);
	currentColumn.Visible = false;
}
function __show_all_columns(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	if (!sender.Tag) {
		sender.Tag = {};
	}
	let oHiddenColumns = sender.Tag.oHiddenColumns;
	if (!oHiddenColumns) {
		oHiddenColumns = [];
		sender.Tag.oHiddenColumns = oHiddenColumns;
	}

	for (const col of oHiddenColumns) col.Visible = true;

	sender.Tag.oHiddenColumns = [];
}

function __copy_cell(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	if (sender.ContextMenu.Args.cellIndex === undefined) return;
	__copyToClipboard(sender.ContextMenu.Args.td.innerText);
}

/**
 * Copies the given text to the clipboard
 * If the browser does not allow clipboard access, a message box will be displayed
 * @param {string} text
 */
function __copyToClipboard(text) {
	window.navigator.permissions.query({ name: 'clipboard-write' }).then(result => {
		if (result.state === 'granted') return window.navigator.clipboard.writeText(text);
		if (result.state === 'prompt')
			return result.addEventListener('change', result => {
				if (result.state === 'granted') return window.navigator.clipboard.writeText(text);
				Dialogs.MessageBox(
					'Clipboard access denied, please allow it in your browser settings to use this feature.',
					'Clipboard access denied',
					'OK',
					'Error',
				);
			});
		Dialogs.MessageBox(
			'Clipboard access denied, please allow it in your browser settings to use this feature.',
			'Clipboard access denied',
			'OK',
			'Error',
		);
	});
}

function __copySelectedRows(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	const visibleColumns = sender.RootTable.Columns.ToArray().filter(column => column.Visible === true);
	let str = '';

	visibleColumns.forEach(current => {
		str += `"${current.DataMember}"` + `\t`;
	});
	str += '\n\r';

	const rowIndexes = sender.GetSelectedRows();

	for (let i = 0; i < rowIndexes.length; i++) {
		const aRow = sender.DataSet.Tables[0].Rows[rowIndexes[i]];
		let strRow = '';
		for (const aColumn of visibleColumns) {
			strRow += `"${aRow[aColumn.DataMember] === null ? '' : aRow[aColumn.DataMember]}"` + `\t`;
		}
		str += `${strRow}\n\r`;
	}
	__copyToClipboard(str);
}

/**
 * Refreshes the datagrid data
 * @param {Datagrid} sender
 */
async function __refresh_data(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	sender.mask('Refreshing...');
	await Application.DoEvents();
	if (sender.Data) sender.Refresh();
	sender.unmask();
}

/**
 * Displays a message box with the column information (Table + Column)
 * @param {Datagrid} sender
 */
function __dialogColumnInfo(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	if (!(sender instanceof DataGrid)) return;
	const table = sender.RootTable.DataMember;
	let text;

	const col = getClickedColumn(sender);

	if (col?.Caption && col?.DataMember) {
		text = `${col.Caption} : ${table}.${col.DataMember}`;
	} else {
		text = col.DataMember ? col.DataMember : 'Error Column Info';
	}
	Dialogs.MessageBox(text, 'Column Info', 'OK', 'Info');
}

/**
 * Finds the correct column from the frozen grid if the context menu was opened on a frozen datagrid
 * @param {Datagrid} grid
 * @returns the correct column from the frozen grid
 */
function getColumnWithFrozenGrid(grid) {
	const [gridX] = grid.getPosition();
	const eX = event.x;
	const frozenColsWidth = grid.lockedGrid.columnManager.columns.reduce((acc, c) => acc + c.Width, 0);
	const inLockedGrid = eX - gridX <= frozenColsWidth;
	if (inLockedGrid) return grid.lockedGrid.columnManager.columns[grid.ContextMenu.Args.cellIndex];
	return grid.normalGrid.columnManager.columns[grid.ContextMenu.Args.cellIndex];
}

/**
 * Clears the grouping of a datagrid
 * @param {Datagrid} sender
 */
async function __clear_grouping(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	sender.grouper = null;
	sender.store.group(null);
}

/**
 * Add a column to the grouping of a datagrid
 * If the column is already in the grouping, it will be moved to the first position
 * @param {Datagrid} sender
 */
async function __add_to_group(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	const col = getClickedColumn(sender);
	if (!sender.grouper) {
		sender.grouper = createNewGrouper(col);
		sender.suspendEvents(); // Needs to be suspended in >12.6, do this for every `store.group` call
		sender.store.group(sender.grouper);
		sender.resumeEvents();
		return;
	}
	if (sender.grouper.columns.includes(col)) {
		// column already in grouping
		sender.grouper.setSortProperty(col.Id); // Sort by this column
		sender.grouper.columns = [col, ...sender.grouper.columns.filter(c => c.Id !== col.Id)];
		sender.suspendEvents();
		sender.store.group(sender.grouper);
		sender.resumeEvents();
		return;
	}
	const newColumns = [...sender.grouper.columns, col];
	const newGrouper = Ext.create('Ext.util.Grouper', {
		columns: newColumns,
		groupFn: sender.grouper.getGroupFn(),
		sortProperty: sender.grouper.getSortProperty(),
	});
	sender.grouper = newGrouper;
	sender.suspendEvents();
	sender.store.group(newGrouper);
	sender.resumeEvents();
}

/**
 * Sorts the grouping of a datagrid by the column on which the context menu was opened
 * @param {Datagrid} sender
 */
async function __sort_grouping(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	const col = getClickedColumn(sender);
	if (!sender.grouper) sender.grouper = createNewGrouper(col);
	else if (!sender.grouper.columns.includes(col)) addColumnToGrouper(sender.grouper, col);
	sender.grouper.setSortProperty(col.Id);
	sender.grouper.columns = [col, ...sender.grouper.columns.filter(c => c.Id !== col.Id)];
	sender.suspendEvents();
	sender.store.group(sender.grouper);
	sender.resumeEvents();
}

/**
 * Removes a column from the grouping of a datagrid
 * @param {Datagrid} sender
 */
async function __remove_field_from_grouper(sender) {
	if (Array.isArray(sender)) sender = sender[0];
	const col = getClickedColumn(sender);
	if (!sender.grouper) return Dialogs.MessageBox('No grouping to remove from.', 'Error', 'OK', 'ERROR');
	else if (!sender.grouper.columns.includes(col))
		return Dialogs.MessageBox('This column is not in the grouping.', 'Error', 'OK', 'ERROR');

	const newColumns = sender.grouper.columns.filter(c => c.Id !== col.Id);
	const newGrouper = Ext.create('Ext.util.Grouper', {
		columns: newColumns,
		groupFn: sender.grouper.getGroupFn(),
		sortProperty: sender.grouper.getSortProperty(),
	});
	sender.grouper = newGrouper;
	sender.suspendEvents();
	sender.store.group(newGrouper);
	sender.resumeEvents();
}

function createNewGrouper(...cols) {
	return Ext.create('Ext.util.Grouper', {
		columns: [...cols],
		groupFn: function (row) {
			return this.columns.map(c => `${c.Caption ?? c.Id} : ${row.get(c.Id)}`).join(' | ');
		},
		sortProperty: cols[0].Id,
	});
}

function addColumnToGrouper(grouper, col) {
	const newColumns = [...grouper.columns, col];
	return Ext.create('Ext.util.Grouper', {
		columns: newColumns,
		groupFn: grouper.getGroupFn(),
		sortProperty: grouper.getSortProperty(),
	});
}

function getClickedColumn(sender) {
	if (sender.lockedGrid && sender.normalGrid && !sender.ContextMenu.Args.column)
		return getColumnWithFrozenGrid(sender);
	else if (sender.ContextMenu.Args.cellIndex !== undefined)
		return sender.RootTable.Columns[sender.ContextMenu.Args.cellIndex];
	return sender.ContextMenu.Args.column;
}
